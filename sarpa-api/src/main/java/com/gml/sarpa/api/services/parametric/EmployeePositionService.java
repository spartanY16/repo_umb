/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: EmployeePositionService.java
 * Created on: Wed Jul 19 12:45:39 COT 2017
 * Project: sarpa
 * Objective: To manage employees positions
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.parametric;

import com.gml.sarpa.api.entity.parametric.EmployeePosition;
import java.util.List;

/**
 * To manage employees positions
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface EmployeePositionService{

    /**
     * List all entities
     * @return
     */
    List<EmployeePosition> findAll();

    /**
     * Save entity
     * @param employeePosition
     */
    void save(EmployeePosition employeePosition,String userName);

    /**
     * Update entity
     * @param employeePosition
     */
    void update(EmployeePosition employeePosition,String userName);

    /**
     * Delete entity
     *@param employeePosition
     */
    void remove(EmployeePosition employeePosition,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    EmployeePosition getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<EmployeePosition> getAllByFilter(String filter);

    
}
