/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContractTypeService.java
 * Created on: Thu Aug 17 22:44:19 COT 2017
 * Project: sarpa
 * Objective: To manage clients contract types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.commercial;

import com.gml.sarpa.api.entity.commercial.ContractType;
import java.util.List;

/**
 * To manage clients contract types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface ContractTypeService{

    /**
     * List all entities
     * @return
     */
    List<ContractType> findAll();

    /**
     * Save entity
     * @param contractType
     */
    void save(ContractType contractType,String userName);

    /**
     * Update entity
     * @param contractType
     */
    void update(ContractType contractType,String userName);

    /**
     * Delete entity
     *@param contractType
     */
    void remove(ContractType contractType,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    ContractType getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<ContractType> getAllByFilter(String filter);

    
}
