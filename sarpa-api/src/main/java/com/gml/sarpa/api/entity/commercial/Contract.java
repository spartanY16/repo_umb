/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Contract.java
 * Created on: Fri Jul 21 15:40:44 COT 2017
 * Project: sarpa
 * Objective: To manage clients contracts
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.commercial;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * To manage clients contracts
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "contract", schema = "commercial", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"code"},name = Contract.UC_contract_code),
})
public class Contract implements Serializable{

    public static final String UC_contract_code=
        "UC_contract_code";

    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    private Long id;

    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * User last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;

    /**
    * code
    */
    @Column(name = "code", nullable = false)
    private String code;

    /**
    * customer
    */
    @JoinColumn(name = "customer_id", nullable = false)
    @ManyToOne
    private Customer customer;

    /**
    * contractType
    */
    @JoinColumn(name = "contract_type_id", nullable = false)
    @ManyToOne
    private ContractType contractType;

    /**
    * initDate
    */
    @Column(name = "init_date", nullable = false)
    private Date initDate;

    /**
    * endDate
    */
    @Column(name = "end_date", nullable = false)
    private Date endDate;

    /**
    * discountPercent
    */
    @Column(name = "discount_percent", nullable = false)
    private Double discountPercent;

    /**
    * details
    */
    @Column(name = "details", nullable = false)
    private String details;

    
    /**
     * name
     */
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * description
     */
    @Column(name = "description", nullable = false)
    private String description;

    /*
     * -----------------------------------------------------------------------
     */
    
    public Contract() {
        
    }

    /*
     * -----------------------------------------------------------------------
     */

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }
    
    public void setDateCreated(Date date) {
        this.dateCreated = date;
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ContractType getContractType() {
        return contractType;
    }

    public void setContractType(ContractType contractType) {
        this.contractType = contractType;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
   
    /*
     * -----------------------------------------------------------------------
     */

    @Override
    public String toString() {
        return "Contract {" + 
            "code=" + code+
            "customer=" + customer.getCode()+
            "contractType=" + contractType.getName()+
            "initDate=" + initDate+
            "endDate=" + endDate+
            "discountPercent=" + discountPercent+
            "details=" + details+
+ '}';
    }

    
}
