/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gml.sarpa.api.utilities;

import com.gml.sarpa.api.dto.ResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import static com.gml.sarpa.exception.code.ErrorCode.ERR_001_UNKNOW;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author angie
 */
public final class Util {
    /**
     * User in session
     */
    public static final String SESSION_USER = "session_user";
    
    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER =
        Logger.getLogger(com.gml.sarpa.api.utilities.Util.class);
    
     /**
     * Calcula el Hash de un string utilizando el algoritmo SHA-1
     *
     * @param texto
     *
     * @return Hash del texto utilizando el algor&icute;tmo
     */
    public static String calculateSha1(String texto) {
        
        LOGGER.info("Metodo [ calculateSha1 ]");
        return DigestUtils.sha1Hex(texto);
    }
    /**
     * Mensaje de error en json.
     */
    private static final String JSON_ERROR = String.
        format("{\"code\": 1, \"message\": \"%s\"}", ERR_001_UNKNOW.getMessage());

    public static String getJson(ResponseDto response) {
        LOGGER.info("Ejecutando metodo [ getJson ]");

        try {
            ObjectMapper mapper = new ObjectMapper();

            return mapper.writeValueAsString(response);
        } catch (Exception ex) {
            LOGGER.error("Error", ex);
        }

        return JSON_ERROR;
    }
    
}
