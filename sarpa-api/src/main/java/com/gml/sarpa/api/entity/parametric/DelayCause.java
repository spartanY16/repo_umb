/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: DelayCause .java
 * Created on: Mon Jul 17 06:41:33 COT 2017
 * Project: sarpa
 * Objective: To manage Grounds for take-off delays
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.parametric;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * To manage Grounds for take-off delays
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "delay_cause", schema = "parametric", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"code"},name = DelayCause.UC_DELAY_CAUSE_CODE),
    @UniqueConstraint(columnNames = {"name"},name = DelayCause.UC_DELAY_CAUSE_NAME),
})
public class DelayCause implements Serializable{

    public static final String UC_DELAY_CAUSE_CODE =
        "UC_DELAY_CAUSE_CODE";
    
    public static final String UC_DELAY_CAUSE_NAME =
        "UC_DELAY_CAUSE_NAME";
    
    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    private Long id;

    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * User last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;

    /**
    * code
    */
    @Column(name = "code", nullable = false)
    private String code;
    
    /**
    * name
    */
    @Column(name = "name", nullable = false)
    private String name;

    /**
    * description
    */
    @Column(name = "description", nullable = true)
    private String description;


    /*
     * -----------------------------------------------------------------------
     */
    
    public DelayCause() {
        
    }

    /*
     * -----------------------------------------------------------------------
     */

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }
    
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated ;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

   
    /*
     * -----------------------------------------------------------------------
     */

    @Override
    public String toString() {
        return "DelayCause {" + 
                "code=" + code+
                "name=" + name+
                "description=" + description+
            + '}';
    }

    
}
