/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: MinuteValueService.java
 * Created on: Fri Jul 21 10:43:45 COT 2017
 * Project: sarpa
 * Objective: To manage companies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.commercial;

import com.gml.sarpa.api.entity.commercial.MinuteValue;
import java.util.List;

/**
 * To manage companies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface MinuteValueService{

    /**
     * List all entities
     * @return
     */
    List<MinuteValue> findAll();

    /**
     * Save entity
     * @param journeyValue
     */
    void save(MinuteValue journeyValue,String userName);

    /**
     * Update entity
     * @param journeyValue
     */
    void update(MinuteValue journeyValue,String userName);

    /**
     * Delete entity
     *@param journeyValue
     */
    void remove(MinuteValue journeyValue,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    MinuteValue getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<MinuteValue> getAllByFilter(String filter);

    
}
