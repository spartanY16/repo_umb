/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: MinuteValue.java
 * Created on: Fri Jul 21 10:43:45 COT 2017
 * Project: sarpa
 * Objective: To manage companies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.commercial;

import com.gml.sarpa.api.entity.operation.AirplaneType;
import com.gml.sarpa.api.entity.operation.Journey;
import com.gml.sarpa.api.entity.operation.ServiceType;
import com.gml.sarpa.api.entity.parametric.BusinessUnit;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * To manage companies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "minute_value", schema = "commercial", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"airplane_type_id", "service_type_id", "flg_international"},
            name = MinuteValue.UC_minute_value),})
public class MinuteValue implements Serializable {

    public static final String UC_minute_value
            = "UC_minute_value";

    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema = "public",
            sequenceName = "hibernate_sequence")
    private Long id;

    /**
     * Object version
     */
    @Column(name = "version", nullable = false)
    private Integer version;

    /**
     * Date created
     */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;

    /**
     * Date last updated
     */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;

    /**
     * User created
     */
    @Column(name = "user_created", nullable = false)
    private String userCreated;

    /**
     * User last updated
     */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;

    /**
     * businessUnit
     */
    @JoinColumn(name = "service_type_id", nullable = false)
    @ManyToOne
    private ServiceType serviceType;

    /**
     * journey
     */
    @JoinColumn(name = "airplane_type_id", nullable = false)
    @ManyToOne
    private AirplaneType airplaneType;

    /**
     * value
     */
    @Column(name = "value", nullable = false)
    private Double value;

    /**
     * flgInternational
     */
    @Column(name = "flg_international", nullable = false)
    private Boolean flgInternational;

    /*
     * -----------------------------------------------------------------------
     */
    public MinuteValue() {

    }

    /*
     * -----------------------------------------------------------------------
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion + 1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public void setDateCreated(Date dateCreated) {
         this.dateCreated = dateCreated;
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public AirplaneType getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(AirplaneType airplaneType) {
        this.airplaneType = airplaneType;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Boolean getFlgInternational() {
        return flgInternational;
    }

    public void setFlgInternational(Boolean flgInternational) {
        this.flgInternational = flgInternational;
    }

    /*
     * -----------------------------------------------------------------------
     */
    @Override
    public String toString() {
        return "MinuteValue{" + "serviceType=" + serviceType + ", airplaneType="
                + airplaneType + ", value=" + value + '}';
    }

}
