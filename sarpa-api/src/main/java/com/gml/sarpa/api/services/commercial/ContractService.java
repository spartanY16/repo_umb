/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContractService.java
 * Created on: Fri Jul 21 15:40:44 COT 2017
 * Project: sarpa
 * Objective: To manage clients contracts
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.commercial;

import com.gml.sarpa.api.entity.commercial.Contract;
import java.util.List;

/**
 * To manage clients contracts
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface ContractService{

    /**
     * List all entities
     * @return
     */
    List<Contract> findAll();

    /**
     * Save entity
     * @param contract
     */
    void save(Contract contract,String userName);

    /**
     * Update entity
     * @param contract
     */
    void update(Contract contract,String userName);

    /**
     * Delete entity
     *@param contract
     */
    void remove(Contract contract,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    Contract getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<Contract> getAllByFilter(String filter);

    
}
