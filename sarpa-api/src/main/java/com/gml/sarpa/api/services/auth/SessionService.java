/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: SessionService.java
 * Created on: 2017/02/10, 10:55:46 PM
 * Project: SARPA
 * Objective: Interface for to define sessions actions.
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.auth;

import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.auth.User;
import java.util.List;

/**
 * Interface for to define sessions actions.
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
public interface SessionService {

    /**
     * List all entities
     * @return
     */
    List<Session> getAll();

    /**
     * Save entity
     * @param session
     */
    void save(Session session);

    /**
     * Update entity
     * @param session
     */
    void update(Session session);

    /**
     * Delete entity
     *@param session
     */
    void remove(Session session);

    /**
     * Find session by user
     * @param user
     * @return
     */
    Session findByUser(User user);

    /**
     * Expire sessions
     * @return
     */
    int expireSessions();
    
    /**
     * Update last ping date for session
     * @return
     */
    public Integer sessionPing(Session session);
}
