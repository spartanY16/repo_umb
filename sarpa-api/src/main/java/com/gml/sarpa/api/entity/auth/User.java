/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: User.java
 * Created on: 2017/02/10, 10:55:46 PM
 * Project: SARPA
 * Objective: To represents a system user.
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.auth;

import com.gml.sarpa.api.entity.people.Person;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 * Entity for represents a system user
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
@Entity
@Table(name = "USERS", schema = "AUTH", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"USERNAME"},name = User.UC_USER_USERNAME)
})
public class User implements Serializable{

    public static final String UC_USER_USERNAME =
        "UC_USER_USERNAME";
    
    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    public Long id;

    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;
    
    /**
     * User username.
     */
    @Column(name = "USERNAME", nullable = false)
    private String username;

    /**
     * User password.
     */
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "PASSWORD")
    private String password;
    
    /**
     * Transient User password confirmation.
     */
    @Transient
    private String confPassword;
    
    /**
     * Person
     */
    @JoinColumn(name = "person_id", nullable = false)
    @ManyToOne
    private Person person;
    
    

    /*
     * -----------------------------------------------------------------------
     */
    
    public User() {
        
    }
       
    /*
     * -----------------------------------------------------------------------
     */


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfPassword() {
        return confPassword;
    }

    public void setConfPassword(String confPassword) {
        this.confPassword = confPassword;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    
}
