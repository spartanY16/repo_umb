/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AccountExecutiveService .java
 * Created on: Fri Jun 30 22:08:06 COT 2017
 * Project: sarpa
 * Objective: To define commercial account executives
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.commercial;

import com.gml.sarpa.api.entity.commercial.AccountExecutive;
import java.util.List;

/**
 * To define commercial account executives
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface AccountExecutiveService{

    /**
     * List all entities
     * @return
     */
    List<AccountExecutive> findAll();

    /**
     * Save entity
     * @param accountExecutive
     */
    void save(AccountExecutive accountExecutive,String userName);

    /**
     * Update entity
     * @param accountExecutive
     */
    void update(AccountExecutive accountExecutive,String userName);

    /**
     * Delete entity
     *@param accountExecutive
     */
    void remove(AccountExecutive accountExecutive,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    AccountExecutive getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<AccountExecutive> getAllByFilter(String filter);

    
}
