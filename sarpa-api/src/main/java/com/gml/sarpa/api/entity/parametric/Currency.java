/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Currency.java
 * Created on: Thu Aug 03 09:42:27 COT 2017
 * Project: sarpa
 * Objective: To manage money currencies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.parametric;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * To manage money currencies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "currency", schema = "parametric", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"code"},name = Currency.UC_CURRENCY_CODE)
})
public class Currency implements Serializable{

    public static final String UC_CURRENCY_CODE="UC_CURRENCY_CODE";
    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    public Long id;

    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * User last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;
    
    /**
    * code
    */
    @Column(name = "code", nullable = false)
    private String code;

    /**
    * name
    */
    @Column(name = "name", nullable = false)
    private String name;

    /**
    * details
    */
    @Column(name = "details", nullable = true)
    private String details;

    /**
    * convFactorCopExt
    */
    @Column(name = "conv_factor_cop_ext", nullable = false)
    private Double convFactorCopExt;

    /**
    * convFactorExtCop
    */
    @Column(name = "conv_factor_ext_cop", nullable = false)
    private Double convFactorExtCop;


    /*
     * -----------------------------------------------------------------------
     */
    
    public Currency() {
        
    }

    /*
     * -----------------------------------------------------------------------
     */

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }
    
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Double getConvFactorCopExt() {
        return convFactorCopExt;
    }

    public void setConvFactorCopExt(Double convFactorCopExt) {
        this.convFactorCopExt = convFactorCopExt;
    }

    public Double getConvFactorExtCop() {
        return convFactorExtCop;
    }

    public void setConvFactorExtCop(Double convFactorExtCop) {
        this.convFactorExtCop = convFactorExtCop;
    }

    /*
     * -----------------------------------------------------------------------
     */

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Currency other = (Currency) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Currency{" + "id=" + id + ", code=" + code + ", name=" + name +
            ", details=" + details + ", convFactorCopExt=" + convFactorCopExt +
            ", convFactorExtCop=" + convFactorExtCop + '}';
    }
    
    
    
    
}
