/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: EmployeeService.java
 * Created on: Tue Jul 18 06:52:02 COT 2017
 * Project: sarpa
 * Objective: To manage employees, crew included
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.people;

import com.gml.sarpa.api.entity.people.Employee;
import java.util.List;

/**
 * To manage employees, crew included
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface EmployeeService{

    /**
     * List all entities
     * @return
     */
    List<Employee> findAll();

    /**
     * Save entity
     * @param employee
     */
    void save(Employee employee,String userName);

    /**
     * Update entity
     * @param employee
     */
    void update(Employee employee,String userName);

    /**
     * Delete entity
     *@param employee
     */
    void remove(Employee employee,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    Employee getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<Employee> getAllByFilter(String filter);

    
}
