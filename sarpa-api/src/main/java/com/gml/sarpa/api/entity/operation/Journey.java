/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Journey.java
 * Created on: Mon Jul 10 16:26:09 COT 2017
 * Project: sarpa
 * Objective: To define journeys times
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.operation;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * To define journeys times
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "journey", schema = "operation", uniqueConstraints = {
   @UniqueConstraint(columnNames = {"base_id","destination_id","airplane_type_id"}
           ,name = Journey.UC_journey),
})
public class Journey implements Serializable{

    public static final String UC_journey =
        "UC_journey";
    
    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    public Long id;

    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * User last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;
    
    /**
    * base
    */
    @JoinColumn(name = "base_id", nullable = false)
    @ManyToOne
    private Airport base;

    /**
    * destination
    */
    @JoinColumn(name = "destination_id", nullable = false)
    @ManyToOne
    private Airport destination;

    /**
    * airplaneType
    */
    @JoinColumn(name = "airplane_type_id", nullable = false)
    @ManyToOne
    private AirplaneType airplaneType;

    /**
    * time
    */
    @Column(name = "time", nullable = false)
    private Double time;

    /**
    * flgInternational
    */
    @Column(name = "flg_international", nullable = false)
    private Boolean flgInternational;
    
    /**
    * code
    */
    @Column(name = "code", nullable = false)
    String code;

    /*
     * -----------------------------------------------------------------------
     */
    
    public Journey() {
        
    }

    /*
     * -----------------------------------------------------------------------
     */

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }
    
    public Airport getBase() {
        return base;
    }

    public void setBase(Airport base) {
        this.base = base;
    }

    public Airport getDestination() {
        return destination;
    }

    public void setDestination(Airport destination) {
        this.destination = destination;
    }

    public AirplaneType getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(AirplaneType airplaneType) {
        this.airplaneType = airplaneType;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    public Boolean getFlgInternational() {
        return flgInternational;
    }

    public void setFlgInternational(Boolean flgInternational) {
        this.flgInternational = flgInternational;
    }

    public String getCode() {
        return code;
    }

    public void setCode() {
        if (null != base){
            this.code = base.getIcaoCode() +"-";
        }
        if (null != destination){
            this.code = this.code + destination.getIcaoCode() +"-";
        }
        if (null != airplaneType){
            this.code = this.code + airplaneType.getName();
        }
    }
    public void setCode(String code) {
        this.code = code;
        
    }

    /*
     * -----------------------------------------------------------------------
     */

    @Override
    public String toString() {
        return "Journey{" + ", id=" + id +
                "userLastUpdated=" + userLastUpdated + ", code=" + 
                code + ", airplaneType=" + 
                airplaneType + ", time=" + time + ", flgInternational"+
                flgInternational+"}";
    }
    
        /*
     * -----------------------------------------------------------------------
     */

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Journey other = (Journey) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    
    
}
