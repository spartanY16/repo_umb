/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Customer.java
 * Created on: Tue Jul 25 20:03:42 COT 2017
 * Project: sarpa
 * Objective: To manage curstomers
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.commercial;

import com.gml.sarpa.api.entity.parametric.BusinessUnit;
import com.gml.sarpa.api.entity.people.Person;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 * To manage curstomers
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "customer", schema = "commercial", uniqueConstraints = {
   @UniqueConstraint(columnNames = {"code"},name = Customer.UC_customer_code),
    @UniqueConstraint(columnNames = {"person_id"},name = Customer.UC_customer_person),
    @UniqueConstraint(columnNames = {"company_id"},name = Customer.UC_customer_company),
})
public class Customer implements Serializable{

    public static final String UC_customer_code =
        "UC_customer_code";
     public static final String UC_customer_person =
        "UC_customer_person";
      public static final String UC_customer_company =
        "UC_customer_company";

    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    private Long id;

    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * User last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;

    /**
    * person
    */
    @JoinColumn(name = "person_id", nullable = true)
    @ManyToOne
    private Person person;

    /**
    * company
    */
    @JoinColumn(name = "company_id", nullable = true)
    @ManyToOne
    private Company company;

    /**
    * accountExecutive
    */
    @JoinColumn(name = "account_executive_id", nullable = true)
    @ManyToOne
    private AccountExecutive accountExecutive;

    /**
    * businessUnit
    */
    @JoinColumn(name = "business_unit_id", nullable = false)
    @ManyToOne
    private BusinessUnit businessUnit;

    /**
    * year
    */
    @Column(name = "year", nullable = false)
    private String year;

    /**
    * status
    */
    @Column(name = "status", nullable = false)
    private String status;

    /**
    * details
    */
    @Column(name = "details", nullable = true)
    private String details;

    /**
    * code
    */
    @Column(name = "code", nullable = false)
    private String code;

    /**
    * name
    */
    @Transient
    private String name;


    /*
     * -----------------------------------------------------------------------
     */
    
    public Customer() {
        
    }

    /*
     * -----------------------------------------------------------------------
     */

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }
    
    public void setDateCreated(Date date) {
        this.dateCreated = date;
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public AccountExecutive getAccountExecutive() {
        return accountExecutive;
    }

    public void setAccountExecutive(AccountExecutive accountExecutive) {
        this.accountExecutive = accountExecutive;
    }

    public BusinessUnit getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(BusinessUnit businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        if (null != person){
            this.name = code + "-" + person.getCompleteName();
        }
        if (null != company){
            this.name = code + "-" + company.getNameAddress();
        }
        return name;
    }

    public void setName() {
        if (null != person){
            this.name = code + "-" + person.getCompleteName();
        }
        if (null != company){
            this.name = code + "-" + company.getNameAddress();
        }
    }

   
    /*
     * -----------------------------------------------------------------------
     */

    @Override
    public String toString() {
        String toString ="Customer {" + 
            "accountExecutive=" + accountExecutive+
            "businessUnit=" + businessUnit+
            "year=" + year+
            "code=" + code+
            "status=" + status+
            "details=" + details + '}';
        if (null != person){
            toString = toString + "person=" + person.getCompleteName();
        }
        if (null != company){
            toString = toString + "company=" + company.getName();
        }
        return toString;
    }

    
}
