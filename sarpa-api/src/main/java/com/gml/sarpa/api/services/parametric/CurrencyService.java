/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CurrencyService.java
 * Created on: Thu Aug 03 09:42:27 COT 2017
 * Project: sarpa
 * Objective: To manage money currencies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.parametric;

import com.gml.sarpa.api.entity.parametric.Currency;
import java.util.List;

/**
 * To manage money currencies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface CurrencyService{

    /**
     * List all entities
     * @return
     */
    List<Currency> findAll();

    /**
     * Save entity
     * @param currency
     */
    void save(Currency currency,String userName);

    /**
     * Update entity
     * @param currency
     */
    void update(Currency currency,String userName);

    /**
     * Delete entity
     *@param currency
     */
    void remove(Currency currency,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    Currency getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<Currency> getAllByFilter(String filter);

    
}
