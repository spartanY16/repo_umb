/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AuditLog .java
 * Created on: Tue Jul 11 16:47:06 COT 2017
 * Project: sarpa
 * Objective: To audit actions over entityes
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.audit;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * To audit actions over entityes
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "audit_log", schema = "audit", uniqueConstraints = {
   
})
public class AuditLog implements Serializable{

    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema="audit", 
        sequenceName = "audit_log_sequence")
    public Long id;

    /**
    * date
    */
    @Column(name = "date", nullable = false)
    private Date date;

    /**
    * user
    */
    @Column(name = "username", nullable = false)
    private String username;

    /**
    * className
    */
    @Column(name = "class_name", nullable = false)
    private String className;

    /**
    * action
    */
    @Column(name = "action", nullable = false)
    private String action;

    /**
    * objectId
    */
    @Column(name = "object_id", nullable = false)
    private Long objectId;

    /**
    * objectVersion
    */
    @Column(name = "object_version", nullable = false)
    private Integer objectVersion;

    /**
    * initial data
    */
    @Column(name = "initial_data", nullable = true)
    private String initialData;
    
    /**
    * property
    */
    @Column(name = "property", nullable = true)
    private String property;

    /**
    * value
    */
    @Column(name = "value", nullable = true)
    private String value;


    /*
     * -----------------------------------------------------------------------
     */
    
    public AuditLog() {
        
    }

    /*
     * -----------------------------------------------------------------------
     */

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Integer getObjectVersion() {
        return objectVersion;
    }

    public void setObjectVersion(Integer objectVersion) {
        this.objectVersion = objectVersion;
    }

    public String getInitialData() {
        return initialData;
    }

    public void setInitialData(String initialData) {
        this.initialData = initialData;
    }
    
    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

   
    /*
     * -----------------------------------------------------------------------
     */

    
    
}
