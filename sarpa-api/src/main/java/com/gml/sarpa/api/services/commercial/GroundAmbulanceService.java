/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: GroundAmbulanceService.java
 * Created on: 2017/09/06, 10:37:03 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.commercial;

import com.gml.sarpa.api.entity.commercial.GroundAmbulance;
import java.util.List;

/**
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
public interface GroundAmbulanceService {
    /**
     * List all entities
     * @return
     */
    List<GroundAmbulance> findAll();

    /**
     * Save entity
     * @param groundAmbulance
     */
    void save(GroundAmbulance groundAmbulance,String userName);

    /**
     * Update entity
     * @param groundAmbulance
     */
    void update(GroundAmbulance groundAmbulance,String userName);

    /**
     * Delete entity
     *@param groundAmbulance
     */
    void remove(GroundAmbulance groundAmbulance,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    GroundAmbulance getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<GroundAmbulance> getAllByFilter(String filter);
}
