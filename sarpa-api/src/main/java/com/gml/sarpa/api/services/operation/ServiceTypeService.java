/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ServiceTypeService .java
 * Created on: Fri Jun 30 23:16:43 COT 2017
 * Project: sarpa
 * Objective: To define services types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.operation;

import com.gml.sarpa.api.entity.operation.ServiceType;
import java.util.List;

/**
 * To define services types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface ServiceTypeService{

    /**
     * List all entities
     * @return
     */
    List<ServiceType> findAll();

    /**
     * Save entity
     * @param serviceType
     */
    void save(ServiceType serviceType,String userName);

    /**
     * Update entity
     * @param serviceType
     */
    void update(ServiceType serviceType,String userName);

    /**
     * Delete entity
     *@param serviceType
     */
    void remove(ServiceType serviceType,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    ServiceType getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<ServiceType> getAllByFilter(String filter);

    
}
