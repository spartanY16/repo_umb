/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirplaneTypeService .java
 * Created on: Thu Jun 29 11:47:51 COT 2017
 * Project: sarpa
 * Objective: To define Airplane Types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.operation;

import com.gml.sarpa.api.entity.operation.AirplaneType;
import java.util.List;

/**
 * To define Airplane Types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface AirplaneTypeService{

    /**
     * List all entities
     * @return
     */
    List<AirplaneType> findAll();

    /**
     * Save entity
     * @param airplaneType
     */
    void save(AirplaneType airplaneType,String userName);

    /**
     * Update entity
     * @param airplaneType
     */
    void update(AirplaneType airplaneType,String userName);

    /**
     * Delete entity
     *@param airplaneType
     */
    void remove(AirplaneType airplaneType,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    AirplaneType getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<AirplaneType> getAllByFilter(String filter);

    
}
