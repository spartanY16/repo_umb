/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Company.java
 * Created on: Wed Jul 19 16:36:43 COT 2017
 * Project: sarpa
 * Objective: To manage companies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.commercial;

import com.gml.sarpa.api.entity.parametric.TypeOfCompanyRelationship;
import com.gml.sarpa.api.entity.people.Contact;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 * To manage companies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "company", schema = "commercial", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"nit", "address"}, name = Company.UC_company)
    ,
    @UniqueConstraint(columnNames = {"code"}, name = Company.UC_company_code),})
public class Company implements Serializable {

    public static final String UC_company
            = "UC_company";

    public static final String UC_company_code
            = "UC_company_code";
    
    public static final String PENDING_VALUE
            = "VALOR PENDIENTE";

    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema = "public",
            sequenceName = "hibernate_sequence")
    private Long id;

    /**
     * Object version
     */
    @Column(name = "version", nullable = false)
    private Integer version;

    /**
     * Date created
     */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;

    /**
     * Date last updated
     */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;

    /**
     * User created
     */
    @Column(name = "user_created", nullable = false)
    private String userCreated;

    /**
     * User last updated
     */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;

    /**
     * name
     */
    @Column(name = "name", nullable = true)
    private String name;

    /**
     * nit
     */
    @Column(name = "nit", nullable = true)
    private String nit;

    /**
     * address
     */
    @Column(name = "address", nullable = true)
    private String address;

    /**
     * phone
     */
    @Column(name = "phone", nullable = true)
    private String phone;

    /**
     * regimeType
     */
    @Column(name = "regime_type", nullable = true)
    private String regimeType;

    /**
     * retentionType
     */
    @Column(name = "retention_type", nullable = true)
    private String retentionType;

    /**
     * detail
     */
    @Column(name = "detail", nullable = true)
    private String detail;

    /**
     * nameAddress
     */
    @Transient
    private String nameAddress;

    /**
     * code
     */
    @Column(name = "code", nullable = false)
    private String code;
    
    /**
     * Email
     */
    @Column(name = "email", nullable = true)
    private String email;
    
    /**
    * contactType
    */
    @JoinColumn(name = "contact_id", nullable = true)
    @ManyToOne
    private Contact contact;
    
    @JoinColumn(name = "type_company_relationship_id", nullable = true)
    @ManyToOne
    private TypeOfCompanyRelationship typeCompanyRelationShip;

    /*
     * -----------------------------------------------------------------------
     */
    public Company() {

    }

    /*
     * -----------------------------------------------------------------------
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion + 1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegimeType() {
        return regimeType;
    }

    public void setRegimeType(String regimeType) {
        this.regimeType = regimeType;
    }

    public String getRetentionType() {
        return retentionType;
    }

    public void setRetentionType(String retentionType) {
        this.retentionType = retentionType;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getNameAddress() {
        return this.name + " - " + this.address;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
    
    public TypeOfCompanyRelationship getTypeCompanyRelationShip() {
        return typeCompanyRelationShip;
    }

    public void setTypeCompanyRelationShip(TypeOfCompanyRelationship typeCompanyRelationShip) {
        this.typeCompanyRelationShip = typeCompanyRelationShip;
    }

    /*
     * -----------------------------------------------------------------------
     */

    @Override
    public String toString() {
        return "Company {"
                + "name=" + name
                + "nit=" + nit
                + "address=" + address
                + "phone=" + phone
                + "regimeType=" + regimeType
                + "code=" + code
                + "retentionType=" + retentionType
                + "detail=" + detail + '}';
    }

}
