/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirportExtensionHourService.java
 * Created on: Thu Sep 28 09:00:31 COT 2017
 * Project: sarpa
 * Objective: To manage airports rates.
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.parametric;

import com.gml.sarpa.api.entity.parametric.AirportExtensionHour;
import java.util.List;

/**
 * To manage airports rates
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */

public interface AirportExtensionHourService{

    /**
     * List all entities
     * @return
     */
    List<AirportExtensionHour> findAll();

    /**
     * Save entity
     * @param airportRate
     */
    void save(AirportExtensionHour airportRate,String userName);

    /**
     * Update entity
     * @param airportRate
     */
    void update(AirportExtensionHour airportRate,String userName);

    /**
     * Delete entity
     *@param airportRate
     */
    void remove(AirportExtensionHour airportRate,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    AirportExtensionHour getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<AirportExtensionHour> getAllByFilter(String filter);

    
}
