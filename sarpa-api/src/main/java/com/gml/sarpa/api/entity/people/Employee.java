/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Employee .java
 * Created on: Tue Jul 18 06:52:02 COT 2017
 * Project: sarpa
 * Objective: To manage employees, crew included
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.people;

import com.gml.sarpa.api.entity.operation.AirplaneType;
import com.gml.sarpa.api.entity.parametric.EmployeePosition;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * To manage employees, crew included
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "employee", schema = "people", uniqueConstraints = {
   @UniqueConstraint(columnNames = {"person_id"},name = Employee .UC_employee_person),
})
public class Employee implements Serializable{

    public static final String UC_employee_person =
        "UC_employee_person";

    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    private Long id;

    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * User last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;

    /**
    * person
    */
    @JoinColumn(name = "person_id", nullable = false)
    @ManyToOne
    private Person person;

    /**
    * airplaneType
    */
    @JoinColumn(name = "airplane_type_id", nullable = true)
    @ManyToOne
    private AirplaneType airplaneType;

    /**
    * type
    */
    @Column(name = "type", nullable = false)
    private String type;

    /**
    * position
    */
    @JoinColumn(name = "position_id", nullable = true)
    @ManyToOne
    private EmployeePosition position;

    /**
    * birthDay
    */
    @Column(name = "birth_day", nullable = true)
    private Date birthDay;

    /**
    * operativeCode
    */
    @Column(name = "operative_code", nullable = true)
    private Integer operativeCode;

    /**
    * contractType
    */
    @Column(name = "contract_type", nullable = true)
    private String contractType;

    /**
    * salary
    */
    @Column(name = "salary", nullable = true)
    private Double salary;

    /**
    * anualBonus
    */
    @Column(name = "anual_bonus", nullable = true)
    private Double anualBonus;

    /**
    * admissionDate
    */
    @Column(name = "admission_date", nullable = true)
    private Date admissionDate;
    
    /**
    * initDate
    */
    @Column(name = "init_date", nullable = true)
    private Date initDate;

    /**
    * retirementDate
    */
    @Column(name = "retirement_date", nullable = true)
    private Date retirementDate;

    /**
    * rh
    */
    @Column(name = "rh", nullable = true)
    private String rh;

    /**
    * medCertificatesExpiration
    */
    @Column(name = "med_certificates_expiration", nullable = true)
    private Date medCertificatesExpiration;

    /**
    * licenseNumber
    */
    @Column(name = "license_number", nullable = true)
    private String licenseNumber;

    /**
    * passportNumber
    */
    @Column(name = "passport_number", nullable = true)
    private String passportNumber;

    /**
    * passportExpiration
    */
    @Column(name = "passport_expiration", nullable = true)
    private Date passportExpiration;

    /**
    * americanVisaNumber
    */
    @Column(name = "american_visa_number", nullable = true)
    private String americanVisaNumber;

    /**
    * americanVisaExpiration
    */
    @Column(name = "american_visa_expiration", nullable = true)
    private Date americanVisaExpiration;

    /**
    * status
    */
    @Column(name = "admin_status", nullable = true)
    private String adminStatus;
    
    /**
    * status
    */
    @Column(name = "fly_status", nullable = true)
    private String flyStatus;


    /*
     * -----------------------------------------------------------------------
     */
    
    public Employee() {
        
    }

    /*
     * -----------------------------------------------------------------------
     */

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public AirplaneType getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(AirplaneType airplaneType) {
        this.airplaneType = airplaneType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public EmployeePosition getPosition() {
        return position;
    }

    public void setPosition(EmployeePosition position) {
        this.position = position;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public Integer getOperativeCode() {
        return operativeCode;
    }

    public void setOperativeCode(Integer operativeCode) {
        this.operativeCode = operativeCode;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Double getAnualBonus() {
        return anualBonus;
    }

    public void setAnualBonus(Double anualBonus) {
        this.anualBonus = anualBonus;
    }

    public Date getAdmissionDate() {
        return admissionDate;
    }

    public void setAdmissionDate(Date adminssionDate) {
        this.admissionDate = adminssionDate;
    }

    public Date getRetirementDate() {
        return retirementDate;
    }

    public void setRetirementDate(Date retirementDate) {
        this.retirementDate = retirementDate;
    }

    public String getRh() {
        return rh;
    }

    public void setRh(String rh) {
        this.rh = rh;
    }

    public Date getMedCertificatesExpiration() {
        return medCertificatesExpiration;
    }

    public void setMedCertificatesExpiration(Date medCertificatesExpiration) {
        this.medCertificatesExpiration = medCertificatesExpiration;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public Date getPassportExpiration() {
        return passportExpiration;
    }

    public void setPassportExpiration(Date passportExpiration) {
        this.passportExpiration = passportExpiration;
    }

    public String getAmericanVisaNumber() {
        return americanVisaNumber;
    }

    public void setAmericanVisaNumber(String americanVisaNumber) {
        this.americanVisaNumber = americanVisaNumber;
    }

    public Date getAmericanVisaExpiration() {
        return americanVisaExpiration;
    }

    public void setAmericanVisaExpiration(Date americanVisaExpiration) {
        this.americanVisaExpiration = americanVisaExpiration;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public String getAdminStatus() {
        return adminStatus;
    }

    public void setAdminStatus(String adminStatus) {
        this.adminStatus = adminStatus;
    }

    public String getFlyStatus() {
        return flyStatus;
    }

    public void setFlyStatus(String flyStatus) {
        this.flyStatus = flyStatus;
    }

    /*
     * -----------------------------------------------------------------------
     */

    @Override
    public String toString() {
        return "Employee {" + 
            "person=" + person.toString()+
            "airplaneType=" + airplaneType.toString()+
            "type=" + type+
            "position=" + position.getName()+
            "birthDay=" + birthDay+
            "operativeCode=" + operativeCode+
            "contractType=" + contractType+
            "salary=" + salary+
            "anualBonus=" + anualBonus+
            "adminssionDate=" + admissionDate+
            "initDate=" + initDate+
            "retirementDate=" + retirementDate+
            "rh=" + rh+
            "medCertificatesExpiration=" + medCertificatesExpiration+
            "licenseNumber=" + licenseNumber+
            "passportNumber=" + passportNumber+
            "passportExpiration=" + passportExpiration+
            "americanVisaNumber=" + americanVisaNumber+
            "americanVisaExpiration=" + americanVisaExpiration+
            "adminStatus=" + adminStatus+"flyStatus=" + flyStatus+
        + '}';
    }

    
}
