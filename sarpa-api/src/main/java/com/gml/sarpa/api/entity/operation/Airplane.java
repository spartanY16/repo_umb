/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Airplane .java
 * Created on: Thu Jun 29 15:09:23 COT 2017
 * Project: sarpa
 * Objective: To define Airplans
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.operation;

import com.gml.sarpa.api.entity.parametric.BusinessUnit;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * To define Airplans
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Entity
@Table(name = "airplane", schema = "operation", uniqueConstraints = {
   @UniqueConstraint(columnNames = {"name"},name = Airplane.UC_Airplane_name),
})
public class Airplane implements Serializable{

    public static final String UC_Airplane_name =
        "UC_Airplane_name";
    
    /**
     * Object Id.
     */
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    public Long id;

    /**
    * Object version
    */
    @Column(name = "version", nullable = false)
    private Integer version;
    
    /**
    * Date created
    */
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;
    
    /**
    * Date last updated
    */
    @Column(name = "date_last_updated", nullable = false)
    private Date dateLastUpdated;
    
    /**
    * User created
    */
    @Column(name = "user_created", nullable = false)
    private String userCreated;
    
    /**
    * User last updated
    */
    @Column(name = "user_last_updated", nullable = false)
    private String userLastUpdated;
    
    /**
    * name
    */
    @Column(name = "name", nullable = false)
    private String name;

    /**
    * airplaneType
    */
    @JoinColumn(name = "airplane_type_id", nullable = false)
    @ManyToOne
    private AirplaneType airplaneType;

    /**
    * passengersNumber
    */
    @Column(name = "passengers_number", nullable = false)
    private Integer passengersNumber;

    /**
    * stretchersNumber
    */
    @Column(name = "stretchers_number", nullable = false)
    private Integer stretchersNumber;

    /**
    * businessUnit
    */
    @JoinColumn(name = "business_unit_id", nullable = false)
    @ManyToOne
    private BusinessUnit businessUnit;

    
    /**
    * loadingCapacity
    */
    @Column(name = "loading_capacity", nullable = true)
    private Long loadingCapacity;
    
   /**
    * grossWeight
    */
    @Column(name = "gross_weight", nullable = true)
    private Long grossWeight;
    


    /*
     * -----------------------------------------------------------------------
     */
    
    public Airplane() {
        
    }

    /*
     * -----------------------------------------------------------------------
     */

     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer currentVersion) {
        this.version = currentVersion+1;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated() {
        this.dateCreated = new Date();
    }

    public Date getDateLastUpdated() {
        return dateLastUpdated;
    }

    public void setDateLastUpdated() {
        this.dateLastUpdated = new Date();
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserLastUpdated() {
        return userLastUpdated;
    }

    public void setUserLastUpdated(String userLastUpdated) {
        this.userLastUpdated = userLastUpdated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AirplaneType getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(AirplaneType airplaneType) {
        this.airplaneType = airplaneType;
    }

    public Integer getPassengersNumber() {
        return passengersNumber;
    }

    public void setPassengersNumber(Integer passengersNumber) {
        this.passengersNumber = passengersNumber;
    }

    public Integer getStretchersNumber() {
        return stretchersNumber;
    }

    public void setStretchersNumber(Integer stretchersNumber) {
        this.stretchersNumber = stretchersNumber;
    }

    public BusinessUnit getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(BusinessUnit businessUnit) {
        this.businessUnit = businessUnit;
    }

    public Long getLoadingCapacity() {
        return loadingCapacity;
    }

    public void setLoadingCapacity(Long loadingCapacity) {
        this.loadingCapacity = loadingCapacity;
    }

    public Long getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(Long grossWeight) {
        this.grossWeight = grossWeight;
    }
    
    
   
    /*
     * -----------------------------------------------------------------------
     */

    @Override
    public String toString() {
        return "Airplane{" + "name=" + name + ", airplaneType=" + airplaneType + 
                ", passengersNumber=" + passengersNumber + ", stretchersNumber=" 
                + stretchersNumber + ", businessUnit=" + businessUnit 
                + ", loadingCapacity=" + loadingCapacity
                + ", grossWeight=" + grossWeight
                + '}';
    }

    
    
    
}
