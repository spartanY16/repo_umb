/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: Session.java
 * Created on: 2017/02/10, 10:55:46 PM
 * Project: SARPA
 * Objective: To represents a system session user.
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.entity.auth;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity for represents a system session user
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
@Table(name = "SESSIONS", schema = "AUTH")
@Entity
public class Session implements Serializable {

    /**
     * Entity id
     */
    @Id
    @Column(name = "ID")
     @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
        "generator_id")
    @SequenceGenerator(name = "generator_id", schema="public", 
        sequenceName = "hibernate_sequence")
    private Long id;
    
    /**
     * User
     */
    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private User user;

    /**
     * Log in date
     */
    @Column(name = "LOGIN_DATE")
    private Date loginDate;
    
    /**
     * Last operation date
     */
    @Column(name = "LAST_OPERATION_DATE")
    private Date lastOperationDate;
    
    /**
     * Last client ping date
     */
    @Column(name = "LAST_PING_DATE")
    private Date lastPingDate;
    
    
    /*--------------------------------------------------------*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public Date getLastOperationDate() {
        return lastOperationDate;
    }

    public void setLastOperationDate(Date lastOperationDate) {
        this.lastOperationDate = lastOperationDate;
    }

    public Date getLastPingDate() {
        return lastPingDate;
    }

    public void setLastPingDate(Date lastPingDate) {
        this.lastPingDate = lastPingDate;
    }

    

    /*--------------------------------------------------------*/

    @Override
    public String toString() {
        return "Session{" + "id=" + id + ", user=" + user + 
                ", loginDate=" + loginDate + 
                ", lastOperationDate=" + lastOperationDate + 
                ", lastPingDate=" + lastPingDate + '}';
    }

    
    
}
