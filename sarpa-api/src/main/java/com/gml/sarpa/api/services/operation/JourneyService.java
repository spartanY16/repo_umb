/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: JourneyService.java
 * Created on: Mon Jul 10 16:26:09 COT 2017
 * Project: sarpa
 * Objective: To define journeys times
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.operation;

import com.gml.sarpa.api.entity.operation.Journey;
import java.util.List;

/**
 * To define journeys times
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface JourneyService{

    /**
     * List all entities
     * @return
     */
    List<Journey> findAll();

    /**
     * Save entity
     * @param journey
     */
    void save(Journey journey,String userName);

    /**
     * Update entity
     * @param journey
     */
    void update(Journey journey,String userName);

    /**
     * Delete entity
     *@param journey
     */
    void remove(Journey journey,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    Journey getOne(Long id);

    /**
     * Search entity by filter
     * @    /**
        param filter
     * @return
     */
    List<Journey> getAllByFilter(String filter);
    
    /*
     * Search List entity by Id
     * @param idAirplaneType
     * @return
     */
    List<Journey> getJorneysByAirlineType(Long idAirplaneType, Long flagIdAirplaneType);
    
}
