/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FBOCostService.java
 * Created on: Thu Aug 03 09:42:27 COT 2017
 * Project: sarpa
 * Objective: To manage fbo costs.
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.parametric;

import com.gml.sarpa.api.entity.parametric.FBOCost;
import java.util.List;

/**
 * To manage fbo costs
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */

public interface FBOCostService{

    /**
     * List all entities
     * @return
     */
    List<FBOCost> findAll();

    /**
     * Save entity
     * @param fboCost
     */
    void save(FBOCost fboCost,String userName);

    /**
     * Update entity
     * @param fboCost
     */
    void update(FBOCost fboCost,String userName);

    /**
     * Delete entity
     *@param fboCost
     */
    void remove(FBOCost fboCost,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    FBOCost getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<FBOCost> getAllByFilter(String filter);

    
}
