    /*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: AirAmbulanceService.java
 * Created on: 2017/09/06, 10:37:03 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.api.services.commercial;

import com.gml.sarpa.api.entity.commercial.AirAmbulance;
import java.util.List;

/**
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
public interface AirAmbulanceService {
    /**
     * List all entities
     * @return
     */
    List<AirAmbulance> findAll();

    /**
     * Save entity
     * @param company
     */
    void save(AirAmbulance company,String userName);

    /**
     * Update entity
     * @param company
     */
    void update(AirAmbulance company,String userName);

    /**
     * Delete entity
     *@param company
     */
    void remove(AirAmbulance company,String userName);

    /**
     * Get entity by id
     * @param id
     * @return
     */
    AirAmbulance getOne(Long id);

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    List<AirAmbulance> getAllByFilter(String filter);
}
