/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirplaneTypeRepository .java
 * Created on: Thu Jun 29 11:47:51 COT 2017
 * Project: sarpa
 * Objective: To define Airplane Types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.operation;

import com.gml.sarpa.api.entity.operation.AirplaneType;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To define Airplane Types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface AirplaneTypeRepository extends JpaRepository<AirplaneType, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from operation.airplane_type  where name ilike %?1% ", nativeQuery = true)
    public List<AirplaneType> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from operation.airplane_type where id = ?1 ", nativeQuery = true)
    public AirplaneType get(Long id);
}
