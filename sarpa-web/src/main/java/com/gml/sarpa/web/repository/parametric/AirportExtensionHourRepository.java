/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirportExtensionHourRepository.java
 * Created on: Thu Aug 03 09:42:27 COT 2017
 * Project: sarpa
 * Objective: To manage money currencies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.parametric;

import com.gml.sarpa.api.entity.parametric.AirportExtensionHour;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To manage airports rates.
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */

public interface AirportExtensionHourRepository extends JpaRepository<AirportExtensionHour, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from parametric.airport_ext_hour  where name ilike %?1% ", nativeQuery = true)
    public List<AirportExtensionHour> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from parametric.airport_ext_hour where id = ?1 ", nativeQuery = true)
    public AirportExtensionHour get(Long id);
    
}
