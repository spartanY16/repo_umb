/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CurrencyServiceImpl.java
 * Created on: Thu Aug 03 09:42:27 COT 2017
 * Project: sarpa
 * Objective: To manage money currencies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.parametric;

import com.gml.sarpa.api.entity.parametric.Currency;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.parametric.CurrencyService;
import com.gml.sarpa.web.repository.parametric.CurrencyRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To manage money currencies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "CurrencyService")
public class CurrencyServiceImpl implements CurrencyService{

    /**
     * Repository for data access
     */
    @Autowired
    private CurrencyRepository repository;
    
    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;
    

    /**
     * List all entities
     * @return
     */
    @Override
    public List<Currency> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param currency
     */
    @Override
    public void save(Currency currency,String userName){
        currency.setVersion(0);
        currency.setUserCreated(userName);
        currency.setDateCreated();
        currency.setUserLastUpdated(userName);
        currency.setDateLastUpdated();
        repository.save(currency);
        auditLogService.auditInsert(currency.getClass().getName(),
                currency.getId(),currency.getVersion(),
                currency.toString(),userName);
    }

    /**
     * Update entity
     * @param currency
     */
    @Override
    public void update(Currency currency,String userName){
        Currency currentCurrency = getOne(currency.getId());
        currency.setVersion(currentCurrency.getVersion());
        currency.setUserLastUpdated(userName);
        currency.setDateLastUpdated();
        currency.setDateCreated(currentCurrency.getDateCreated());
        currency.setUserCreated(currentCurrency.getUserCreated());
        repository.save(currency);
        if(!currentCurrency.getName().equals(currency.getName())){
            auditLogService.auditUpdate(currency.getClass().getName(),
            currency.getId(),currency.getVersion(),
            currentCurrency.toString(),"name",currency.getName(),userName);
        }
    }

    /**
     * Delete entity
     *@param currency
     */
    @Override
    public void remove(Currency currency,String userName){
        repository.delete(currency);
        auditLogService.auditDelete(currency.getClass().getName(),
                currency.getId(),currency.getVersion(),
                currency.toString(),userName);
    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public Currency getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<Currency> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
