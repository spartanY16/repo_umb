/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CompanyServiceImpl.java
 * Created on: Wed Jul 19 16:36:43 COT 2017
 * Project: sarpa
 * Objective: To manage companies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.commercial;

import com.gml.sarpa.api.entity.commercial.Company;
import com.gml.sarpa.api.services.commercial.CompanyService;
import com.gml.sarpa.web.repository.commercial.CompanyRepository;
import com.gml.sarpa.api.services.audit.AuditLogService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To manage companies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "CompanyService")
public class CompanyServiceImpl implements CompanyService{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(CompanyServiceImpl.class);
    
    /**
     * Repository for data access
     */
    @Autowired
    private CompanyRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<Company> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param company 
     */
    @Override
    public void save(Company  company ,String userName){
        company.setVersion(0);
        company.setUserCreated(userName);
        company.setDateCreated();
        company.setUserLastUpdated(userName);
        company.setDateLastUpdated();
        company.setCode("COM-"+repository.getNextCode());
        repository.save(company );
        auditLogService.auditInsert(company.getClass().getName(),
                company.getId(),company.getVersion(),
                company.toString(),userName);
    }

    /**
     * Update entity
     * @param company 
     */
    @Override
    public void update(Company  company ,String userName){
        Company  currentCompany  = getOne(company.getId());
        company.setVersion(currentCompany.getVersion());
        company.setUserLastUpdated(userName);
        company.setDateLastUpdated();
        company.setDateCreated(currentCompany.getDateCreated());
        company.setUserCreated(currentCompany.getUserCreated());
        repository.save(company );
        if(!currentCompany.getName().equals(company.getName())){
            auditLogService.auditUpdate(company.getClass().getName(),
            company.getId(),company.getVersion(),
            currentCompany.toString(),"name",company.getName(),userName);
        }
    }

    /**
     * Delete entity
     *@param company 
     */
    @Override
    public void remove(Company  company ,String userName){
        repository.delete(company );
        auditLogService.auditDelete(company.getClass().getName(),
                company.getId(),company.getVersion(),
                company.toString(),userName);
    }

    

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public Company getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<Company> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
