/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContactRepository .java
 * Created on: Fri Jun 30 22:30:20 COT 2017
 * Project: sarpa
 * Objective: To define persons contact data
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.people;

import com.gml.sarpa.api.entity.people.Contact;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To define persons contact data
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface ContactRepository extends JpaRepository<Contact, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from people.contact  where name ilike %?1% ", nativeQuery = true)
    public List<Contact> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from people.contact where id = ?1 ", nativeQuery = true)
    public Contact get(Long id);
    
    /**
     * Search entity by value
     * @param value
     * @return 
     */
    @Query(value =
        "select cont.* " +
        "from people.contact cont " +
        "join people.person pers " +
        "on cont.person_id = pers.id " +
        "where " +
        "(0 = ?2 or " +
        "cont.value ilike %?1% or " +
        "pers.first_name ilike %?1% or " +
        "pers.last_name ilike %?1%) " +
        "order by pers.first_name, pers.last_name ", nativeQuery = true)
    List<Contact> getContactsParams(String value, Long fillValue);
}
