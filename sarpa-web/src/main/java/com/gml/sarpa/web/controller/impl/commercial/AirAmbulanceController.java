/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: AirAmbulanceController.java
 * Created on: 2017/09/06, 10:56:09 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.commercial;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.commercial.AirAmbulance;
import com.gml.sarpa.api.services.commercial.AirAmbulanceService;
import com.gml.sarpa.api.utilities.Util;
import com.gml.sarpa.web.controller.ControllerDefinition;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;

/**
 *
 * @author <a href="mailto:josej@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
@Controller
public class AirAmbulanceController implements ControllerDefinition {
    
    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(AirAmbulanceController.class);

    /**
     * Session user information
     */
    private Session userSession;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private AirAmbulanceService airAmbulanceService;

    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/airAmbulance-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session,
            RedirectAttributes redirectAttributes) {

        LOGGER.info("Runing [ AirAmbulanceController - onDisplay ]");
        init(request);
        ModelAndView modelAndView = new ModelAndView("commercial/airAmbulance/index");

        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType", flashMap.get("flashType"));
            modelAndView.addObject("flashMessage", flashMap.get("flashMessage"));
        }

        modelAndView.addObject("indexList", this.airAmbulanceService.findAll());

        return modelAndView;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/airAmbulance-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session) {

        LOGGER.info("Runing [ AirAmbulanceController - create ]");
        init(request);
        ModelAndView modelAndView = new ModelAndView("commercial/airAmbulance/create");

        AirAmbulance airAmbulance = new AirAmbulance();
        modelAndView.addObject("airAmbulance", airAmbulance);
        return modelAndView;
    }

    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/airAmbulance-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("airAmbulance") AirAmbulance airAmbulance) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ AirAmbulanceController - save ]");
        init(request);
        try {
            airAmbulanceService.save(airAmbulance, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "commercial.airAmbulance.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.airAmbulance.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/airAmbulance-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session,
            RedirectAttributes redirectAttributes,
            @RequestParam(value = "id", required = true) Long id) {

        LOGGER.info("Runing [ AirAmbulanceController - edit ] id " + id);
        ModelAndView modelAndView = new ModelAndView("commercial/airAmbulance/edit");
        init(request);
        AirAmbulance airAmbulance = airAmbulanceService.getOne(id);

        if (airAmbulance != null) {
            modelAndView.addObject("airAmbulance", airAmbulance);

        } else {
            redirectAttributes.addFlashAttribute("flashType", "danger");
            redirectAttributes.addFlashAttribute("flashMessage", "commercial.airAmbulance.edit.success");
            return new ModelAndView("redirect:/airAmbulance-index.htm");
        }
        return modelAndView;

    }

    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/airAmbulance-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("airAmbulance") AirAmbulance airAmbulance) {

        LOGGER.info("Runing [ AirAmbulanceController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            airAmbulanceService.update(airAmbulance, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "commercial.airAmbulance.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.airAmbulance.update.error", null, locale));

        }

        return Util.getJson(result);
    }

    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/airAmbulance-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
            @RequestParam(value = "id", required = true) Long id) {

        LOGGER.info("Runing [ BusinessUnitController - delete ] id " + id);
        ModelAndView modelAndView = new ModelAndView("redirect:/airAmbulance-index.htm");
        init(request);
        AirAmbulance airAmbulance = airAmbulanceService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (airAmbulance != null) {
            try {
                airAmbulanceService.remove(airAmbulance, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "commercial.airAmbulance.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "commercial.airAmbulance.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("commercial.airAmbulance.delete.error");
        }
        return Util.getJson(result);

    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }

}
