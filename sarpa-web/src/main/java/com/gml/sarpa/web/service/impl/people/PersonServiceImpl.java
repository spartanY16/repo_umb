/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PersonServiceImpl .java
 * Created on: Fri Jun 30 22:34:42 COT 2017
 * Project: sarpa
 * Objective: To define persons contact data
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.people;

import com.gml.sarpa.api.entity.people.Person;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.people.PersonService;
import com.gml.sarpa.web.repository.people.PersonRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To define persons contact data
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "PersonService")
public class PersonServiceImpl implements PersonService{

    /**
     * Repository for data access
     */
    @Autowired
    private PersonRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;
    
    /**
     * List all entities
     * @return
     */
    @Override
    public List<Person> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param person
     */
    @Override
    public void save(Person person,String userName){
        person.setVersion(0);
        person.setUserCreated(userName);
        person.setDateCreated();
        person.setUserLastUpdated(userName);
        person.setDateLastUpdated();
        repository.save(person);
        auditLogService.auditInsert(person.getClass().getName(),
                person.getId(),person.getVersion(),
                person.toString(),userName);
    }

    /**
     * Update entity
     * @param person
     */
    @Override
    public void update(Person person,String userName){
        Person currentPerson = getOne(person.getId());
        person.setVersion(currentPerson.getVersion());
        person.setUserLastUpdated(userName);
        person.setDateLastUpdated();
        repository.save(person);
        if(!currentPerson.getDocumentNumber().equals(person.getDocumentNumber())){
            auditLogService.auditUpdate(person.getClass().getName(),
            person.getId(),person.getVersion(),
            currentPerson.toString(),"documentNumber",person.getDocumentNumber(),userName);
        }
        if(!currentPerson.getDocumentType().equals(person.getDocumentType())){
            auditLogService.auditUpdate(person.getClass().getName(),
            person.getId(),person.getVersion(),
            currentPerson.toString(),"documentType",person.getDocumentType(),userName);
        }
        if(!currentPerson.getFirstName().equals(person.getFirstName())){
            auditLogService.auditUpdate(person.getClass().getName(),
            person.getId(),person.getVersion(),
            currentPerson.toString(),"firstName",person.getFirstName(),userName);
        }
        if(!currentPerson.getLastName().equals(person.getLastName())){
            auditLogService.auditUpdate(person.getClass().getName(),
            person.getId(),person.getVersion(),
            currentPerson.toString(),"lastName",person.getLastName(),userName);
        }
    }

    /**
     * Delete entity
     *@param person
     */
    @Override
    public void remove(Person person,String userName){
        repository.delete(person);
        auditLogService.auditDelete(person.getClass().getName(),
                person.getId(),person.getVersion(),
                person.toString(),userName);
    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public Person getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<Person> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
