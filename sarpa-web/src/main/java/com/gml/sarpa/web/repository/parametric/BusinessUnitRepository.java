/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: BusinessUnitRepository .java
 * Created on: Thu Jun 29 15:22:59 COT 2017
 * Project: sarpa
 * Objective: To define business units
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.parametric;

import com.gml.sarpa.api.entity.parametric.BusinessUnit;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To define business units
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface BusinessUnitRepository extends JpaRepository<BusinessUnit, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from parametric.business_unit  where name ilike %?1% ", nativeQuery = true)
    public List<BusinessUnit> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from parametric.business_unit where id = ?1 ", nativeQuery = true)
    public BusinessUnit get(Long id);
}
