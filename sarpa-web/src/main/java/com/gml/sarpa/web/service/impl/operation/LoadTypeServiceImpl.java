/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: LoadTypeServiceImpl .java
 * Created on: Fri Jun 30 23:16:25 COT 2017
 * Project: sarpa
 * Objective: To define load types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.operation;

import com.gml.sarpa.api.entity.operation.LoadType;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.operation.LoadTypeService;
import com.gml.sarpa.web.repository.operation.LoadTypeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To define load types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "LoadTypeService")
public class LoadTypeServiceImpl implements LoadTypeService{

    /**
     * Repository for data access
     */
    @Autowired
    private LoadTypeRepository repository;
    
    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<LoadType> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param loadType
     */
    @Override
    public void save(LoadType loadType,String userName){
        loadType.setVersion(0);
        loadType.setUserCreated(userName);
        loadType.setDateCreated();
        loadType.setUserLastUpdated(userName);
        loadType.setDateLastUpdated();
        repository.save(loadType);
        auditLogService.auditInsert(loadType.getClass().getName(),
                loadType.getId(),loadType.getVersion(),
                loadType.toString(),userName);
    }

    /**
     * Update entity
     * @param loadType
     */
    @Override
    public void update(LoadType loadType,String userName){
        LoadType currentLoadType = getOne(loadType.getId());
        loadType.setVersion(currentLoadType.getVersion());
        loadType.setUserLastUpdated(userName);
        loadType.setDateLastUpdated();
        repository.save(loadType);
        if(!currentLoadType.getName().equals(loadType.getName())){
            auditLogService.auditUpdate(loadType.getClass().getName(),
            loadType.getId(),loadType.getVersion(),
            currentLoadType.toString(),"name",loadType.getName(),userName);
        }
    }

    /**
     * Delete entity
     *@param loadType
     */
    @Override
    public void remove(LoadType loadType,String userName){
        repository.delete(loadType);
        auditLogService.auditDelete(loadType.getClass().getName(),
                loadType.getId(),loadType.getVersion(),
                loadType.toString(),userName);
    }


    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public LoadType getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<LoadType> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
