/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: UserServiceImpl.java
 * Created on: 2017/02/10, 10:55:46 PM
 * Project: SARPA
 * Objective: Interface to implements user services
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.auth;

import com.gml.sarpa.api.entity.auth.User;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.auth.UserService;
import com.gml.sarpa.web.repository.auth.UserRepository;
import org.apache.log4j.Logger;
import com.gml.sarpa.api.utilities.Util;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service to implements user services
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "userService")
public class UserServiceImpl implements UserService {

    /**
     * Logger para la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

    /**
     * Servicio de repositorio para los procesos de carga
     */
    @Autowired
    private UserRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;
    
    /*-------------------------------------------------------------------------*/
    
    /**
     * Save entity
     * @param user
     */
    @Override
    public void save(User user,String userName){
        user.setVersion(0);
        user.setUserCreated(userName);
        user.setDateCreated();
        user.setUserLastUpdated(userName);
        user.setDateLastUpdated();
        user.setPassword(Util.calculateSha1(user.getPassword()));
        repository.save(user);
        auditLogService.auditInsert(user.getClass().getName(),
                user.getId(),user.getVersion(),
                user.toString(),userName);
    }

    /**
     * Update entity
     * @param user
     */
    @Override
    public void update(User user,String userName){
        User currentUser = getOne(user.getId());
        user.setVersion(currentUser.getVersion());
        user.setUserLastUpdated(userName);
        user.setDateLastUpdated();
        user.setPassword(Util.calculateSha1(user.getPassword()));
        repository.save(user);
        if(!currentUser.getPerson().equals(user.getPerson())){
            auditLogService.auditUpdate(user.getClass().getName(),
            user.getId(),user.getVersion(),
            currentUser.toString(),"person",user.getPerson().getCompleteName(),userName);
        }
    }

    /**
     * Delete entity
     *@param user
     */
    @Override
    public void remove(User user,String userName){
        repository.delete(user);
        auditLogService.auditDelete(user.getClass().getName(),
                user.getId(),user.getVersion(),
                user.toString(),userName);
    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public User getOne(Long id){
        return repository.get(id);
    }
    
    @Override
    public User findByUsername(String username) {
         LOGGER.info("Executing method [ findByUsername of userServiceImpl] " +
             "for username "+username);
        return this.repository.findByUsername(username);
    }

    @Override
    public User findByUsernameAndPassword(User user) {
        LOGGER.info("Executing method [ findByUsernameAndPassword of userServiceImpl] " +
             "for username "+user.getUsername());
        return this.repository.
                    findByUsernameAndPassword(user.getUsername(), 
                            Util.calculateSha1(user.getPassword()));
    }

    @Override
    public List<User> getAllByFilter(String filter) {
        return repository.getAllByFilter(filter);
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }
    
    


}
