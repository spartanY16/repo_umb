/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContractController.java
 * Created on: Fri Jul 21 15:40:44 COT 2017
 * Project: sarpa
 * Objective: To manage customers contracts
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.commercial;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.web.controller.ControllerDefinition;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.commercial.Contract;
import com.gml.sarpa.api.services.commercial.CustomerService;
import com.gml.sarpa.api.services.commercial.ContractService;
import com.gml.sarpa.api.services.commercial.ContractTypeService;
import com.gml.sarpa.api.utilities.Util;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * To manage customers contracts
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class ContractController implements ControllerDefinition{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(ContractController.class);

    /**
     * Session user information
     */
    private Session userSession;

    /**
     * Contract service
     */
    @Autowired
    private ContractService entityService;
    
    @Autowired
    private MessageSource messageSource;
    
    /**
     * ContractValue service
     */
    @Autowired
    private ContractTypeService contractTypeService;
    
    /**
     * ContractValue service
     */
    @Autowired
    private CustomerService customerService;


    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/contract-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response, Locale locale, HttpSession session,
        RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Runing [ ContractController - onDisplay ]");
        ModelAndView modelAndView = new ModelAndView("commercial/contract/index");
        init(request);
        
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType",flashMap.get("flashType"));
            modelAndView.addObject("flashMessage",flashMap.get("flashMessage"));
        }
        
        modelAndView.addObject("indexList",this.entityService.findAll());

        return modelAndView ;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/contract-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ ContractController - create ]");
        ModelAndView modelAndView = new ModelAndView("commercial/contract/create");
        init(request);
        
        Contract contract = new Contract();
        modelAndView.addObject("contract", contract);
        modelAndView.addObject("contractValueList", contractTypeService.findAll());
        modelAndView.addObject("customerList", customerService.findAll());
        return modelAndView;
    }
    
    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/contract-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("contract") Contract contract) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ ContractController - save ]");
        init(request);
        try {
            entityService.save(contract, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "parametric.contract.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "parametric.contract.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    
    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/contract-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        RedirectAttributes redirectAttributes,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ ContractController - edit ] id "+id);
        ModelAndView modelAndView = new ModelAndView("commercial/contract/edit");
        init(request);
        
        Contract contract = entityService.getOne(id);

        if (contract != null){
            modelAndView.addObject("contract", contract);
            modelAndView.addObject("contractValueList", contractTypeService.findAll());
            modelAndView.addObject("customerList", customerService.findAll());
        }else{
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","commercial.contract.edit.success");
            return new ModelAndView("redirect:/contract-index.htm");
        }
        return modelAndView;
        
    }
    
    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/contract-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("contract") Contract contract) {

        LOGGER.info("Runing [ ContractController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            entityService.update(contract, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "parametric.contract.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "parametric.contract.update.error", null, locale));

        }

        return Util.getJson(result);
    }


    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/contract-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ ContractController - delete ] id "+id);
        init(request);
        
        Contract object = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (object != null) {
            try {
                entityService.remove(object, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "parametric.contract.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "parametric.contract.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("parametric.contract.delete.error");
        }
        return Util.getJson(result);
        
    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }
}
