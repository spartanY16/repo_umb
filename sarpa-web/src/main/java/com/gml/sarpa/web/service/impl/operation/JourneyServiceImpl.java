/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: JourneyServiceImpl.java
 * Created on: Mon Jul 10 16:26:09 COT 2017
 * Project: sarpa
 * Objective: To define journeys times
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.operation;

import com.gml.sarpa.api.entity.operation.Airport;
import com.gml.sarpa.api.entity.operation.Journey;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.operation.AirplaneTypeService;
import com.gml.sarpa.api.services.operation.AirportService;
import com.gml.sarpa.api.services.operation.JourneyService;
import com.gml.sarpa.web.repository.operation.JourneyRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To define journeys times
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "JourneyService")
public class JourneyServiceImpl implements JourneyService{

    /**
     * Repository for data access
     */
    @Autowired
    private JourneyRepository repository;
    
    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

     /**
     * Service for audit actions
     */
    @Autowired
    private AirportService airportService;
    
     /**
     * Service for airplaneType
     */
    @Autowired
    private AirplaneTypeService airplaneTypeService;
    
    /**
     * List all entities
     * @return
     */
    @Override
    public List<Journey> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param journey
     */
    @Override
    public void save(Journey journey,String userName){
        journey.setVersion(0);
        journey.setUserCreated(userName);
        journey.setDateCreated();
        journey.setUserLastUpdated(userName);
        journey.setDateLastUpdated();
        journey.setBase(airportService.getOne(journey.getBase().getId()));
        journey.setDestination(airportService.getOne(journey.getDestination().getId()));
        journey.setFlgInternational(isInternational(journey));
        journey.setBase(airportService.getOne(journey.getBase().getId()));
        journey.setDestination(airportService.getOne(journey.getDestination().getId()));
        journey.setAirplaneType(airplaneTypeService.getOne(journey.getAirplaneType().getId()));
        journey.setCode();
        repository.save(journey);
        auditLogService.auditInsert(journey.getClass().getName(),
                journey.getId(),journey.getVersion(),
                journey.toString(),userName);
    }

    /**
     * Update entity
     * @param journey
     */
    @Override
    public void update(Journey journey,String userName){
        Journey currentJourney = getOne(journey.getId());
        journey.setVersion(currentJourney.getVersion());
        journey.setUserLastUpdated(userName);
        journey.setDateLastUpdated();
        journey.setCode(currentJourney.getCode());
        journey.setBase(airportService.getOne(journey.getBase().getId()));
        journey.setDestination(airportService.getOne(journey.getDestination().getId()));
        journey.setFlgInternational(isInternational(journey));
        repository.save(journey);
        if(!currentJourney.getBase().getName().equals(journey.getBase().getName())){
            auditLogService.auditUpdate(journey.getClass().getName(),
            journey.getId(),journey.getVersion(),
            currentJourney.toString(),"base",journey.getBase().getName(),userName);
        }
    }

    /**
     * Delete entity
     *@param journey
     */
    @Override
    public void remove(Journey journey,String userName){
        repository.delete(journey);
        auditLogService.auditDelete(journey.getClass().getName(),
                journey.getId(),journey.getVersion(),
                journey.toString(),userName);
    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public Journey getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<Journey> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }
    
    /**
     *
     * @param idAirplaneType
     * @param flagIdAirplaneType
     * @return
     */
    @Override
    public List<Journey> getJorneysByAirlineType(Long idAirplaneType, Long flagIdAirplaneType){
        return repository.getJorneysByAirlineType(idAirplaneType, flagIdAirplaneType);
    }

    private boolean isInternational(Journey journey){
        Airport base = airportService.getOne(journey.getBase().getId());
        Airport destination = airportService.getOne(journey.getDestination().getId());
        return (!base.getCountry().equals("Colombia") || !destination.getCountry().equals("Colombia"));
    }
    
}
