/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: LoginController.java
 * Created on: 2017/02/10, 10:55:46 PM
 * Project: SARPA
 * Objective: Controller for login actions.
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.auth;


import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.auth.User;
import com.gml.sarpa.api.services.auth.SessionService;
import com.gml.sarpa.api.services.auth.UserService;
import com.gml.sarpa.web.controller.ControllerDefinition;
import java.io.IOException;
import java.util.Date;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;

/**
 * Controller for login actions
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica Jim&eacute;nez Rozo</a>
 */
@Controller
public class LoginController implements ControllerDefinition {
    
    /**
     * Loger for class.
     */
    private static final Logger LOGGER = Logger.getLogger(LoginController.class);
    
     /**
     * Service for user administration
     */
    @Autowired
    private UserService userService;
    
    /**
     * Service for sessions administration
     */
    @Autowired
    private SessionService sessionService;
    
    /**
     * Stock action result type
     *
     */
    String flashType;
    
    /**
     * Stock action result message
     *
     */
    String flashMessage;
    
    /**
     * Default action
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param user
     *
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        final RedirectAttributes redirectAttributes) {
        LOGGER.info("Executing  [ onDisplay ]");
        ModelAndView modelAndView = new ModelAndView();
        return modelAndView;
    }
    
    /**
     * Action for users login
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param user
     *
     * @return
     */
    @RequestMapping(value = "/login.htm", method = RequestMethod.POST)
    public ModelAndView login(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        final RedirectAttributes redirectAttributes, @ModelAttribute("user") User user) {
        LOGGER.info("Executing  [ login ]");
        flashType = "success";
        flashMessage = null;
        ModelAndView modelAndView = new ModelAndView("index");
        
        try {
            User valUser = this.userService.findByUsernameAndPassword(user);
            if (valUser != null) {
                Session sessionUsr = this.sessionService.findByUser(valUser);

                if (sessionUsr != null) {
                    flashType = "danger";
                    flashMessage = "login.session.failed";
                    modelAndView.addObject("flashType",flashType);
                    modelAndView.addObject("flashMessage",flashMessage);
                }
                else{
                    sessionUsr = new Session();
                    sessionUsr.setUser(valUser);
                    sessionUsr.setLoginDate(new Date());
                    sessionUsr.setLastOperationDate(new Date());
                    sessionUsr.setLastPingDate(new Date());
                    
                    this.sessionService.save(sessionUsr);
                    request.getSession().setAttribute(SESSION_USER, sessionUsr);
                    return new ModelAndView("redirect:/dashboard.htm");
                  
                }     
                
                
            } else {
                flashType = "danger";
                flashMessage = "login.validation.failed";
            }
        } catch (Exception e) {
            
            LOGGER.info("Error", e);
            flashType = "danger";
            flashMessage = "login.validation.failed";
            modelAndView = new ModelAndView("index.htm");
            modelAndView.addObject("flashType",flashType);
            modelAndView.addObject("flashMessage",flashMessage);
            
        } finally {
            LOGGER.info("End login");
        }
        modelAndView.addObject("flashType",flashType);
        modelAndView.addObject("flashMessage",flashMessage);
        return modelAndView;
    }
    
    
    /**
     * Action for users logout
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = "/logout.htm", method = RequestMethod.POST)
    public ModelAndView logout(HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        LOGGER.info("Executing  [ logout LoginController ]");

        Session session = (Session) request.getSession().
                getAttribute(SESSION_USER);

        if (session != null){
            sessionService.remove(session);
            request.getSession().invalidate();
        }   
        return new ModelAndView("redirect:/index.htm");
    }
    
    /**
     * Internal action called javascript way for update last ping date
     * in session table
     * @param request
     * @param response
     */
    @RequestMapping(value = "/sessionPing.htm", method = RequestMethod.POST)
    public JSONObject sessionPing(HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.info("Executing  [ sessionPing LoginController ]");

        Session session = (Session) request.getSession().
                getAttribute(SESSION_USER);

        JSONObject jSonResult = new JSONObject();
        if (session != null){
            Integer sessionPingResult = sessionService.sessionPing(session);

            try {
                jSonResult.put("result", sessionPingResult);
            } catch (Exception e) {
                LOGGER.error(e);
            }
        }
        return jSonResult;
    }

}
