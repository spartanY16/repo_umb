/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: DelayCauseRepository .java
 * Created on: Mon Jul 17 06:41:33 COT 2017
 * Project: sarpa
 * Objective: To manage Grounds for take-off delays
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.parametric;

import com.gml.sarpa .api.entity.parametric.DelayCause;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To manage Grounds for take-off delays
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface DelayCauseRepository extends JpaRepository<DelayCause, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from parametric.delay_cause  where name ilike %?1% ", nativeQuery = true)
    public List<DelayCause> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from parametric.delay_cause where id = ?1 ", nativeQuery = true)
    public DelayCause get(Long id);
}
