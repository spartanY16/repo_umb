/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: EmployeePositionServiceImpl.java
 * Created on: Wed Jul 19 12:45:39 COT 2017
 * Project: sarpa
 * Objective: To manage employees positions
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.parametric;

import com.gml.sarpa.api.entity.parametric.EmployeePosition;
import com.gml.sarpa.api.services.parametric.EmployeePositionService;
import com.gml.sarpa.web.repository.parametric.EmployeePositionRepository;
import com.gml.sarpa.api.services.audit.AuditLogService;
import org.apache.log4j.Logger;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

/**
 * To manage employees positions
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "EmployeePositionService")
public class EmployeePositionServiceImpl implements EmployeePositionService{

    /**
     * Repository for data access
     */
    @Autowired
    private EmployeePositionRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<EmployeePosition> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param employeePosition 
     */
    @Override
    public void save(EmployeePosition  employeePosition ,String userName){
        employeePosition.setVersion(0);
        employeePosition.setUserCreated(userName);
        employeePosition.setDateCreated();
        employeePosition.setUserLastUpdated(userName);
        employeePosition.setDateLastUpdated();
        repository.save(employeePosition );
        auditLogService.auditInsert(employeePosition.getClass().getName(),
                employeePosition.getId(),employeePosition.getVersion(),
                employeePosition.toString(),userName);
    }

    /**
     * Update entity
     * @param employeePosition 
     */
    @Override
    public void update(EmployeePosition  employeePosition ,String userName){
        EmployeePosition  currentEmployeePosition  = getOne(employeePosition.getId());
        employeePosition.setVersion(currentEmployeePosition.getVersion());
        employeePosition.setUserLastUpdated(userName);
        employeePosition.setDateCreated(currentEmployeePosition.getDateCreated());
        employeePosition.setUserCreated(currentEmployeePosition.getUserCreated());
        employeePosition.setDateLastUpdated();
        repository.save(employeePosition );
        if(!currentEmployeePosition.getName().equals(employeePosition.getName())){
            auditLogService.auditUpdate(employeePosition.getClass().getName(),
            employeePosition.getId(),employeePosition.getVersion(),
            currentEmployeePosition.toString(),"name",employeePosition.getName(),userName);
        }
        if(!currentEmployeePosition.getDescription().equals(employeePosition.getDescription())){
            auditLogService.auditUpdate(employeePosition.getClass().getName(),
            employeePosition.getId(),employeePosition.getVersion(),
            currentEmployeePosition.toString(),"description",employeePosition.getDescription(),userName);
        }
    }

    /**
     * Delete entity
     *@param employeePosition 
     */
    @Override
    public void remove(EmployeePosition  employeePosition ,String userName){
        repository.delete(employeePosition );
        auditLogService.auditDelete(employeePosition.getClass().getName(),
                employeePosition.getId(),employeePosition.getVersion(),
                employeePosition.toString(),userName);
    }

    

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public EmployeePosition getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<EmployeePosition> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
