/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CurrencyRepository.java
 * Created on: Thu Aug 03 09:42:27 COT 2017
 * Project: sarpa
 * Objective: To manage money currencies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.parametric;

import com.gml.sarpa.api.entity.parametric.Currency;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To manage money currencies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface CurrencyRepository extends JpaRepository<Currency, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from parametric.currency  where name ilike %?1% ", nativeQuery = true)
    public List<Currency> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from parametric.currency where id = ?1 ", nativeQuery = true)
    public Currency get(Long id);
}
