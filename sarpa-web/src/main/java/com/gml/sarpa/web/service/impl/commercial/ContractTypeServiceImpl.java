/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContractTypeServiceImpl.java
 * Created on: Thu Aug 17 22:44:19 COT 2017
 * Project: sarpa
 * Objective: To manage clients contract types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.commercial;

import com.gml.sarpa.api.entity.commercial.ContractType;
import com.gml.sarpa.api.services.commercial.ContractTypeService;
import com.gml.sarpa.web.repository.commercial.ContractTypeRepository;
import com.gml.sarpa.api.services.audit.AuditLogService;
import org.apache.log4j.Logger;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

/**
 * To manage clients contract types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "ContractTypeService")
public class ContractTypeServiceImpl implements ContractTypeService{

    /**
     * Repository for data access
     */
    @Autowired
    private ContractTypeRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<ContractType> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param contractType 
     */
    @Override
    public void save(ContractType  contractType ,String userName){
        contractType.setVersion(0);
        contractType.setUserCreated(userName);
        contractType.setDateCreated();
        contractType.setUserLastUpdated(userName);
        contractType.setDateLastUpdated();
        repository.save(contractType );

        auditLogService.auditInsert(contractType.getClass().getName(),
                contractType.getId(),contractType.getVersion(),
                contractType.toString(),userName);
    }

    /**
     * Update entity
     * @param contractType 
     */
    @Override
    public void update(ContractType  contractType ,String userName){
        ContractType  currentContractType  = getOne(contractType.getId());
        contractType.setVersion(currentContractType.getVersion());
        contractType.setDateCreated(currentContractType.getDateCreated());
        contractType.setUserCreated(currentContractType.getUserCreated());
        contractType.setUserLastUpdated(userName);
        contractType.setDateLastUpdated();
        repository.save(contractType );

        if(!currentContractType.getName().equals(contractType.getName())){
            auditLogService.auditUpdate(contractType.getClass().getName(),
            contractType.getId(),contractType.getVersion(),
            currentContractType.toString(),"name",contractType.getName(),userName);
        }
    }

    /**
     * Delete entity
     *@param contractType 
     */
    @Override
    public void remove(ContractType  contractType ,String userName){
        repository.delete(contractType );
        auditLogService.auditDelete(contractType.getClass().getName(),
                contractType.getId(),contractType.getVersion(),
                contractType.toString(),userName);
    }

    

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public ContractType getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<ContractType> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
