/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: GroundAmbulanceServiceImpl.java
 * Created on: 2017/09/06, 03:27:31 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.commercial;

import com.gml.sarpa.api.entity.commercial.GroundAmbulance;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.commercial.GroundAmbulanceService;
import com.gml.sarpa.web.repository.commercial.GroundAmbulanceRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
@Service(value = "GroundAmbulanceService")
public class GroundAmbulanceServiceImpl implements GroundAmbulanceService {
       
    /**
     * Repository for data access
     */
    @Autowired
    private GroundAmbulanceRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    @Override
    public List<GroundAmbulance> findAll() {
        return repository.findAll();
    }

    @Override
    public void save(GroundAmbulance groundAmbulance, String userName) {
        groundAmbulance.setVersion(0);
        groundAmbulance.setUserCreated(userName);
        groundAmbulance.setDateCreated();
        groundAmbulance.setUserLastUpdated(userName);
        groundAmbulance.setDateLastUpdated();
        repository.save(groundAmbulance);
        auditLogService.auditInsert(groundAmbulance.getClass().getName(),
            groundAmbulance.getId(), groundAmbulance.getVersion(),
            groundAmbulance.toString(), userName);
    }

    @Override
    public void update(GroundAmbulance groundAmbulance, String userName) {
        GroundAmbulance currentGroundAmbulance = getOne(groundAmbulance.getId());
        groundAmbulance.setVersion(currentGroundAmbulance.getVersion());
        groundAmbulance.setUserLastUpdated(userName);
        groundAmbulance.setDateLastUpdated();
        groundAmbulance.setDateCreated(currentGroundAmbulance.getDateCreated());
        groundAmbulance.setUserCreated(currentGroundAmbulance.getUserCreated());
        repository.save(groundAmbulance);

        auditLogService.auditInsert(groundAmbulance.getClass().getName(),
            groundAmbulance.getId(), groundAmbulance.getVersion(),
            groundAmbulance.toString(), userName);
    }

    @Override
    public void remove(GroundAmbulance groundAmbulance, String userName) {
        repository.delete(groundAmbulance);
        auditLogService.auditDelete(groundAmbulance.getClass().getName(),
            groundAmbulance.getId(), groundAmbulance.getVersion(),
            groundAmbulance.toString(), userName);
    }

    @Override
    public GroundAmbulance getOne(Long id) {
        return repository.get(id);
    }

    @Override
    public List<GroundAmbulance> getAllByFilter(String filter) {
        return repository.getAllByFilter(filter);
    }

}
