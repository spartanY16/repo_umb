/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: SessionRepository.java
 * Created on: 2017/02/10, 10:55:46 PM
 * Project: SARPA
 * Objective: Repository for session administration.
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.auth;

import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.auth.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository for session administration.
 *
 * @author <a href="mailto:javierr@gmlsoftware.com">Javier Rocha</a>
 */
@Transactional
public interface SessionRepository extends JpaRepository<Session, Long> {

    /**
     * Find any sesion for an user
     * @param user 
     * @return
     */
    @Query(nativeQuery = true, value = "SELECT * FROM AUTH.SESSIONS WHERE " +
        "USER_ID = ?1")
    Session findByUser(User user);
    
    /**
     * Update last ping _date for user session
     * @param sessionId
     * @return
     */
    @Modifying
    @Query(value = "UPDATE AUTH.SESSIONS SET LAST_PING_DATE = CURRENT_TIMESTAMP " +
        "WHERE ID = ?1",
        nativeQuery = true)
    Integer sessionPing(Long sessionId);
    
    /**
     * Update LAST_OPERATION_DATE for session id
     * @param userId
     * @return
     */
    @Modifying
    @Query(value = "UPDATE AUTH.SESSIONS SET LAST_OPERATION_DATE = CURRENT_TIMESTAMP " +
        "WHERE  USUARIO = ?1",
        nativeQuery = true)
    Integer updateSession(Long userId);

    /**
     * Expirate sessions:
     * When total session time is higher than TOTAL_SESSION_EXP_TIME parameter
     * or when inactive session time is higher than INACTIVE_SESSION_EXP_TIME parameter
     * @return
     */
    @Modifying
    @Query(value = "delete " +
    "from auth.sessions " +
    "where " +
    "(extract(minute from (current_timestamp - login_date))) >  " +
    "	(select cast (value as integer) from parametric.parameter where key = 'TOTAL_SESSION_EXP_TIME') " +
    "or (extract(minute from (current_timestamp - last_ping_date))) >  " +
    "(select cast (value as integer)  from parametric.parameter where key = 'INACTIVE_SESSION_EXP_TIME') " +
    "; "
        , nativeQuery = true)
    Integer expireSessions();
    
    
}
