/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: GroundAmbulanceRepository.java
 * Created on: 2017/09/06, 10:50:45 AM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.commercial;

import com.gml.sarpa.api.entity.commercial.GroundAmbulance;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
public interface GroundAmbulanceRepository extends JpaRepository<GroundAmbulance, Long>{
     /**
     * Search entity by filter
     */
    @Query(value =
        "select * from commercial.groundambulance  where name ilike %?1% ", nativeQuery = true)
    public List<GroundAmbulance> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from commercial.groundambulance where id = ?1 ", nativeQuery = true)
    public GroundAmbulance get(Long id);
    
    /**
     * Search next code value
     */
    @Query(value =
        "select nextval('commercial.groundambulance_sequence') ", nativeQuery = true)
    public Long getNextCode();
}
