/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: SessionServiceImpl.java
 * Created on: 2017/02/10, 10:55:46 PM
 * Project: SARPA
 * Objective: Services for sessions management.
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.auth;

import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.auth.User;
import com.gml.sarpa.api.services.auth.SessionService;
import com.gml.sarpa.web.repository.auth.SessionRepository;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Services for sessions management.
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Ang&eacute;lica
 * Jim&eacute;nez Rozo</a>
 */
@Service(value = "sessionService")
public class SessionServiceImpl implements SessionService {

    /**
     * Logger for class.
     */
    private static final Logger LOGGER = Logger.getLogger(SessionServiceImpl.class);
    
    /**
     * Sessions repository
     */
    @Autowired
    private SessionRepository repository;

    
    @Override
    public List<Session> getAll() {
        LOGGER.info("Executing  [ SessionServiceImpl - getAll ]");
        return this.repository.findAll();
    }

    @Override
    public void save(Session session) {
        LOGGER.info("Executing  [ SessionServiceImpl - save ]");
        
        this.repository.saveAndFlush(session);
    }

    @Override
    public void update(Session session) {
        LOGGER.info("Executing  [ SessionServiceImpl - update ]");
        this.repository.saveAndFlush(session);
    }

    @Override
    public void remove(Session session) {
        LOGGER.info("Executing  [ SessionServiceImpl - remove ] for session "+
                session.getId());
        this.repository.delete(session);
    }

    @Override
    public Session findByUser(User user) {
        LOGGER.info("Executing  [ SessionServiceImpl - findByUser ]");
        
        return this.repository.findByUser(user);
    }
    
    @Override
    public int expireSessions() {
        
        LOGGER.info("Executing  [ SessionServiceImpl - expireSessions ]");
        return this.repository.expireSessions();
    }

    @Override
    public Integer sessionPing(Session session) {
        
        try {
            LOGGER.info("Executing  [ SessionServiceImpl - sessionPing ] "
                    + "for sesion: " +session.toString());
            return this.repository.sessionPing(session.getId());
        } catch (Exception e) {
            LOGGER.error("Not session founded: "+e);
            return 0;
        }
        
        
    }

}
