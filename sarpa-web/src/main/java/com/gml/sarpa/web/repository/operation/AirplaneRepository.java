/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirplaneRepository .java
 * Created on: Thu Jun 29 15:09:23 COT 2017
 * Project: sarpa
 * Objective: To define Airplans
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.operation;

import com.gml.sarpa.api.entity.operation.Airplane;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To define Airplans
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface AirplaneRepository extends JpaRepository<Airplane, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from operation.airplane  where name ilike %?1% ", nativeQuery = true)
    public List<Airplane> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from operation.airplane where id = ?1 ", nativeQuery = true)
    public Airplane get(Long id);
}
