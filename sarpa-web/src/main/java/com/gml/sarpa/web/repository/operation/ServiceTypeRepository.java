/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ServiceTypeRepository .java
 * Created on: Fri Jun 30 23:16:43 COT 2017
 * Project: sarpa
 * Objective: To define services types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.operation;

import com.gml.sarpa.api.entity.operation.ServiceType;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To define services types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface ServiceTypeRepository extends JpaRepository<ServiceType, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from operation.service_type  where name ilike %?1% ", nativeQuery = true)
    public List<ServiceType> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from operation.service_type where id = ?1 ", nativeQuery = true)
    public ServiceType get(Long id);
}
