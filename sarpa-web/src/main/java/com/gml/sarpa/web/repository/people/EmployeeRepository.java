/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: EmployeeRepository .java
 * Created on: Tue Jul 18 06:52:02 COT 2017
 * Project: sarpa
 * Objective: To manage employees, crew included
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.people;

import com.gml.sarpa .api.entity.people.Employee;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To manage employees, crew included
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from people.employee  where name ilike %?1% ", nativeQuery = true)
    public List<Employee> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from people.employee where id = ?1 ", nativeQuery = true)
    public Employee get(Long id);
}
