/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: LoadTypeRepository .java
 * Created on: Fri Jun 30 23:16:25 COT 2017
 * Project: sarpa
 * Objective: To define load types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.operation;

import com.gml.sarpa.api.entity.operation.LoadType;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To define load types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface LoadTypeRepository extends JpaRepository<LoadType, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from operation.load_type  where name ilike %?1% ", nativeQuery = true)
    public List<LoadType> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from operation.load_type where id = ?1 ", nativeQuery = true)
    public LoadType get(Long id);
}
