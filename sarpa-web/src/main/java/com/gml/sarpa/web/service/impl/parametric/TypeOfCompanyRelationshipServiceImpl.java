/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TypeOfCompanyRelationshipServiceImpl .java
 * Created on: Thu Jun 29 15:22:59 COT 2017
 * Project: sarpa
 * Objective: To define business units
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.parametric;

import com.gml.sarpa.api.entity.parametric.TypeOfCompanyRelationship;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.parametric.TypeOfCompanyRelationshipService;
import com.gml.sarpa.web.repository.parametric.TypeOfCompanyRelationshipRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To define business units
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
@Service(value = "TypeOfCompanyRelationshipService")
public class TypeOfCompanyRelationshipServiceImpl implements TypeOfCompanyRelationshipService{

    /**
     * Repository for data access
     */
    @Autowired
    private TypeOfCompanyRelationshipRepository repository;
    
    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;
    
    

    /**
     * List all entities
     * @return
     */
    @Override
    public List<TypeOfCompanyRelationship> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param typeOfCompanyRelationship
     */
    @Override
    public void save(TypeOfCompanyRelationship typeOfCompanyRelationship,String userName){
        typeOfCompanyRelationship.setVersion(0);
        typeOfCompanyRelationship.setUserCreated(userName);
        typeOfCompanyRelationship.setDateCreated(new Date());
        typeOfCompanyRelationship.setDateLastUpdated();
        typeOfCompanyRelationship.setUserLastUpdated(userName);
        repository.save(typeOfCompanyRelationship);
        auditLogService.auditInsert(typeOfCompanyRelationship.getClass().getName(),
                typeOfCompanyRelationship.getId(),typeOfCompanyRelationship.getVersion(),
                typeOfCompanyRelationship.toString(),userName);
    }

    /**
     * Update entity
     * @param typeOfCompanyRelationship
     */
    @Override
    public void update(TypeOfCompanyRelationship typeOfCompanyRelationship,String userName){
        TypeOfCompanyRelationship currentTypeOfCompanyRelationship = getOne(typeOfCompanyRelationship.getId());
        typeOfCompanyRelationship.setVersion(currentTypeOfCompanyRelationship.getVersion());
        typeOfCompanyRelationship.setUserLastUpdated(userName);
        typeOfCompanyRelationship.setDateLastUpdated();
        typeOfCompanyRelationship.setDateCreated(currentTypeOfCompanyRelationship.getDateCreated());
        typeOfCompanyRelationship.setUserCreated(currentTypeOfCompanyRelationship.getUserCreated());
        repository.save(typeOfCompanyRelationship);
        if(!currentTypeOfCompanyRelationship.getName().equals(typeOfCompanyRelationship.getName())){
            auditLogService.auditUpdate(typeOfCompanyRelationship.getClass().getName(),
            typeOfCompanyRelationship.getId(),typeOfCompanyRelationship.getVersion(),
            currentTypeOfCompanyRelationship.toString(),"name",typeOfCompanyRelationship.getName(),userName);
        }
        if(!currentTypeOfCompanyRelationship.getDescription().equals(typeOfCompanyRelationship.getDescription())){
            auditLogService.auditUpdate(typeOfCompanyRelationship.getClass().getName(),
            typeOfCompanyRelationship.getId(),typeOfCompanyRelationship.getVersion(),
            currentTypeOfCompanyRelationship.toString(),"description",typeOfCompanyRelationship.getDescription(),userName);
        }
    }

    /**
     * Delete entity
     *@param typeOfCompanyRelationship
     */
    @Override
    public void remove(TypeOfCompanyRelationship typeOfCompanyRelationship,String userName){
        repository.delete(typeOfCompanyRelationship);
        auditLogService.auditDelete(typeOfCompanyRelationship.getClass().getName(),
                typeOfCompanyRelationship.getId(),typeOfCompanyRelationship.getVersion(),
                typeOfCompanyRelationship.toString(),userName);
    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public TypeOfCompanyRelationship getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<TypeOfCompanyRelationship> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
