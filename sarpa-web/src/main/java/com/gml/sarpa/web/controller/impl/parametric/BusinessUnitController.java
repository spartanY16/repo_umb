/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: BusinessUnitController.java
 * Created on: Thu Jun 29 15:22:59 COT 2017
 * Project: sarpa
 * Objective: To define business units
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.parametric;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.parametric.BusinessUnit;
import com.gml.sarpa.api.services.parametric.BusinessUnitService;
import com.gml.sarpa.api.utilities.Util;
import static com.gml.sarpa.api.utilities.Util.SESSION_USER;
import com.gml.sarpa.web.controller.ControllerDefinition;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

/**
 * To define business units
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class BusinessUnitController implements ControllerDefinition {

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(BusinessUnitController.class);

    /**
     * Session user information
     */
    private Session userSession;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private BusinessUnitService entityService;

    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/businessUnit-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session,
            RedirectAttributes redirectAttributes) {

        LOGGER.info("Runing [ BusinessUnitController - onDisplay ]");
        init(request);
        ModelAndView modelAndView = new ModelAndView("parametric/businessUnit/index");

        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType", flashMap.get("flashType"));
            modelAndView.addObject("flashMessage", flashMap.get("flashMessage"));
        }

        modelAndView.addObject("indexList", this.entityService.findAll());

        return modelAndView;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/businessUnit-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session) {

        LOGGER.info("Runing [ BusinessUnitController - create ]");
        init(request);
        ModelAndView modelAndView = new ModelAndView("parametric/businessUnit/create");

        BusinessUnit businessUnit = new BusinessUnit();
        modelAndView.addObject("businessUnit", businessUnit);
        return modelAndView;
    }

    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/businessUnit-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("businessUnit") BusinessUnit businessUnit) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ BusinessUnitController - save ]");
        init(request);
        try {
            entityService.save(businessUnit, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "parametric.businessUnit.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "parametric.businessUnit.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/businessUnit-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session,
            RedirectAttributes redirectAttributes,
            @RequestParam(value = "id", required = true) Long id) {

        LOGGER.info("Runing [ BusinessUnitController - edit ] id " + id);
        ModelAndView modelAndView = new ModelAndView("parametric/businessUnit/edit");
        init(request);
        BusinessUnit businessUnit = entityService.getOne(id);

        if (businessUnit != null) {
            modelAndView.addObject("businessUnit", businessUnit);

        } else {
            redirectAttributes.addFlashAttribute("flashType", "danger");
            redirectAttributes.addFlashAttribute("flashMessage", "parametric.businessUnit.edit.success");
            return new ModelAndView("redirect:/businessUnit-index.htm");
        }
        return modelAndView;

    }

    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/businessUnit-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("businessUnit") BusinessUnit businessUnit) {

        LOGGER.info("Runing [ BusinessUnitController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            entityService.update(businessUnit, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "parametric.businessUnit.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "parametric.businessUnit.update.error", null, locale));

        }

        return Util.getJson(result);
    }

    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/businessUnit-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
            @RequestParam(value = "id", required = true) Long id) {

        LOGGER.info("Runing [ BusinessUnitController - delete ] id " + id);
        ModelAndView modelAndView = new ModelAndView("redirect:/businessUnit-index.htm");
        init(request);
        BusinessUnit businessUnit = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (businessUnit != null) {
            try {
                entityService.remove(businessUnit, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "parametric.businessUnit.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "parametric.businessUnit.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("parametric.businessUnit.delete.error");
        }
        return Util.getJson(result);

    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }

}
