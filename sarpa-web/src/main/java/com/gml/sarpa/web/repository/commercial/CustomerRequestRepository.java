/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CustomerRequestRepository.java
 * Created on: Wed Jul 26 17:22:44 COT 2017
 * Project: sarpa
 * Objective: To manage customers requests
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.commercial;

import com.gml.sarpa.api.entity.commercial.CustomerRequest;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To manage customers requests
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface CustomerRequestRepository extends JpaRepository<CustomerRequest, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from commercial.customer_request  where name ilike %?1% ", nativeQuery = true)
    public List<CustomerRequest> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from commercial.customer_request where id = ?1 ", nativeQuery = true)
    public CustomerRequest get(Long id);

    /**
     * Search next code value
     */
    @Query(value =
        "select nextval('commercial.customer_request_sequence') ", nativeQuery = true)
    public Long getNextCode();
}
