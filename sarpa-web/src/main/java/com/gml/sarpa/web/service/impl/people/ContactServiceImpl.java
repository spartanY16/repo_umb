/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContactServiceImpl .java
 * Created on: Fri Jun 30 22:30:20 COT 2017
 * Project: sarpa
 * Objective: To define persons contact data
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.people;

import com.gml.sarpa.api.entity.people.Contact;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.people.ContactService;
import com.gml.sarpa.web.repository.people.ContactRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To define persons contact data
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "ContactService")
public class ContactServiceImpl implements ContactService{

    /**
     * Repository for data access
     */
    @Autowired
    private ContactRepository repository;
    
    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<Contact> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param contact
     */
    @Override
    public void save(Contact contact,String userName){
        contact.setVersion(0);
        contact.setUserCreated(userName);
        contact.setDateCreated();
        contact.setUserLastUpdated(userName);
        contact.setDateLastUpdated();
        repository.save(contact);
        auditLogService.auditInsert(contact.getClass().getName(),
                contact.getId(),contact.getVersion(),
                contact.toString(),userName);
    }

    /**
     * Update entity
     * @param contact
     */
    @Override
    public void update(Contact contact,String userName){
        Contact currentContact = getOne(contact.getId());
        contact.setVersion(currentContact.getVersion());
        contact.setUserLastUpdated(userName);
        contact.setDateLastUpdated();
        repository.save(contact);
        if(!currentContact.getPerson().getCompleteName().equals(contact.getPerson().getCompleteName())){
            auditLogService.auditUpdate(contact.getClass().getName(),
            contact.getId(),contact.getVersion(),
            currentContact.toString(),"person",contact.getPerson().getCompleteName(),userName);
        }
    }

    /**
     * Delete entity
     *@param contact
     */
    @Override
    public void remove(Contact contact,String userName){
        repository.delete(contact);
        auditLogService.auditDelete(contact.getClass().getName(),
                contact.getId(),contact.getVersion(),
                contact.toString(),userName);
    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public Contact getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<Contact> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }
    /**
     * Search entity by value
     * @param value
     * @return 
     */
    @Override
    public List<Contact> getContactsParams(String value, Long fillValue) {
        return repository.getContactsParams(value, fillValue);
    }

    
}
