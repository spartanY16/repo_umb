/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirportExtensionHourServiceImpl.java
 * Created on: Thu Sep 28 08:58:21 COT 2017
 * Project: sarpa
 * Objective: To manage airports rates
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.parametric;

import com.gml.sarpa.api.entity.parametric.AirportExtensionHour;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.parametric.AirportExtensionHourService;
import com.gml.sarpa.web.repository.parametric.AirportExtensionHourRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To manage airports rates
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
@Service(value = "AirportExtensionHourService")
public class AirportExtensionHourServiceImpl implements AirportExtensionHourService{


    /**
     * Repository Data.
     */
    @Autowired
    private AirportExtensionHourRepository repository;
    
    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;
    

    /**
     * List all entities
     * @return
     */
    @Override
    public List<AirportExtensionHour> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param airportRate
     */
    @Override
    public void save(AirportExtensionHour airportRate,String userName){
        airportRate.setVersion(0);
        airportRate.setUserCreated(userName);
        airportRate.setDateCreated();
        airportRate.setUserLastUpdated(userName);
        airportRate.setDateLastUpdated();
        repository.save(airportRate);
        auditLogService.auditInsert(airportRate.getClass().getName(),
                airportRate.getId(),airportRate.getVersion(),
                airportRate.toString(),userName);
    }

    /**
     * Update entity
     * @param airportRate
     */
    @Override
    public void update(AirportExtensionHour airportRate,String userName){
        AirportExtensionHour currentCurrency = getOne(airportRate.getId());
        airportRate.setVersion(currentCurrency.getVersion());
        airportRate.setUserLastUpdated(userName);
        airportRate.setDateLastUpdated();
        airportRate.setDateCreated(currentCurrency.getDateCreated());
        airportRate.setUserCreated(currentCurrency.getUserCreated());
        repository.save(airportRate);
        /**if(!currentCurrency.getName().equals(airportRate.getName())){
            auditLogService.auditUpdate(airportRate.getClass().getName(),
            airportRate.getId(),airportRate.getVersion(),
            currentCurrency.toString(),"name",airportRate.getName(),userName);
        }**/
    }

    /**
     * Delete entity
     *@param airportRate
     */
    @Override
    public void remove(AirportExtensionHour airportRate,String userName){
        repository.delete(airportRate);
        auditLogService.auditDelete(airportRate.getClass().getName(),
                airportRate.getId(),airportRate.getVersion(),
                airportRate.toString(),userName);
    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public AirportExtensionHour getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<AirportExtensionHour> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
