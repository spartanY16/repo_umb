/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirplaneServiceImpl .java
 * Created on: Thu Jun 29 15:09:23 COT 2017
 * Project: sarpa
 * Objective: To define Airplans
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.operation;

import com.gml.sarpa.api.entity.operation.Airplane;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.operation.AirplaneService;
import com.gml.sarpa.web.repository.operation.AirplaneRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To define Airplans
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "AirplaneService")
public class AirplaneServiceImpl implements AirplaneService{

    /**
     * Repository for data access
     */
    @Autowired
    private AirplaneRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;
    
    /**
     * List all entities
     * @return
     */
    @Override
    public List<Airplane> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param airplane
     */
    @Override
    public void save(Airplane airplane,String userName){
        airplane.setVersion(0);
        airplane.setUserCreated(userName);
        airplane.setDateCreated();
        airplane.setUserLastUpdated(userName);
        airplane.setDateLastUpdated();
        repository.save(airplane);
        auditLogService.auditInsert(airplane.getClass().getName(),
                airplane.getId(),airplane.getVersion(),
                airplane.toString(),userName);
    }

    /**
     * Update entity
     * @param airplane
     */
    @Override
    public void update(Airplane airplane,String userName){
        Airplane currentAirplane = getOne(airplane.getId());
        airplane.setVersion(currentAirplane.getVersion());
        airplane.setUserLastUpdated(userName);
        airplane.setDateLastUpdated();
        repository.save(airplane);
        if(!currentAirplane.getName().equals(airplane.getName())){
            auditLogService.auditUpdate(airplane.getClass().getName(),
            airplane.getId(),airplane.getVersion(),
            currentAirplane.toString(),"name",airplane.getName(),userName);
        }
        if(!currentAirplane.getAirplaneType().equals(airplane.getAirplaneType())){
            auditLogService.auditUpdate(airplane.getClass().getName(),
            airplane.getId(),airplane.getVersion(),
            currentAirplane.toString(),"airplaneType",airplane.getAirplaneType().getName(),userName);
        }
        if(!currentAirplane.getPassengersNumber().equals(airplane.getPassengersNumber())){
            auditLogService.auditUpdate(airplane.getClass().getName(),
            airplane.getId(),airplane.getVersion(),
            currentAirplane.toString(),"passengersNumber",airplane.getPassengersNumber().toString(),userName);
        }
        if(!currentAirplane.getStretchersNumber().equals(airplane.getStretchersNumber())){
            auditLogService.auditUpdate(airplane.getClass().getName(),
            airplane.getId(),airplane.getVersion(),
            currentAirplane.toString(),"StretchersNumber",airplane.getStretchersNumber().toString(),userName);
        }
    }

    /**
     * Delete entity
     *@param airplane
     */
    @Override
    public void remove(Airplane airplane,String userName){
        repository.delete(airplane);
        auditLogService.auditDelete(airplane.getClass().getName(),
                airplane.getId(),airplane.getVersion(),
                airplane.toString(),userName);
    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public Airplane getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<Airplane> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
