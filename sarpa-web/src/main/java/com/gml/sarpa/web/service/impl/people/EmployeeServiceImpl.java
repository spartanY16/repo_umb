/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: EmployeeServiceImpl.java
 * Created on: Tue Jul 18 06:52:02 COT 2017
 * Project: sarpa
 * Objective: To manage employees, crew included
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.people;

import com.gml.sarpa.api.entity.people.Employee;
import com.gml.sarpa.api.services.people.EmployeeService;
import com.gml.sarpa.web.repository.people.EmployeeRepository;
import com.gml.sarpa.api.services.audit.AuditLogService;
import org.apache.log4j.Logger;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

/**
 * To manage employees, crew included
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "EmployeeService")
public class EmployeeServiceImpl implements EmployeeService{

    /**
     * Repository for data access
     */
    @Autowired
    private EmployeeRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<Employee> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param employee 
     */
    @Override
    public void save(Employee  employee ,String userName){
        employee.setVersion(0);
        employee.setUserCreated(userName);
        employee.setDateCreated();
        employee.setUserLastUpdated(userName);
        employee.setDateLastUpdated();
        repository.save(employee );
        auditLogService.auditInsert(employee.getClass().getName(),
                employee.getId(),employee.getVersion(),
                employee.toString(),userName);
    }

    /**
     * Update entity
     * @param employee 
     */
    @Override
    public void update(Employee  employee ,String userName){
        Employee  currentEmployee  = getOne(employee.getId());
        employee.setVersion(currentEmployee.getVersion());
        employee.setUserLastUpdated(userName);
        employee.setDateLastUpdated();
        repository.save(employee );
        if(!currentEmployee.getPerson().equals(employee.getPerson())){
            auditLogService.auditUpdate(employee.getClass().getName(),
            employee.getId(),employee.getVersion(),
            currentEmployee.toString(),"person",employee.getPerson().getCompleteName(),userName);
        }
    }

    /**
     * Delete entity
     *@param employee 
     */
    @Override
    public void remove(Employee  employee ,String userName){
        repository.delete(employee );
        auditLogService.auditDelete(employee.getClass().getName(),
                employee.getId(),employee.getVersion(),
                employee.toString(),userName);
    }

    

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public Employee getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<Employee> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
