/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContractTypeRepository.java
 * Created on: Thu Aug 17 22:44:19 COT 2017
 * Project: sarpa
 * Objective: To manage clients contract types
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.commercial;

import com.gml.sarpa.api.entity.commercial.ContractType;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To manage clients contract types
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface ContractTypeRepository extends JpaRepository<ContractType, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from commercial.contract_type  where name ilike %?1% ", nativeQuery = true)
    public List<ContractType> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from commercial.contract_type where id = ?1 ", nativeQuery = true)
    public ContractType get(Long id);
}
