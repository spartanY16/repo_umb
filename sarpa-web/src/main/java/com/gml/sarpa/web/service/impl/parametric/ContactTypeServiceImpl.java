/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContactTypeServiceImpl .java
 * Created on: Fri Jun 30 22:28:37 COT 2017
 * Project: sarpa
 * Objective: To define persons contact data
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.parametric;

import com.gml.sarpa.api.entity.parametric.ContactType;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.people.ContactTypeService;
import com.gml.sarpa.web.repository.parametric.ContactTypeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To define persons contact data
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "ContactTypeService")
public class ContactTypeServiceImpl implements ContactTypeService{

    /**
     * Repository for data access
     */
    @Autowired
    private ContactTypeRepository repository;
    
    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<ContactType> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param contactType
     */
    @Override
    public void save(ContactType contactType,String userName){
        contactType.setVersion(0);
        contactType.setUserCreated(userName);
        contactType.setDateCreated();
        contactType.setDateLastUpdated();
        contactType.setUserLastUpdated(userName);
        repository.save(contactType);
        auditLogService.auditInsert(contactType.getClass().getName(),
                contactType.getId(),contactType.getVersion(),
                contactType.toString(),userName);
    }

    /**
     * Update entity
     * @param contactType
     */
    @Override
    public void update(ContactType contactType,String userName){
        ContactType currentContactType = getOne(contactType.getId());
        contactType.setVersion(currentContactType.getVersion());
        contactType.setUserLastUpdated(userName);
        contactType.setDateLastUpdated();
        repository.save(contactType);
        if(!currentContactType.getName().equals(contactType.getName())){
            auditLogService.auditUpdate(contactType.getClass().getName(),
            contactType.getId(),contactType.getVersion(),
            currentContactType.toString(),"name",contactType.getName(),userName);
        }
        
    }

    /**
     * Delete entity
     *@param contactType
     */
    @Override
    public void remove(ContactType contactType,String userName){
        repository.delete(contactType);
        auditLogService.auditDelete(contactType.getClass().getName(),
                contactType.getId(),contactType.getVersion(),
                contactType.toString(),userName);
    }

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public ContactType getOne(Long id){
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<ContactType> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
