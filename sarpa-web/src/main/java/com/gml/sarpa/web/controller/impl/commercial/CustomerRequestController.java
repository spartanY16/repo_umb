/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CustomerRequestController.java
 * Created on: Wed Jul 26 17:22:44 COT 2017
 * Project: sarpa
 * Objective: To manage clients requests
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.commercial;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.commercial.Company;
import com.gml.sarpa.api.entity.commercial.CustomerRequest;
import com.gml.sarpa.api.entity.operation.Journey;
import com.gml.sarpa.api.enums.EnumOptions;
import com.gml.sarpa.api.services.commercial.AirAmbulanceService;
import com.gml.sarpa.api.services.commercial.CompanyService;
import com.gml.sarpa.api.services.commercial.CustomerRequestService;
import com.gml.sarpa.api.services.commercial.CustomerService;
import com.gml.sarpa.api.services.commercial.GroundAmbulanceService;
import com.gml.sarpa.api.services.operation.AirplaneService;
import com.gml.sarpa.api.services.operation.AirplaneTypeService;
import com.gml.sarpa.api.services.operation.JourneyService;
import com.gml.sarpa.api.services.operation.LoadTypeService;
import com.gml.sarpa.api.services.operation.ServiceTypeService;
import com.gml.sarpa.api.services.parametric.BusinessUnitService;
import com.gml.sarpa.api.utilities.Util;
import com.gml.sarpa.web.controller.ControllerDefinition;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.web.bind.annotation.SessionAttributes;


/**
 * To manage clients requests
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
@SessionAttributes({"customerRequest"})
public class CustomerRequestController implements ControllerDefinition{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(CustomerRequestController.class);

    /**
     * Session user information
     */
    private Session userSession;
    
    /**
     * CustomerRequest selected
     */
    private CustomerRequest customerRequestSelected;

    /**
     * CustomerRequest service
     */
    @Autowired
    private CustomerRequestService entityService;
    
    /**
     * CustomerService service
     */
    @Autowired
    private CustomerService customerService;
    
    /**
     * AirplaneService service
     */
    @Autowired
    private AirplaneService airplaneService;
    
    /**
     * ServiceType service
     */
    @Autowired
    private ServiceTypeService serviceTypeService;
    
    /**
     * LoadType service
     */
    @Autowired
    private LoadTypeService loadTypeService;
    /**
     * MessageSource service
     */
    @Autowired
    private MessageSource messageSource;
    /**
     * Business Unit Service
     */
    @Autowired
    private BusinessUnitService businessUnitService;
    /**
     * Company Service.
     */
    @Autowired
    private CompanyService companyService;
    /**
     * Ground Ambulance Service.
     */
    @Autowired
    private GroundAmbulanceService configurationGA;
    /**
     * Air Ambulance Service.
     */
    @Autowired
    private AirAmbulanceService airAmbulanceService;
    
    /**
     * Air Ambulance Service.
     */
    @Autowired
    private AirplaneTypeService airplaneTypeService;
    
    /**
     * Journey Service.
     */
    @Autowired
    private JourneyService journeyService;
    
    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/customerRequest-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response, Locale locale, HttpSession session,
        RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Runing [ CustomerRequestController - onDisplay ]");
        ModelAndView modelAndView = new ModelAndView("commercial/customerRequest/index");
        init(request);
        getInfoRequestSession(request, true);
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType",flashMap.get("flashType"));
            modelAndView.addObject("flashMessage",flashMap.get("flashMessage"));
        }
        
        modelAndView.addObject("indexList",this.entityService.findAll());

        return modelAndView ;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/customerRequest-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ CustomerRequestController - create ]");
        ModelAndView modelAndView = new ModelAndView("commercial/customerRequest/create");
        init(request);      
        CustomerRequest customerRequest =  this.getInfoRequestSession(request, true);
        if(customerRequest == null){
            customerRequest = new CustomerRequest();
        }
        modelAndView.addObject("customerRequest", customerRequest);
        modelAndView.addObject("customerList", customerService.findAll());
        modelAndView.addObject("airplaneList", airplaneService.findAll());
        modelAndView.addObject("serviceTypeList", serviceTypeService.findAll());
        modelAndView.addObject("loadTypeList", loadTypeService.findAll());
        modelAndView.addObject("businessUnitList", businessUnitService.findAll());
        modelAndView.addObject("companyList", companyService.findAll());
        return modelAndView;
    }
    
    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/customerRequest-create2.htm", method = RequestMethod.GET)
    public ModelAndView create2page(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ CustomerRequestController - create 2 ]");
        ModelAndView modelAndView = new ModelAndView("commercial/customerRequest/create2");
        init(request);
        
        CustomerRequest customerRequest = new CustomerRequest();
        final boolean isCustomerRequestUpdate = this.customerRequestSelected != null
            && this.customerRequestSelected.getId() != null;
        if(isCustomerRequestUpdate){
            customerRequest = entityService.getOne(this.customerRequestSelected.getId());
        }
        modelAndView.addObject("customerRequest", customerRequest);
        modelAndView.addObject("airAmbulanceList", airAmbulanceService.findAll());
        modelAndView.addObject("loadTypeList", loadTypeService.findAll());
        modelAndView.addObject("configurationGAList", configurationGA.findAll());
        //modelAndView.addObject("enumsOptionList", Arrays.asList(EnumOptions.values()));
        modelAndView.addObject("enumsOptionList", EnumOptions.values());
        return modelAndView;
    }
    
    @RequestMapping(value = "/customerRequest-create3.htm", method = RequestMethod.GET)
    public ModelAndView create3page(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ CustomerRequestController - create 3 ]");
        ModelAndView modelAndView = new ModelAndView("commercial/customerRequest/create3");
        init(request);
        
        CustomerRequest customerRequest = new CustomerRequest();
        final boolean isCustomerRequestUpdate = this.customerRequestSelected != null
            && this.customerRequestSelected.getId() != null;
        if(isCustomerRequestUpdate){
            customerRequest = entityService.getOne(this.customerRequestSelected.getId());
        }
        modelAndView.addObject("customerRequest", customerRequest);
        modelAndView.addObject("airplaneTypeServiceList", airplaneTypeService.findAll());
        return modelAndView;
    }
    
    /**
     * 
     * @param request
     * @param response
     * @param locale
     * @param session
     * @return 
     */
    @RequestMapping(value = "/customerRequest-create4.htm", method = RequestMethod.GET)
    public ModelAndView create4page(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ CustomerRequestController - create 4 ]");
        ModelAndView modelAndView = new ModelAndView("commercial/customerRequest/create4");
        init(request);
        
        CustomerRequest customerRequest = new CustomerRequest();
        final boolean isCustomerRequestUpdate = this.customerRequestSelected != null
            && this.customerRequestSelected.getId() != null;
        if(isCustomerRequestUpdate){
            customerRequest = entityService.getOne(this.customerRequestSelected.getId());
        }
        modelAndView.addObject("customerRequest", customerRequest);
        modelAndView.addObject("airplaneTypeServiceList", airplaneTypeService.findAll());
        return modelAndView;
    }
    /**
     * 
     * @param request
     * @param response
     * @param locale
     * @param session
     * @return 
     */
    @RequestMapping(value = "/customerRequest-create5.htm", method = RequestMethod.GET)
    public ModelAndView create5page(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ CustomerRequestController - create 5 ]");
        ModelAndView modelAndView = new ModelAndView("commercial/customerRequest/create5");
        init(request);
        
        CustomerRequest customerRequest = new CustomerRequest();
        final boolean isCustomerRequestUpdate = this.customerRequestSelected != null
            && this.customerRequestSelected.getId() != null;
        if(isCustomerRequestUpdate){
            customerRequest = entityService.getOne(this.customerRequestSelected.getId());
        }
        modelAndView.addObject("customerRequest", customerRequest);
        modelAndView.addObject("airplaneTypeServiceList", airplaneTypeService.findAll());
        return modelAndView;
    }
    
    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/customerRequest-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("customerRequest") CustomerRequest customerRequest) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ CustomerRequestController - save ]");
        init(request);
        try {
            customerRequest.setDateLastUpdated();
            customerRequest.setInitDate(new Date());
            customerRequest.setEndDate(new Date());
            customerRequest.setStatus("status");
            customerRequest.setUserCreated(this.userSession.getUser().getUsername());
            customerRequest.setUserLastUpdated(this.userSession.getUser().getUsername());
            entityService.save(customerRequest, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "commercial.customerRequest.save.success", null, locale));
            this.customerRequestSelected = customerRequest;
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.customerRequest.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    
    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/customerRequest-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        RedirectAttributes redirectAttributes,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ CustomerRequestController - edit ] id "+id);
        ModelAndView modelAndView = new ModelAndView("commercial/customerRequest/edit");
        init(request);
        
        CustomerRequest customerRequest = entityService.getOne(id);

        if (customerRequest != null){
            modelAndView.addObject("customerRequest", customerRequest);
            modelAndView.addObject("customerList", customerService.findAll());
            modelAndView.addObject("airplaneList", airplaneService.findAll());
            modelAndView.addObject("serviceTypeList", serviceTypeService.findAll());
            modelAndView.addObject("loadTypeList", loadTypeService.findAll());
            modelAndView.addObject("businessUnitList", businessUnitService.findAll());
            modelAndView.addObject("companyList", companyService.findAll());
        }else{
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","commercial.customerRequest.edit.success");
            return new ModelAndView("redirect:/customerRequest-index.htm");
        }
        return modelAndView;
        
    }
    
    /**
     * 
     * @param model
     * @param request
     * @param locale
     * @param customerRequest
     * @return 
     */
    @RequestMapping(value = "/customerRequest-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("customerRequest") CustomerRequest customerRequest) {

        LOGGER.info("Runing [ CustomerRequestController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            String[] listIdJourneys = request.getParameterValues("journeys.id");
            if(listIdJourneys != null) {
                setJourneys(customerRequest, listIdJourneys);
                LOGGER.info("Existen parametros para trayectos");
            } else {
                LOGGER.error("Pailas. No hay parameros de javascritp de trayectos");
            }
            customerRequest.setDateLastUpdated();
            customerRequest.setInitDate(new Date());
            customerRequest.setEndDate(new Date());
            customerRequest.setStatus("status");
            customerRequest.setUserCreated(this.userSession.getUser().getUsername());
            customerRequest.setUserLastUpdated(this.userSession.getUser().getUsername());
            entityService.update(customerRequest, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "commercial.customerRequest.update.success", null, locale));
            this.customerRequestSelected = customerRequest;
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.customerRequest.update.error", null, locale));

        }

        return Util.getJson(result);
    }
    
    /**
     * Fill information about journeys in a customerRequest.
     * @param customerRequest
     * @param listIdJourneys 
     */
    private void setJourneys(CustomerRequest customerRequest, String [] listIdJourneys) {
        
        Set<Journey> listJourneys = customerRequest.getJourneys();
        if(listJourneys == null) {
            listJourneys = new HashSet<>();
        }
        
        for(String idJourney : listIdJourneys) {
            Journey itemJourney = journeyService.getOne(new Long(idJourney));
            listJourneys.add(itemJourney);
        }
        customerRequest.setJourneys(listJourneys);
    }


    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/customerRequest-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ CustomerRequestController - delete ] id "+id);
        init(request);
        
        CustomerRequest object = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (object != null) {
            try {
                entityService.remove(object, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "commercial.customerRequest.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "commercial.customerRequest.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("commercial.customerRequest.delete.error");
        }
        return Util.getJson(result);
        
    }
    
    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/company-create-crc.htm", method = RequestMethod.GET)
    public ModelAndView createCompany(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ CustomerRequestController - create ]");
        ModelAndView modelAndView = new ModelAndView("commercial/customerRequest/createCompany");
        init(request);
        
        Company company = new Company();
        modelAndView.addObject("company", company);
        return modelAndView;
    }
    
    /**
     * 
     * @param model
     * @param request
     * @param locale
     * @param id
     * @return 
     */
    @RequestMapping(value = "/customerRequest-findById.htm", method = RequestMethod.GET)
    @ResponseBody
    public String getFindById(Model model, HttpServletRequest request, Locale locale,
            @RequestParam(value = "id", required = true) Long id) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Running [ CustomerRequestController - getFindById ]");
        init(request);
        try {
            result.setCode("success");
			result.setData(this.entityService.getOne(id));
            result.setMessage(messageSource.getMessage(
                    "commercial.customerRequest.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "commercial.customerRequest.save.error", null, locale));
        }
        return Util.getJson(result);
    }
    
     public CustomerRequest getCustomerRequestSelected() {
        return customerRequestSelected;
    }
     
    public void setCustomerRequestSelected(
        CustomerRequest customerRequestSelected) {
        this.customerRequestSelected = customerRequestSelected;
    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }
    
    private CustomerRequest getInfoRequestSession(HttpServletRequest request, boolean isRemoveDataInSession){
        final HttpSession session = request.getSession();
        CustomerRequest customerRequestSession = (CustomerRequest)session.getAttribute("customerRequest");
        if(customerRequestSession == null) {
            session.setAttribute("customerRequest", new CustomerRequest());
        } else {
            session.setAttribute("customerRequest", customerRequestSession);
        }
        if(isRemoveDataInSession) {
            session.removeAttribute("customerRequest");
            customerRequestSession = new CustomerRequest();
        }
        return customerRequestSession;
    }
}
