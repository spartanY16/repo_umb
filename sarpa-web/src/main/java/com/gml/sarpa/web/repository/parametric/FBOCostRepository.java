/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: FBOCostRepository.java
 * Created on: Thu Aug 03 09:42:27 COT 2017
 * Project: sarpa
 * Objective: To manage money currencies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.parametric;

import com.gml.sarpa.api.entity.parametric.FBOCost;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To manage fbo costs.
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */

public interface FBOCostRepository extends JpaRepository<FBOCost, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from parametric.fbo_cost  where name ilike %?1% ", nativeQuery = true)
    public List<FBOCost> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from parametric.fbo_cost where id = ?1 ", nativeQuery = true)
    public FBOCost get(Long id);
    
}
