/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: EmployeeController.java
 * Created on: Tue Jul 18 06:52:02 COT 2017
 * Project: sarpa
 * Objective: To manage employees, crew included
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.people;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.web.controller.ControllerDefinition;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.people.Employee;
import com.gml.sarpa.api.services.operation.AirplaneTypeService;
import com.gml.sarpa.api.services.parametric.EmployeePositionService;
import com.gml.sarpa.api.services.people.EmployeeService;
import com.gml.sarpa.api.services.people.PersonService;
import com.gml.sarpa.api.utilities.Util;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * To manage employees, crew included
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class EmployeeController implements ControllerDefinition{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(EmployeeController.class);

    /**
     * Session user information
     */
    private Session userSession;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private EmployeeService entityService;

    @Autowired
    private PersonService personService;
    
    @Autowired
    private AirplaneTypeService airplaneTypeService;
    
    @Autowired
    private EmployeePositionService employeePositionService;
    
    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/employee-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response, Locale locale, HttpSession session,
        RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Runing [ EmployeeController - onDisplay ]");
        ModelAndView modelAndView = new ModelAndView("people/employee/index");
        init(request);
        
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType",flashMap.get("flashType"));
            modelAndView.addObject("flashMessage",flashMap.get("flashMessage"));
        }
        
        modelAndView.addObject("indexList",this.entityService.findAll());

        return modelAndView ;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/employee-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ EmployeeController - create ]");
        ModelAndView modelAndView = new ModelAndView("people/employee/create");
        init(request);
        
        Employee employee = new Employee();
        modelAndView.addObject("employee", employee);
        modelAndView.addObject("personList", personService.findAll());
        modelAndView.addObject("airplaneTypeList", airplaneTypeService.findAll());
        modelAndView.addObject("employeePositionList", employeePositionService.findAll());
        return modelAndView;
    }
    
    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/employee-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("employee") Employee employee) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ EmployeeController - save ]");
        init(request);
        try {
            entityService.save(employee, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "people.employee.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "people.employee.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    
    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/employee-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        RedirectAttributes redirectAttributes,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ EmployeeController - edit ] id "+id);
        ModelAndView modelAndView = new ModelAndView("people/employee/edit");
        init(request);
        
        Employee employee = entityService.getOne(id);

        if (employee != null){
            modelAndView.addObject("employee", employee);
            modelAndView.addObject("personList", personService.findAll());
            modelAndView.addObject("airplaneTypeList", airplaneTypeService.findAll());
            modelAndView.addObject("employeePositionList", employeePositionService.findAll());
        }else{
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","people.employee.edit.success");
            return new ModelAndView("redirect:/employee-index.htm");
        }
        return modelAndView;
        
    }
    
    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/employee-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("employee") Employee employee) {

        LOGGER.info("Runing [ EmployeeController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            entityService.update(employee, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "people.employee.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "people.employee.update.error", null, locale));

        }

        return Util.getJson(result);
    }


    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/employee-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ EmployeeController - delete ] id "+id);
        init(request);
        
        Employee object = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (object != null) {
            try {
                entityService.remove(object, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "people.employee.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "people.employee.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("people.employee.delete.error");
        }
        return Util.getJson(result);
        
    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }
}
