/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContactTypeRepository .java
 * Created on: Fri Jun 30 22:28:37 COT 2017
 * Project: sarpa
 * Objective: To define persons contact data
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.parametric;

import com.gml.sarpa.api.entity.parametric.ContactType;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To define persons contact data
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface ContactTypeRepository extends JpaRepository<ContactType, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from people.contact_type  where name ilike %?1% ", nativeQuery = true)
    public List<ContactType> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from people.contact_type where id = ?1 ", nativeQuery = true)
    public ContactType get(Long id);
}
