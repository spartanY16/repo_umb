/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: AirportRepository .java
 * Created on: Wed Jun 28 07:47:20 COT 2017
 * Project: sarpa
 * Objective: To define airports
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.operation;

import com.gml.sarpa.api.entity.operation.Airport;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To define airports
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface AirportRepository extends JpaRepository<Airport, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from operation.airport  where name ilike %?1% ", nativeQuery = true)
    public List<Airport> getAllByFilter(String filter);

    /**
     * Search entity by FlgDestination
     */
    @Query(value =
        "select * from operation.airport where flg_destination = ?1 order by date_last_updated desc", nativeQuery = true)
    public List<Airport> getAllByFlgDestination(boolean flag);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from operation.airport where id = ?1 ", nativeQuery = true)
    public Airport get(Long id);

    /**
     * Search entity by FlgDestination
     */
    @Query(value =
        "select * from operation.airport where flg_base = ?1 order by date_last_updated desc", nativeQuery = true)
    public List<Airport> getAllByFlgBase(boolean flag);
     /**
     * Search cities in airports
     */
    @Query(value =
        "select distinct(city) from  operation.airport where city ilike %?1% order by city", nativeQuery = true)
    public List<String> getCitiesInAirport(String city);
    
    /**
     * Search airports buy city or Iata Code
     * @param name
     * @param fillValueAirport
     * @return 
     */
    @Query(value =
        "SELECT * FROM operation.airport WHERE " +
        "(0 = ?2 or name ilike %?1% or iata_code ilike %?1% or city ilike %?1% ) " +
        " order by name", nativeQuery = true)
    public List<Airport> getAirportsByCityOrNameOrIataCode(String name, Long fillValueAirport);
}
