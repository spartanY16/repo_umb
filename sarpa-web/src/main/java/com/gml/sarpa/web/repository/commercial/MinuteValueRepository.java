/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: MinuteValueRepository.java
 * Created on: Fri Jul 21 10:43:45 COT 2017
 * Project: sarpa
 * Objective: To manage companies
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.commercial;

import com.gml.sarpa.api.entity.commercial.MinuteValue;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To manage companies
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface MinuteValueRepository extends JpaRepository<MinuteValue, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from commercial.minute_value  where name ilike %?1% ", nativeQuery = true)
    public List<MinuteValue> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from commercial.minute_value where id = ?1 ", nativeQuery = true)
    public MinuteValue get(Long id);
    
    /**
     * Search next code value
     */
    @Query(value =
        "select nextval('commercial.minute_value_sequence') ", nativeQuery = true)
    public Long getNextCode();
}
