/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: PersonController.java
 * Created on: Fri Jun 30 22:34:42 COT 2017
 * Project: sarpa
 * Objective: To define persons contact data
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.people;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.people.Person;
import com.gml.sarpa.api.services.people.PersonService;
import com.gml.sarpa.api.utilities.Util;
import com.gml.sarpa.web.controller.ControllerDefinition;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * To define persons contact data
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Controller
public class PersonController implements ControllerDefinition{

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(PersonController.class);

    /**
     * Session user information
     */
    private Session userSession;
    
    @Autowired
    private PersonService entityService;
    
    @Autowired
    private MessageSource messageSource;


    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/person-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
        HttpServletResponse response, Locale locale, HttpSession session,
        RedirectAttributes redirectAttributes) {
        
        LOGGER.info("Runing [ PersonController - onDisplay ]");
        ModelAndView modelAndView = new ModelAndView("people/person/index");
        init(request);
        
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType",flashMap.get("flashType"));
            modelAndView.addObject("flashMessage",flashMap.get("flashMessage"));
        }
        
        modelAndView.addObject("indexList",this.entityService.findAll());

        return modelAndView ;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/person-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session) {
        
        LOGGER.info("Runing [ PersonController - create ]");
        ModelAndView modelAndView = new ModelAndView("people/person/create");
        init(request);
        
        Person person = new Person();
        modelAndView.addObject("person", person);
        return modelAndView;
    }
    
    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/person-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("person") Person person) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ PersonController - save ]");
        init(request);
        try {
            entityService.save(person, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "people.person.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "people.person.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    
    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/person-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
        HttpServletResponse response,Locale locale , HttpSession session,
        RedirectAttributes redirectAttributes,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ PersonController - edit ] id "+id);
        ModelAndView modelAndView = new ModelAndView("people/person/edit");
        init(request);
        
        Person person = entityService.getOne(id);

        if (person != null){
            modelAndView.addObject("person", person);
            
        }else{
            redirectAttributes.addFlashAttribute("flashType","danger");
            redirectAttributes.addFlashAttribute("flashMessage","people.person.edit.success");
            return new ModelAndView("redirect:/person-index.htm");
        }
        return modelAndView;
        
    }
    
    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/person-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("person") Person person) {

        LOGGER.info("Runing [ PersonController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            entityService.update(person, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "people.person.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "people.person.update.error", null, locale));

        }

        return Util.getJson(result);
    }


    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/person-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
        @RequestParam(value = "id", required = true) Long id ) {

        LOGGER.info("Runing [ PersonController - delete ] id "+id);
        init(request);
        
        Person object = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (object != null) {
            try {
                entityService.remove(object, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "people.person.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "people.person.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("people.person.delete.error");
        }
        return Util.getJson(result);
        
    }
    
    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }
}
