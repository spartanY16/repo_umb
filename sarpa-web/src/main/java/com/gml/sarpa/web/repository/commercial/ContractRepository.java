/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: ContractRepository.java
 * Created on: Fri Jul 21 15:40:44 COT 2017
 * Project: sarpa
 * Objective: To manage clients contracts
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.repository.commercial;

import com.gml.sarpa.api.entity.commercial.Contract;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * To manage clients contracts
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */

public interface ContractRepository extends JpaRepository<Contract, Long> {

    /**
     * Search entity by filter
     */
    @Query(value =
        "select * from commercial.contract  where name ilike %?1% ", nativeQuery = true)
    public List<Contract> getAllByFilter(String filter);

    /**
     * Search entity by id
     */
    @Query(value =
        "select * from commercial.contract where id = ?1 ", nativeQuery = true)
    public Contract get(Long id);
    
    /**
     * Search next code value
     */
    @Query(value =
        "select nextval('commercial.contract_sequence') ", nativeQuery = true)
    public Long getNextCode();


}
