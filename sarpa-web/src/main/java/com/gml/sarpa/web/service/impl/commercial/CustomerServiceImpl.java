/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: CustomerServiceImpl.java
 * Created on: Tue Jul 25 20:03:42 COT 2017
 * Project: sarpa
 * Objective: To manage curstomers
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.commercial;

import com.gml.sarpa.api.entity.commercial.Customer;
import com.gml.sarpa.api.services.commercial.CustomerService;
import com.gml.sarpa.web.repository.commercial.CustomerRepository;
import com.gml.sarpa.api.services.audit.AuditLogService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * To manage curstomers
 *
 * @author <a href="mailto:luzj@gmlsoftware.com">Luz Angelica Jimenez Rozo</a>
 */
@Service(value = "CustomerService")
public class CustomerServiceImpl implements CustomerService{
    
    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(CustomerServiceImpl.class);

    /**
     * Repository for data access
     */
    @Autowired
    private CustomerRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    /**
     * List all entities
     * @return
     */
    @Override
    public List<Customer> findAll(){
        return repository.findAll();
    }

    /**
     * Save entity
     * @param customer 
     */
    @Override
    public void save(Customer  customer ,String userName){
        LOGGER.info("Runing [ CustomerServiceImpl - save ]");
        customer.setVersion(0);
        customer.setUserCreated(userName);
        customer.setDateCreated();
        customer.setUserLastUpdated(userName);
        customer.setDateLastUpdated();
        if (null != customer.getCompany()){
            customer.setPerson(null);
            customer.setCode("CLI-PJ-"+repository.getNextCode());
        }else{
            customer.setCode("CLI-PN-"+repository.getNextCode());
        }
        repository.save(customer );
        auditLogService.auditInsert(customer.getClass().getName(),
                customer.getId(),customer.getVersion(),
                customer.toString(),userName);
    }

    /**
     * Update entity
     * @param customer 
     */
    @Override
    public void update(Customer  customer ,String userName){
        LOGGER.info("Runing [ CustomerServiceImpl - update ]");
        Customer  currentCustomer  = getOne(customer.getId());
        customer.setVersion(currentCustomer.getVersion());
        customer.setDateCreated(currentCustomer.getDateCreated());
        customer.setUserCreated(currentCustomer.getUserCreated());
        customer.setUserLastUpdated(userName);
        customer.setDateLastUpdated();
        if (null != customer.getCompany()){
            customer.setPerson(null);
        }    
        repository.save(customer );
        if(!currentCustomer.getYear().equals(customer.getYear())){
            auditLogService.auditUpdate(customer.getClass().getName(),
            customer.getId(),customer.getVersion(),
            currentCustomer.toString(),"year",customer.getYear(),userName);
        }
        if(!currentCustomer.getStatus().equals(customer.getStatus())){
            auditLogService.auditUpdate(customer.getClass().getName(),
            customer.getId(),customer.getVersion(),
            currentCustomer.toString(),"status",customer.getStatus(),userName);
        }
        if(!currentCustomer.getDetails().equals(customer.getDetails())){
            auditLogService.auditUpdate(customer.getClass().getName(),
            customer.getId(),customer.getVersion(),
            currentCustomer.toString(),"details",customer.getDetails(),userName);
        }
    }

    /**
     * Delete entity
     *@param customer 
     */
    @Override
    public void remove(Customer  customer ,String userName){
        repository.delete(customer );
        auditLogService.auditDelete(customer.getClass().getName(),
                customer.getId(),customer.getVersion(),
                customer.toString(),userName);
    }

    

    /**
     * Get entity by id
     * @param id
     * @return
     */
    @Override
    public Customer getOne(Long id){
        LOGGER.info("Runing [ CustomerServiceImpl - getOne ] for id "+id);
        return repository.get(id);
    }

    /**
     * Search entity by filter
     * @param filter
     * @return
     */
    @Override
    public List<Customer> getAllByFilter(String filter){
        return repository.getAllByFilter(filter);
    }

    
}
