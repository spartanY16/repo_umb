/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Document: AirAmbulanceServiceImpl.java
 * Created on: 2017/09/06, 03:27:31 PM
 * Project: SIMPC - Sistema de informaci&ocute;n para el mecanismo de 
 *                  protecci&oacute;n al cesante.
 * 
 * Copyright 2016 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.service.impl.commercial;

import com.gml.sarpa.api.entity.commercial.AirAmbulance;
import com.gml.sarpa.api.services.audit.AuditLogService;
import com.gml.sarpa.api.services.commercial.AirAmbulanceService;
import com.gml.sarpa.web.repository.commercial.AirAmbulanceRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
@Service(value = "AirAmbulanceService")
public class AirAmbulanceServiceImpl implements AirAmbulanceService {
       
    /**
     * Repository for data access
     */
    @Autowired
    private AirAmbulanceRepository repository;

    /**
     * Service for audit actions
     */
    @Autowired
    private AuditLogService auditLogService;

    @Override
    public List<AirAmbulance> findAll() {
        return repository.findAll();
    }

    @Override
    public void save(AirAmbulance airAmbulance, String userName) {
        airAmbulance.setVersion(0);
        airAmbulance.setUserCreated(userName);
        airAmbulance.setDateCreated();
        airAmbulance.setUserLastUpdated(userName);
        airAmbulance.setDateLastUpdated();
        repository.save(airAmbulance);
        auditLogService.auditInsert(airAmbulance.getClass().getName(),
            airAmbulance.getId(), airAmbulance.getVersion(),
            airAmbulance.toString(), userName);
    }

    @Override
    public void update(AirAmbulance airAmbulance, String userName) {
        AirAmbulance currentAirAmbulance = getOne(airAmbulance.getId());
        airAmbulance.setVersion(currentAirAmbulance.getVersion());
        airAmbulance.setUserLastUpdated(userName);
        airAmbulance.setDateLastUpdated();
        airAmbulance.setDateCreated(currentAirAmbulance.getDateCreated());
        airAmbulance.setUserCreated(currentAirAmbulance.getUserCreated());
        repository.save(airAmbulance);

        auditLogService.auditInsert(airAmbulance.getClass().getName(),
            airAmbulance.getId(), airAmbulance.getVersion(),
            airAmbulance.toString(), userName);
    }

    @Override
    public void remove(AirAmbulance airAmbulance, String userName) {
        repository.delete(airAmbulance);
        auditLogService.auditDelete(airAmbulance.getClass().getName(),
            airAmbulance.getId(), airAmbulance.getVersion(),
            airAmbulance.toString(), userName);
    }

    @Override
    public AirAmbulance getOne(Long id) {
        return repository.get(id);
    }

    @Override
    public List<AirAmbulance> getAllByFilter(String filter) {
        return repository.getAllByFilter(filter);
    }

}
