/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: TypeOfCompanyRelationshipController.java
 * Created on: Thu Jun 29 15:22:59 COT 2017
 * Project: sarpa
 * Objective: To define business units
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
package com.gml.sarpa.web.controller.impl.parametric;

import com.gml.sarpa.api.dto.ResponseDto;
import com.gml.sarpa.api.entity.auth.Session;
import com.gml.sarpa.api.entity.parametric.TypeOfCompanyRelationship;
import com.gml.sarpa.api.services.parametric.TypeOfCompanyRelationshipService;
import com.gml.sarpa.api.utilities.Util;
import com.gml.sarpa.web.controller.ControllerDefinition;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import static com.gml.sarpa.api.utilities.Util.SESSION_USER;

/**
 * To define business units
 *
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 */
@Controller
public class TypeOfCompanyRelationshipController implements ControllerDefinition {

    /**
     * Logger for class
     */
    private static final Logger LOGGER = Logger.getLogger(TypeOfCompanyRelationshipController.class);

    /**
     * Session user information
     */
    private Session userSession;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private TypeOfCompanyRelationshipService entityService;

    /**
     * Method for list entity
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/typeofcompanyrelationship-index.htm", method = RequestMethod.GET)
    @Override
    public ModelAndView onDisplay(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session,
            RedirectAttributes redirectAttributes) {

        LOGGER.info("Runing [ TypeOfCompanyRelationshipController - onDisplay ]");
        init(request);
        ModelAndView modelAndView = new ModelAndView("parametric/typeOfCompanyRelationship/index");

        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            modelAndView.addObject("flashType", flashMap.get("flashType"));
            modelAndView.addObject("flashMessage", flashMap.get("flashMessage"));
        }

        modelAndView.addObject("indexList", this.entityService.findAll());

        return modelAndView;
    }

    /**
     * Method for add new entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     *
     * @return
     */
    @RequestMapping(value = "/typeofcompanyrelationship-create.htm", method = RequestMethod.GET)
    public ModelAndView create(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session) {

        LOGGER.info("Runing [ TypeOfCompanyRelationshipController - create ]");
        init(request);
        ModelAndView modelAndView = new ModelAndView("parametric/typeOfCompanyRelationship/create");

        TypeOfCompanyRelationship typeOfCompanyRelationship = new TypeOfCompanyRelationship();
        modelAndView.addObject("typeOfCompanyRelationship", typeOfCompanyRelationship);
        return modelAndView;
    }

    /**
     * Method for save payment method information
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param tradeId
     * @param referenceNumber
     * @param redirectAttributes
     *
     * @return
     */
    @RequestMapping(value = "/typeofcompanyrelationship-save.htm", method = RequestMethod.POST)
    @ResponseBody
    public String save(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("typeOfCompanyRelationship") TypeOfCompanyRelationship typeOfCompanyRelationship) {

        ResponseDto result = new ResponseDto();
        LOGGER.info("Runing [ TypeOfCompanyRelationshipController - save ]");
        init(request);
        try {
            entityService.save(typeOfCompanyRelationship, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "parametric.typeofcompanyrelationship.save.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "parametric.typeofcompanyrelationship.save.error", null, locale));
        }
        return Util.getJson(result);
    }

    /**
     * Method for modify entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/typeofcompanyrelationship-edit.htm", method = RequestMethod.GET)
    public ModelAndView edit(HttpServletRequest request,
            HttpServletResponse response, Locale locale, HttpSession session,
            RedirectAttributes redirectAttributes,
            @RequestParam(value = "id", required = true) Long id) {

        LOGGER.info("Runing [ TypeOfCompanyRelationshipController - edit ] id " + id);
        ModelAndView modelAndView = new ModelAndView("parametric/typeOfCompanyRelationship/edit");
        init(request);
        TypeOfCompanyRelationship typeOfCompanyRelationship = entityService.getOne(id);

        if (typeOfCompanyRelationship != null) {
            modelAndView.addObject("typeOfCompanyRelationship", typeOfCompanyRelationship);

        } else {
            redirectAttributes.addFlashAttribute("flashType", "danger");
            redirectAttributes.addFlashAttribute("flashMessage", "parametric.typeofcompanyrelationship.edit.success");
            return new ModelAndView("redirect:/typeofcompanyrelationship-index.htm");
        }
        return modelAndView;

    }

    /**
     * Method for update entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param entity
     *
     * @return
     */
    @RequestMapping(value = "/typeofcompanyrelationship-update.htm", method = RequestMethod.POST)
    @ResponseBody
    public String update(Model model, HttpServletRequest request, Locale locale,
            @ModelAttribute("typeOfCompanyRelationship") TypeOfCompanyRelationship typeOfCompanyRelationship) {

        LOGGER.info("Runing [ TypeOfCompanyRelationshipController - update ] ");
        ResponseDto result = new ResponseDto();
        init(request);
        try {
            entityService.update(typeOfCompanyRelationship, this.userSession.getUser().getUsername());
            result.setCode("success");
            result.setMessage(messageSource.getMessage(
                    "parametric.typeofcompanyrelationship.update.success", null, locale));
        } catch (Exception e) {
            LOGGER.error(e);
            result.setCode("danger");
            result.setMessage(messageSource.getMessage(
                    "parametric.typeofcompanyrelationship.update.error", null, locale));

        }

        return Util.getJson(result);
    }

    /**
     * Method for delete entity
     *
     * @param request
     * @param response
     * @param locale
     * @param session
     * @param redirectAttributes
     * @param id
     *
     * @return
     */
    @RequestMapping(value = "/typeofcompanyrelationship-delete.htm", method = RequestMethod.GET)
    @ResponseBody
    public String delete(Model model, HttpServletRequest request, Locale locale,
            @RequestParam(value = "id", required = true) Long id) {

        LOGGER.info("Runing [ TypeOfCompanyRelationshipController - delete ] id " + id);
        ModelAndView modelAndView = new ModelAndView("redirect:/typeofcompanyrelationship-index.htm");
        init(request);
        TypeOfCompanyRelationship typeOfCompanyRelationship = entityService.getOne(id);
        ResponseDto result = new ResponseDto();

        if (typeOfCompanyRelationship != null) {
            try {
                entityService.remove(typeOfCompanyRelationship, this.userSession.getUser().getUsername());
                result.setCode("success");
                result.setMessage(messageSource.getMessage(
                        "parametric.typeofcompanyrelationship.delete.success", null, locale));
            } catch (Exception e) {
                LOGGER.error(e);
                result.setCode("danger");
                result.setMessage(messageSource.getMessage(
                        "parametric.typeofcompanyrelationship.delete.error", null, locale));
            }
        } else {
            result.setCode("danger");
            result.setMessage("parametric.typeofcompanyrelationship.delete.error");
        }
        return Util.getJson(result);

    }

    private void init(HttpServletRequest request) {
        this.userSession = (Session) request.getSession().
                getAttribute(SESSION_USER);
    }

}
