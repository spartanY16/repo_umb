<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#createFrm").validate({
            rules: {
                serviceType {
                    required: true
                },
                airplaneType {
                    required: true
                },
                flgInternational{
                    required: true
                },
                value: {
                    required: true
                }
            },
            messages: {
                serviceType {
                    required: '<spring:message code="commercial.minuteValue.serviceType.errors.required" />'
                },
                airplaneType {
                    required: '<spring:message code="commercial.minuteValue.airplaneType.errors.required" />'
                },
                flgInternational {
                    required: '<spring:message code="commercial.minuteValue.airplaneType.errors.required" />'
                },
                value: {
                    required: '<spring:message code="commercial.minuteValue.value.errors.required" />'
                }
            }
        });
    });
</script>