<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true, 
            pageSettings: { pageSize: 50 },
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },
            columns: [
                    { field: "serviceType", headerText: '<spring:message code="commercial.minuteValue.serviceType.label" />', width: 105 },
                    { field: "airplaneType", headerText: '<spring:message code="commercial.minuteValue.airplaneType.label" />', width: 105 },
                    { field: "flgInternational", headerText: '<spring:message code="commercial.minuteValue.flgInternational.label" />', width: 105 },
                    { field: "value", headerText: '<spring:message code="commercial.minuteValue.value.label" />', width: 105 },
                    { field: "actions", headerText: '<spring:message code="common.table.actions" />', width: 85, textAlign: ej.TextAlign.Right }
            ]
        });
        
    });
</script>