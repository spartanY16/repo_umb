<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true, 
            columns: [
                    { field: "name", headerText: '<spring:message code="commercial.airAmbulance.name.label" />', width: 105 },
                    { field: "description", headerText: '<spring:message code="commercial.airAmbulance.description.label" />', width: 105 },
                    { field: "actions", headerText: '<spring:message code="common.table.actions" />', width: 85, textAlign: ej.TextAlign.Right },
                    
            ]
        });
        
    });
</script>