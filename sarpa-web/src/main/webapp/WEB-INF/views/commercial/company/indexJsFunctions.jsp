<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true,
            pageSettings: { pageSize: 50 },
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },
            columns: [
                {field: "code", headerText: "<spring:message code="commercial.company.code.label" />", width: 105},
                {field: "name", headerText: '<spring:message code="commercial.company.name.label" />', width: 105},
                {field: "nit", headerText: '<spring:message code="commercial.company.nit.label" />', width: 105},
                {field: "address", headerText: '<spring:message code="commercial.company.address.label" />', width: 105},
                {field: "phone", headerText: '<spring:message code="commercial.company.phone.label" />', width: 105},
                {field: "actions", headerText: '<spring:message code="common.table.actions" />', width: 85, textAlign: ej.TextAlign.Right}
            ]
        });

    });
</script>