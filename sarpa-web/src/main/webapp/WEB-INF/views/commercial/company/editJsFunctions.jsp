<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#createFrm").validate({
            rules: {
                name: {
                    required: true
                },
                nit: {
                    required: true
                },
                address: {
                    required: true
                },
                phone: {
                    required: true
                },
                regimeType: {
                    required: true
                },
                retentionType: {
                    required: true
                },
                detail: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: '<spring:message code="commercial.company.name.errors.required" />'
                },
                nit: {
                    required: '<spring:message code="commercial.company.nit.errors.required" />'
                },
                address: {
                    required: '<spring:message code="commercial.company.address.errors.required" />'
                },
                phone: {
                    required: '<spring:message code="commercial.company.phone.errors.required" />'
                },
                regimeType: {
                    required: '<spring:message code="commercial.company.regimeType.errors.required" />'
                },
                retentionType: {
                    required: '<spring:message code="commercial.company.retentionType.errors.required" />'
                },
                detail: {
                    required: '<spring:message code="commercial.company.detail.errors.required" />'
                }
            }
        });
    });
</script>