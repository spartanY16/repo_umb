<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#createFrm").validate({
            rules: {
                code: {
                    required: true
                },
                customer: {
                    required: true
                },
                contractValue: {
                    required: true
                },
                initDate: {
                    required: true
                },
                endDate: {
                    required: true
                },
                discountPercent: {
                    required: true
                },
                details: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: '<spring:message code="commercial.contract.code.errors.required" />'
                },
                customer: {
                    required: '<spring:message code="commercial.contract.customer.errors.required" />'
                },
                contractValue: {
                    required: '<spring:message code="commercial.contract.contractValue.errors.required" />'
                },
                initDate: {
                    required: '<spring:message code="commercial.contract.initDate.errors.required" />'
                },
                endDate: {
                    required: '<spring:message code="commercial.contract.endDate.errors.required" />'
                },
                discountPercent: {
                    required: '<spring:message code="commercial.contract.discountPercent.errors.required" />'
                },
                details: {
                    required: '<spring:message code="commercial.contract.details.errors.required" />'
                }
            }
        });
    });
</script>