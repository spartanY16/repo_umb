<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true,
            pageSettings: { pageSize: 50 },
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },
            columns: [
                {field: "code", headerText: '<spring:message code="commercial.contract.code.label" />', width: 105},
                {field: "customer", headerText: '<spring:message code="commercial.contract.customer.label" />', width: 105},
                {field: "contractValue", headerText: '<spring:message code="commercial.contract.contractValue.label" />', width: 105},
                {field: "initDate", headerText: '<spring:message code="commercial.contract.initDate.label" />', width: 105},
                {field: "endDate", headerText: '<spring:message code="commercial.contract.endDate.label" />', width: 105},
                {field: "discountPercent", headerText: '<spring:message code="commercial.contract.discountPercent.label" />', width: 105},
                {field: "details", headerText: '<spring:message code="commercial.contract.details.label" />', width: 105},
                {field: "actions", headerText: '<spring:message code="common.table.actions" />', width: 85, textAlign: ej.TextAlign.Right}

            ]
        });

    });
</script>