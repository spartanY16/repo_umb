<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#editFrm").validate({
            rules: {
                name : {
                        required: true
                    }
                description : {
                        required: true
                    }
                detail : {
                        required: true
                    }
                requiresJourney : {
                        required: true
                    }
                            },
            messages: {
                name : {
                        required: '<spring:message code="commercial.contractType.name.errors.required" />'
                },
                description : {
                        required: '<spring:message code="commercial.contractType.description.errors.required" />'
                },
                detail : {
                        required: '<spring:message code="commercial.contractType.detail.errors.required" />'
                },
                requiresJourney : {
                        required: '<spring:message code="commercial.contractType.requiresJourney.errors.required" />'
                },
                            }
        });
    });
</script>