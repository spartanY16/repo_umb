<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#createFrm").validate({
            rules: {
                person : {
                        required: true
                    },
                company : {
                        required: true
                    },
                accountExecutive : {
                        required: true
                    },
                businessUnit : {
                        required: true
                    },
                year : {
                        required: true
                    },
                status : {
                        required: true
                    },
                details : {
                        required: true
                    },
                code : {
                        required: true
                    }

            },
            messages: {
                person : {
                        required: '<spring:message code="commercial.customer.person.errors.required" />'
                },
                company : {
                        required: '<spring:message code="commercial.customer.company.errors.required" />'
                },
                accountExecutive : {
                        required: '<spring:message code="commercial.customer.accountExecutive.errors.required" />'
                },
                businessUnit : {
                        required: '<spring:message code="commercial.customer.businessUnit.errors.required" />'
                },
                year : {
                        required: '<spring:message code="commercial.customer.year.errors.required" />'
                },
                status : {
                        required: '<spring:message code="commercial.customer.status.errors.required" />'
                },
                details : {
                        required: '<spring:message code="commercial.customer.details.errors.required" />'
                },
                code : {
                        required: '<spring:message code="commercial.customer.code.errors.required" />'
                }
            }
        });
    });
</script>