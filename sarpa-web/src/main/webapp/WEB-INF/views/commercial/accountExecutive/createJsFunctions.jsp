<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#createFrm").validate({
            rules: {
                person: {
                    required: true
                }
            },
            messages: {
                person: {
                    required: '<spring:message code="commercial.accountExecutive.person.errors.required" />'
                }
            }
        });
    });
</script>