<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#createFrm").validate({
            rules: {
                businessUnit : {
                    required: true
                },
                caller : {
                    required: true
                },
                company : {
                    required: true
                },
                serviceType : {
                    required: true
                },
                probableflightdate : {
                    required: true
                },
                probableflighthour : {
                    required: true
                }
            },
            messages: {
                businessUnit : {
                        required: '<spring:message code="commercial.customerRequest.businessunit.required.label" />'
                },
                caller : {
                        required: '<spring:message code="commercial.customerRequest.caller.required.label" />'
                },
                company : {
                        required: '<spring:message code="commercial.customerRequest.company.required.label" />'
                },
                serviceType : {
                        required: '<spring:message code="commercial.customerRequest.typeofservice.required.label" />'
                },
                probableflightdate : {
                        required: '<spring:message code="commercial.customerRequest.probableflightdate.required.label" />'
                },
                probableflighthour : {
                        required: '<spring:message code="commercial.customerRequest.probableflighthour.required.label" />'
                }
            }
        });
        
        $("#fieldOtherLoadType").hide();
        $("#divFiO2").hide();
        
        $("#probableflightdateTxt").ejDatePicker( 
            { 
                dateFormat: "dd/MM/yyyy"
            }
        );
        $("#probableflighthourTxt").ejTimePicker( 
            { 
                dateFormat: "HH:MM"
            }
        );
        $("#idRequiredGroundAmbulance").change(function(){
            var selectedValue = $("#idRequiredGroundAmbulance").val();
            if(selectedValue === "true"){
                $("#idGroundAmbulanceConfiguration").prop('disabled', false);
            } else {
                $("#idGroundAmbulanceConfiguration").prop('disabled', true);
            }      
        });
        
        $("#idIncludesRescueEquipment").change(function(){
            var selectedValue = $("#idIncludesRescueEquipment").val();
            if(selectedValue === "true"){
                $("#numberOfPassengersTxt").prop('disabled', false);
            } else {
                $("#numberOfPassengersTxt").prop('disabled', true);
            }      
        });
        
        $("#idLoadType").change(function(){
            var selectedValue = $('#idLoadType option:selected').text();
            if(selectedValue === "Otros"){
                $("#fieldOtherLoadType").show();
            } else {
                $("#fieldOtherLoadType").hide();
            }      
        });
        
        $("#idRequiresOxygen").change(function(){
            var selectedValue = $("#idRequiresOxygen").val();
            if(selectedValue === "true"){
                $("#divFiO2").show();
            } else {
                $("#divFiO2").hide();
            }      
        });
        
        $("#idLoadTypeLongNA").change(function(){
            var selectedValue = $("#idLoadTypeLongNA").val();
            if(selectedValue === "Si"){
                $("#idloadTypeLong").prop('disabled', false);
            } else {
                $("#idloadTypeLong").prop('disabled', true);
            }      
        });
        
        $("#idLoadTypeWidthNA").change(function(){
            var selectedValue = $("#idLoadTypeWidthNA").val();
            if(selectedValue === "Si"){
                $("#idLoadTypeWidth").prop('disabled', false);
            } else {
                $("#idLoadTypeWidth").prop('disabled', true);
            }      
        });
        
        $("#idLoadTypeHighNA").change(function(){
            var selectedValue = $("#idLoadTypeHighNA").val();
            if(selectedValue === "Si"){
                $("#idloadTypeHigh").prop('disabled', false);
            } else {
                $("#idloadTypeHigh").prop('disabled', true);
            }      
        });
        
        $("#idLoadTypeWeightPerPieceNA").change(function(){
            var selectedValue = $("#idLoadTypeWeightPerPieceNA").val();
            if(selectedValue === "Si"){
                $("#idLoadTypeWeightPerPiece").prop('disabled', false);
            } else {
                $("#idLoadTypeWeightPerPiece").prop('disabled', true);
            }      
        });
        
        $('#serviceTypeId').children('option[value="5"]').css('','none');

    });
</script>