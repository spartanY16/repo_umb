<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#submitBtn").ejButton({
            height: 30,
            width: 130,
            imagePosition: "imageleft",
            contentType: "textandimage",
            showRoundedCorner: true,
            prefixIcon: "e-icon e-handup"
        });

    $("#cancelBtn").ejButton({
            height: 30,
            width: 130,
            imagePosition: "imageleft",
            contentType: "textandimage",
            showRoundedCorner: true,
            prefixIcon: "e-icon e-handup"
        });
        
        $("#createFrm").validate({
            rules: {
                name : {
                    required: true
                }
            },
            messages: {
                name : {
                                            required: "<spring:message code="audit .auditLog .date .errors.required" />",
                                            required: "<spring:message code="audit .auditLog .user .errors.required" />",
                                            required: "<spring:message code="audit .auditLog .className .errors.required" />",
                                            required: "<spring:message code="audit .auditLog .action .errors.required" />",
                                            required: "<spring:message code="audit .auditLog .objectId .errors.required" />",
                                            required: "<spring:message code="audit .auditLog .objectVersion .errors.required" />",
                                            required: "<spring:message code="audit .auditLog .property .errors.required" />",
                                            required: "<spring:message code="audit .auditLog .value .errors.required" />",
                                    }
                
            }
        });
    });
</script>