<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    function confirmElimination() {
        confirm("<spring:message code="common.delete.confirm" />");
    }

    jQuery(function ($) {
        
        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true, 
            columns: [
                                            { field: "date", headerText: "<spring:message code="audit .auditLog .date .label" />", width: 105 },
                                            { field: "user", headerText: "<spring:message code="audit .auditLog .user .label" />", width: 105 },
                                            { field: "className", headerText: "<spring:message code="audit .auditLog .className .label" />", width: 105 },
                                            { field: "action", headerText: "<spring:message code="audit .auditLog .action .label" />", width: 105 },
                                            { field: "objectId", headerText: "<spring:message code="audit .auditLog .objectId .label" />", width: 105 },
                                            { field: "objectVersion", headerText: "<spring:message code="audit .auditLog .objectVersion .label" />", width: 105 },
                                            { field: "property", headerText: "<spring:message code="audit .auditLog .property .label" />", width: 105 },
                                            { field: "value", headerText: "<spring:message code="audit .auditLog .value .label" />", width: 105 },
                                        { field: "actions", headerText: "<spring:message code="common.table.actions" />", width: 85, textAlign: ej.TextAlign.Right },
                    
            ]
        });
        
    });
</script>