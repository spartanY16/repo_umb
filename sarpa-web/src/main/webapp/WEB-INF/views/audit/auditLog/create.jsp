<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title><spring:message code="index.title" /></title>
        
        <link href="assets/css/web/gradient-azure/ej.web.all.min.css" rel="stylesheet" />
        <link href="assets/css/web/default.css" rel="stylesheet" />
        
        <%@ include file="/WEB-INF/views/common/javaScripts.jsp" %>
    </head>
    <body>
        
        <div class="content-container-fluid">
            <div class="row">
                <%@ include file="/WEB-INF/views/common/internalHeader.jsp" %>
            </div>
            
            <div class="row">
                <h2><spring:message code="audit.auditLog .create.title" /></h2>
            </div>
            
            <div class="row">
                <div class="cols-sample-area">
                    <div class="frame frame-lg">
                        <div class="control">
                            <form:form method="post" action="auditLog -save.htm" modelAttribute="auditLog" id="createFrm">
                                <table class="editors">
                                    <tbody>
                                                                                    <tr>
                                                <td>
                                                    <span><spring:message code="audit.auditLog .date .label" /></span>
                                                </td>
                                                <td>
                                                    <form:input id="date Txt" path="date"/>
                                                </td>
                                            </tr>
                                                                                    <tr>
                                                <td>
                                                    <span><spring:message code="audit.auditLog .user .label" /></span>
                                                </td>
                                                <td>
                                                    <form:input id="user Txt" path="user"/>
                                                </td>
                                            </tr>
                                                                                    <tr>
                                                <td>
                                                    <span><spring:message code="audit.auditLog .className .label" /></span>
                                                </td>
                                                <td>
                                                    <form:input id="className Txt" path="className"/>
                                                </td>
                                            </tr>
                                                                                    <tr>
                                                <td>
                                                    <span><spring:message code="audit.auditLog .action .label" /></span>
                                                </td>
                                                <td>
                                                    <form:input id="action Txt" path="action"/>
                                                </td>
                                            </tr>
                                                                                    <tr>
                                                <td>
                                                    <span><spring:message code="audit.auditLog .objectId .label" /></span>
                                                </td>
                                                <td>
                                                    <form:input id="objectId Txt" path="objectId"/>
                                                </td>
                                            </tr>
                                                                                    <tr>
                                                <td>
                                                    <span><spring:message code="audit.auditLog .objectVersion .label" /></span>
                                                </td>
                                                <td>
                                                    <form:input id="objectVersion Txt" path="objectVersion"/>
                                                </td>
                                            </tr>
                                                                                    <tr>
                                                <td>
                                                    <span><spring:message code="audit.auditLog .property .label" /></span>
                                                </td>
                                                <td>
                                                    <form:input id="property Txt" path="property"/>
                                                </td>
                                            </tr>
                                                                                    <tr>
                                                <td>
                                                    <span><spring:message code="audit.auditLog .value .label" /></span>
                                                </td>
                                                <td>
                                                    <form:input id="value Txt" path="value"/>
                                                </td>
                                            </tr>
                                                                                <tr>
                                            <td>
                                                <br/>
                                                <div class="frmSubmit">
                                                    <input type="submit" class="e-btn" id="submitBtn" value="<spring:message code="common.form.save" />">
                                                    <a href="auditLog -index.htm" class="e-btn" id="cancelBtn"><spring:message code="common.form.cancel" /></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table> 
                            </form:form>    
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
               <%@ include file="/WEB-INF/views/common/footer.jsp" %>
            </div>
        </div>
        <%@ include file="/WEB-INF/views/audit/auditLog/createJsFunctions.jsp" %>    
    </body>
</html>    
