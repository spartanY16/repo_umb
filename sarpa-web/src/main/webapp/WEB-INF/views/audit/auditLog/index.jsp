<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title><spring:message code="index.title" /></title>
        
        <link href="assets/css/web/gradient-azure/ej.web.all.min.css" rel="stylesheet" />
        <link href="assets/css/web/default.css" rel="stylesheet" />
        
        <%@ include file="/WEB-INF/views/common/javaScripts.jsp" %>
        <%@ include file="/WEB-INF/views/common/tableJavaScripts.jsp" %>
    </head>
    <body>
        
        <div class="content-container-fluid">
            <div class="row">
                <%@ include file="/WEB-INF/views/common/internalHeader.jsp" %>
            </div>
            <div class="row">
                <h2><spring:message code="audit.auditLog .index.title" /> </h2>
            </div>
                
            
            <div class="row">
                <div class="cols-sample-area">
                    <div id="Grid"></div>
                </div>
            </div>
            <div class="row">
               <%@ include file="/WEB-INF/views/common/footer.jsp" %>
            </div>
        </div>
        <table id="indexTbl">
            <colgroup>
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                    <col />
                                <col />
            </colgroup>
            <thead>
                <tr>
                                            <th>date</th>
                                            <th>user</th>
                                            <th>className</th>
                                            <th>action</th>
                                            <th>objectId</th>
                                            <th>objectVersion</th>
                                            <th>property</th>
                                            <th>value</th>
                                        <th>actions</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${indexList}" var="item">
                    <tr>
                                                <td>$ {item.date}</td>
                                                <td>$ {item.user}</td>
                                                <td>$ {item.className}</td>
                                                <td>$ {item.action}</td>
                                                <td>$ {item.objectId}</td>
                                                <td>$ {item.objectVersion}</td>
                                                <td>$ {item.property}</td>
                                                <td>$ {item.value}</td>
                                                <td>
                            <a id="updateLnk" href="auditLog -edit.htm?id=${item.id}" ><spring:message code="common.form.update" /></a>
                        </td>                    
                    </tr>
                </c:forEach>
                
            </tbody>
        </table>
        <%@ include file="/WEB-INF/views/audit/auditLog/indexJsFunctions.jsp" %>    
    </body>
</html>    
