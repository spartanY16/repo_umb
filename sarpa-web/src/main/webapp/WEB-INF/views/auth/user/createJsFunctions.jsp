<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#createFrm").validate({
            rules: {
                username : {
                    required: true
                },
                password : {
                    required: true
                },
                person_id : {
                    required: true
                }
            },
            messages: {
                username : {
                        required: '<spring:message code="auth.user.username.errors.required" />'
                },
                password : {
                        required: '<spring:message code="auth.user.password.errors.required" />'
                },
                person_id : {
                        required: '<spring:message code="auth.user.person_id.errors.required" />'
                } 
            }
        });
    });
</script>