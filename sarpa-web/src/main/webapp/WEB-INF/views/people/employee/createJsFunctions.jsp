<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#createFrm").validate({
            rules: {
                person: {
                    required: true
                },
                airplaneType: {
                    required: true
                },
                type: {
                    required: true
                },
                position: {
                    required: true
                },
                birthDay: {
                    required: true
                },
                operativeCode: {
                    required: true
                },
                contractType: {
                    required: true
                },
                salary: {
                    required: true
                },
                anualBonus: {
                    required: true
                },
                admissionDate: {
                    required: true
                },
                rh: {
                    required: true
                }

            },
            messages: {
                person: {
                    required: '<spring:message code="people.employee.person.errors.required" />'
                },
                airplaneType: {
                    required: '<spring:message code="people.employee.airplaneType.errors.required" />'
                },
                type: {
                    required: '<spring:message code="people.employee.type.errors.required" />'
                },  
                position: {
                    required: '<spring:message code="people.employee.position.errors.required" />'
                },
                birthDay: {
                    required: '<spring:message code="people.employee.birthDay.errors.required" />'
                },
                operativeCode: {
                    required: '<spring:message code="people.employee.operativeCode.errors.required" />'
                },
                contractType: {
                    required: '<spring:message code="people.employee.contractType.errors.required" />'
                },
                salary: {
                    required: '<spring:message code="people.employee.salary.errors.required" />'
                },
                anualBonus: {
                    required: '<spring:message code="people.employee.anualBonus.errors.required" />'
                },
                admissionDate: {
                    required: '<spring:message code="people.employee.admissionDate.errors.required" />'
                },
                rh: {
                    required: '<spring:message code="people.employee.rh.errors.required" />'
                }

            }
        });
    });
</script>