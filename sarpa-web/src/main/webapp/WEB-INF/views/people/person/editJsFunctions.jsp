<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#editFrm").validate({
            rules: {
                firstName : {
                    required: true
                },
                lastName : {
                    required: true
                },
                documentNumber : {
                    required: true
                },
                documentType : {
                    required: true
                }
            },
            messages: {
                firstName : {
                    required: '"<spring:message code="people.person.firstName.errors.required" />',
                },
                lastName : {
                    required: '<spring:message code="people.person.lastName.errors.required" />',
                },
                documentNumber : {
                    required: '<spring:message code="people.person.documentNumber.errors.required" />',
                },
                documentType : {
                    required: '<spring:message code="people.person.documentType.errors.required" />',
                }
            }
        });
    });
</script>