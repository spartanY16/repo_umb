<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true, 
            pageSettings: { pageSize: 50 },
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },
            columns: [
                { field: "person", headerText: '<spring:message code="people.contact.person.label" />', width: 105 },
                { field: "contactType", headerText: '<spring:message code="people.contact.contactType.label" />', width: 105 },
                { field: "value", headerText: '<spring:message code="people.contact.value.label" />', width: 105 },
                { field: "alias", headerText: '<spring:message code="people.contact.alias.label" />', width: 105 },
                { field: "actions", headerText: '<spring:message code="common.table.actions" />', width: 85, textAlign: ej.TextAlign.Right }
                    
            ]
        });
        
    });
</script>