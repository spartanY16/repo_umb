<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#editFrm").validate({
            rules: {
                person: {
                    required: true
                },
                contactType: {
                    required: true
                },
                value: {
                    required: true
                },
                alias: {
                    required: true
                }
            },
            messages: {
                person: {
                    required: '<spring:message code="people.contact.person.errors.required" />'
                },
                contactType: {
                    required: '<spring:message code="people.contact.contactType.errors.required" />'
                },
                value: {
                    required: '<spring:message code="people.contact.value.errors.required" />'
                },
                alias: {
                    required: '<spring:message code="people.contact.alias.errors.required" />'
                }
            }
        });
    });
</script>