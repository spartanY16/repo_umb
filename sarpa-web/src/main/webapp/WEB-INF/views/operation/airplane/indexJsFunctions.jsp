<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true,
            pageSettings: {pageSize: 50},
            allowSearching: true,
            toolbarSettings: {showToolbar: true, toolbarItems: ["search"]},
            columns: [
                {field: "name", headerText: "<spring:message code="operation.airplane.name.label" />", width: 105},
                {field: "airplaneType", headerText: "<spring:message code="operation.airplane.airplaneType.label" />", width: 105},
                {field: "passengersNumber", headerText: "<spring:message code="operation.airplane.passengersNumber.label" />", width: 105},
                {field: "stretchersNumber", headerText: "<spring:message code="operation.airplane.stretchersNumber.label" />", width: 105},
                {field: "businessUnit", headerText: "<spring:message code="operation.airplane.businessUnit.label" />", width: 105},
                {field: "loadingCapacity", headerText: "<spring:message code="operation.airplane.loadingCapacity.label" />", width: 105},
                {field: "grossWeight", headerText: "<spring:message code="operation.airplane.grossWeight.label" />", width: 105},
                {field: "actions", headerText: "<spring:message code="common.table.actions" />", width: 85, textAlign: ej.TextAlign.Right}
            ]
        });

    });
</script>