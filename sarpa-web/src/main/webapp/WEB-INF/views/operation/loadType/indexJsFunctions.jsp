<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true, 
            pageSettings: { pageSize: 50 },
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },
            columns: [
                { field: "name", headerText: '<spring:message code="operation.loadType.name.label" />', width: 105 },
                { field: "description", headerText: '<spring:message code="operation.loadType.description.label" />', width: 105 },
                { field: "requirements", headerText: '<spring:message code="operation.loadType.requirements.label" />', width: 105 },
                { field: "actions", headerText: '<spring:message code="common.table.actions" />', width: 85, textAlign: ej.TextAlign.Right }
            ]
        });
        
    });
</script>