<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true, 
            pageSettings: { pageSize: 50 },
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },
            columns: [
                        { field: "iataCode", headerText: "<spring:message code="operation.airport.iataCode.label" />", width: 105 },
                        { field: "icaoCode", headerText: "<spring:message code="operation.airport.icaoCode.label" />", width: 105 },
                        { field: "city", headerText: "<spring:message code="operation.airport.city.label" />", width: 105 },
                        { field: "name", headerText: "<spring:message code="operation.airport.name.label" />", width: 105 },
                        { field: "tzTimezone", headerText: "<spring:message code="operation.airport.tzTimezone.label" />", width: 105 },
                        { field: "flgBase", headerText: "<spring:message code="operation.airport.flgBase.label" />", width: 105 },
                        { field: "flgDestination", headerText: "<spring:message code="operation.airport.flgDestination.label" />", width: 105 },
                        { field: "owner", headerText: "<spring:message code="operation.airport.owner.label" />", width: 105 },
                        { field: "operator", headerText: "<spring:message code="operation.airport.operator.label" />", width: 105 },
                        { field: "actions", headerText: "<spring:message code="common.table.actions" />", width: 85, textAlign: ej.TextAlign.Right }
            ]
        });
        
    });
</script>