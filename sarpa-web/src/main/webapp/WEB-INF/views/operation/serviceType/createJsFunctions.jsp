<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#createFrm").validate({
            rules: {
                name: {
                    required: true
                },
                description: {
                    required: true
                }, 
                requiresPathConfiguration {
                    required: true
                }
            },
            messages: {
                name: {
                    required: '<spring:message code="operation.serviceType.name.errors.required" />'
                },
                description: {
                    required: '<spring:message code="operation.serviceType.description.errors.required" />'
                },
                requiresPathConfiguration {
                    required: '<spring:message code="operation.serviceType.requiresPathConfiguration.errors.required" />'
                }
            }
        });
    });
</script>