<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#Grid").ejGrid({
            dataSource: ej.DataManager($("#indexTbl")),
            allowPaging: true,
            allowSorting: true,
            isResponsive: true, 
            pageSettings: { pageSize: 50 },
            allowSearching : true,
            toolbarSettings : { showToolbar : true, toolbarItems : ["search"] },
            columns: [
                { field: "name", headerText: "<spring:message code="operation.airplaneType.name.label" />", width: 105 },
                { field: "description", headerText: "<spring:message code="operation.airplaneType.description.label" />", width: 105 },
                { field: "detail", headerText: "<spring:message code="operation.airplaneType.detail.label" />", width: 105 },
                { field: "autonomy", headerText: "<spring:message code="operation.airplaneType.autonomy.label" />", width: 105 },
                { field: "maxDistance", headerText: "<spring:message code="operation.airplaneType.maxDistance.label" />", width: 105 },
                { field: "currency", headerText: "<spring:message code="operation.airplaneType.currency.label" />", width: 105 },
                { field: "actions", headerText: "<spring:message code="common.table.actions" />", width: 85, textAlign: ej.TextAlign.Right }
            ]
        });
        
    });
</script>