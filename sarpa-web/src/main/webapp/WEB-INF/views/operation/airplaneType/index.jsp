<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><spring:message code="index.title" /></title>

        <link href="assets/css/web/gradient-azure/ej.web.all.min.css" rel="stylesheet" />
        <link href="assets/css/web/default.css" rel="stylesheet" />

        <%@ include file="/WEB-INF/views/common/javaScripts.jsp" %>
        <%@ include file="/WEB-INF/views/common/tableJavaScripts.jsp" %>
    </head>
    <body>
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0"><spring:message code='menu.operation.airplaneType' /></h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><spring:message code='common.index.list' /></a></li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="row match-height">
            <div class="col-md-12">
                <div class="card" style="zoom: 1;">
                    <div class="card-header">
                        <h4 class="card-title"><spring:message code="operation.airplaneType.index.title" /></h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <!--div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div-->
                        <br/>
                        <a id="createLnk" href="airplaneType-create.htm" onclick="formCreateHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                            <spring:message code="operation.airplaneType.create.title" />
                        </a>
                    </div>
                    <div class="card-body collapse in" aria-expanded="true" style="">
                        <div class="card-block">
                            <br/>
                            <h4 class="form-section"><i class="ft-check-circle"></i> <spring:message code='common.index.results' /></h4>
                            <div id="Grid"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <table id="indexTbl">
            <colgroup>
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <thead>
                <tr>
                    <th>name</th>
                    <th>description</th>
                    <th>detail</th>
                    <th>autonomy</th>
                    <th>maxDistance</th>
                    <th>currency</th>
                    <th>actions</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${indexList}" var="item">
                    <tr>
                        <td>${item.name}</td>
                        <td>${item.description}</td>
                        <td>${item.detail}</td>
                        <td>${item.autonomy}</td>
                        <td>${item.maxDistance}</td>
                        <td>${item.currency.name}</td>
                        <td>
                            <a id="updateLnk" name="updateLnk-${item.name}" href="airplaneType-edit.htm?id=${item.id}" onclick="editHandler(event, this);" ><spring:message code="common.form.update" /></a>
                            <a id="deleteLnk" name="deleteLnk-${item.name}" href="airplaneType-delete.htm?id=${item.id}" onclick="deleteHandler(event, this,'airplaneType-index.htm');" ><spring:message code="common.form.delete" /></a>
                        </td>                    
                    </tr>
                </c:forEach>

            </tbody>
        </table>
        <%@ include file="/WEB-INF/views/operation/airplaneType/indexJsFunctions.jsp" %>    
    </body>
</html>    
