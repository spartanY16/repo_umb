<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#editFrm").validate({
            rules: {
                base : {
                    required: true
                },
                destination : {
                    required: true
                },
                airplaneType : {
                    required: true
                },
                time : {
                    required: true
                },
                flgInternational : {
                    required: true
                }
            },
            messages: {
                base : {
                    required: '<spring:message code="operation.journey.base.errors.required" />'
                },
                destination : {
                    required: '<spring:message code="operation.journey.destination.errors.required" />'
                },
                airplaneType : {
                    required: '<spring:message code="operation.journey.airplaneType.errors.required" />'
                },
                time : {
                    required: '<spring:message code="operation.journey.time.errors.required" />'
                },
                flgInternational : {
                    required: '<spring:message code="operation.journey.flgInternational.errors.required" />'
                }            
            }
        });
    });
</script>