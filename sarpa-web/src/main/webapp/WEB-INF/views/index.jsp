<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html lang="es" data-textdirection="ltr" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="SARPA Admin">
        <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="SARPA">
        <title>SARPA</title>
        <link rel="apple-touch-icon" href="assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/ico/favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="assets/fonts/feather/style.min.css">               <!-- Menu superior -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/font-awesome/css/font-awesome.min.css"><!-- Iconos     -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/flag-icon-css/css/flag-icon.min.css">  <!-- Iconos     -->
        <link rel="stylesheet" type="text/css" href="assets/css/pace.css">            <!-- Iconos     -->
        <link rel="stylesheet" type="text/css" href="assets/vendors/css/weather-icons/climacons.min.css"><!-- Iconos     -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/meteocons/style.css">                  <!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="assets/css/app.css">                                <!-- Menu Lateral -->

        <link rel="stylesheet" type="text/css" href="assets/css/core/menu/menu-types/vertical-menu.css">
        <link rel="stylesheet" type="text/css" href="assets/css/core/menu/menu-types/vertical-overlay-menu.css">
        <link rel="stylesheet" type="text/css" href="assets/fonts/simple-line-icons/style.css">            <!-- Iconos     -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/login-register.css">
        <!-- END Custom CSS-->
    </head>
    <body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <div class="app-content content container-fluid">
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <section class="flexbox-container">
                        <%@ include file="/WEB-INF/views/login/login.jsp" %>
                    </section>
                </div>
               <span> V 1.4</span>
            </div>
        </div>

        <script src="assets/vendors/js/vendors.min.js" type="text/javascript"></script> 
        <script src="assets/js/core/app-menu.js" type="text/javascript"></script>
        <script src="assets/js/core/app.js" type="text/javascript"></script>

    </body>
</html>