<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    </head>
    <body>
        <h1><spring:message code="common.header.text" /></h1>
        <%@ include file="/WEB-INF/views/common/languageSelector.jsp" %>
        <div class="cols-sample-area">
            <a href="index.htm"><spring:message code="common.link.gohome" /></a>
        </div>
    </body>
</html>
