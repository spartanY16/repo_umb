<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    function showFlashMessage(resultObj) {

        var introMessage ="";
        if ("success" === resultObj.code){
            introMessage ='<spring:message code="common.form.result.success" />';
        }
        if ("danger" === resultObj.code){
            introMessage ='<spring:message code="common.form.result.error" />';
        }
        if ("warning"=== resultObj.code){
            introMessage ='<spring:message code="common.form.result.warning" />';
        }

        var flashMessage = '<div id="flashMessage_' + resultObj.code + '" class="alert alert-' + resultObj.code + ' alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                '<strong> '+introMessage+' </strong> ' + resultObj.message +
                '</div>';
        $('#flashMessage').html(flashMessage);
    }

    function editHandler(event, object) {
        event.preventDefault();
        $("#divMainContent").load($(object).attr("href"));
    }

    function deleteHandler(event, object, redirectUrl) {
        event.preventDefault();
        var r = confirm('<spring:message code="common.form.delete.confirm" />');
        if (r === true) {
            $.get($(object).attr("href")).done(function (resultData) {

            }).fail(function (resultData) {


            }).always(function (resultData) {

                var resultObj = jQuery.parseJSON(resultData);

                $("#divMainContent").load(redirectUrl);
                showFlashMessage(resultObj);
            });
        }
    }

    function formSubmitHandler(event, object) {
        event.preventDefault();
        var form = object.closest("form");

        if ($(form).valid()) {
            var formData = $(form).serializeArray();

            $.post($(form).attr('action'), formData).done(function (resultData) {


            }).fail(function (resultData) {


            }).always(function (resultData) {
                var resultObj = jQuery.parseJSON(resultData);
                $("#divMainContent").load($(object).attr("href"));
                showFlashMessage(resultObj);
            });

        }
    }


    function formCreateHandler(event, object) {
        event.preventDefault();
        $("#divMainContent").load($(object).attr("href"));
    }

    function breadcrumbHandler(event, object) {
        event.preventDefault();
        $("#divMainContent").load($(object).attr("href"));
    }

    function formCancelHandler(event, object) {
        event.preventDefault();
        var r = confirm('<spring:message code="common.form.cancel.confirm" />');
        if (r === true) {
            $("#divMainContent").load($(object).attr("href"));
        }

    }
    jQuery(function ($) {
        $("#updateLnk").on("click", {}, editHandler);

    });
</script>
