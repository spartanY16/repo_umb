<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="cols-sample-area">
    <spring:message code="common.language.selector" /> 
    <a href="?lang=es"><spring:message code="common.language.selector.es" /></a>|
    <a href="?lang=en"> <spring:message code="common.language.selector.en" /> </a>
</div>
<spring:message code="common.language.current" /> ${pageContext.response.locale}
