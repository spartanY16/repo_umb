<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><spring:message code="index.title" /></title>

        <link href="assets/css/web/gradient-azure/ej.web.all.min.css" rel="stylesheet" />
        <link href="assets/css/web/default.css" rel="stylesheet" />

        <%@ include file="/WEB-INF/views/common/javaScripts.jsp" %>
    </head>
    <body>
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0"><spring:message code='menu.parametric.currency' /></h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a onclick="breadcrumbHandler(event, this);" href="currency-index.htm"><spring:message code='common.index.list' /></a></li>
                            <li class="breadcrumb-item"><a href="#"><spring:message code='common.index.edit' /></a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="row match-height">
            <div class="col-md-12">
                <div class="card" style="zoom: 1;">
                    <div class="card-header">
                        <h4 class="card-title"><i class="ft-file"></i> <spring:message code="parametric.currency.edit.title" /></h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in" aria-expanded="true" style="">
                        <div class="card-block">

                            <div class="card-text">
                                <p class="card-text"><spring:message code="common.form.edit" /></p>
                            </div>

                            <form:form class="form" method="post" action="currency-update.htm" modelAttribute="currency" id="editFrm">
                                <form:hidden id="idHdn" path="id"/>
                                <div class="form-body">
                                    <h4 class="form-section"><i class="ft-info"></i><spring:message code="common.index.info" /></h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="parametric.currency.code.label" /></label>
                                                <form:input class="form-control"  id="codeTxt" path="code"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="projectinput2"  ><spring:message code="parametric.currency.name.label" /></label>
                                                <form:input class="form-control"  id="nameTxt" path="name"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="parametric.currency.details.label" /></label>
                                                <form:input class="form-control"  id="detailsTxt" path="details"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput2"  ><spring:message code="parametric.currency.convFactorCopExt.label" /></label>
                                                <form:input class="form-control"  id="convFactorCopExtTxt" path="convFactorCopExt"/>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1"><spring:message code="parametric.currency.convFactorExtCop.label" /></label>
                                                <form:input class="form-control"  id="convFactorExtCopTxt" path="convFactorExtCop"/>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-actions">
                                    <a onclick="formCancelHandler(event, this);" href="currency-index.htm" class="btn btn-outline-warning mr-1">
                                        <i class="ft-x"></i> <spring:message code="common.form.cancel" />
                                    </a>
                                   <a href="currency-index.htm" id="submitBtn" onclick="formSubmitHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                                        <i class="ft-check"></i> <spring:message code="common.form.save" />
                                    </a>
                                </div>
                            </form:form> 
                        </div>
                    </div>
                </div>
            </div>

        </div>   
        <%@ include file="/WEB-INF/views/parametric/currency/editJsFunctions.jsp" %>    
    </body>
</html>    
