<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#editFrm").validate({
            rules: {
                name: {
                    required: true
                },

                description: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: '<spring:message code="people.contactType.name.errors.required" />'
                },
                description: {
                    required: '<spring:message code="people.contactType.description.errors.required" />,

                }
            }
        });
    });
</script>