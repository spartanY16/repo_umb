<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#submitBtn").ejButton({
            height: 30,
            width: 130,
            imagePosition: "imageleft",
            contentType: "textandimage",
            showRoundedCorner: true,
            prefixIcon: "e-icon e-handup"
        });

        $("#cancelBtn").ejButton({
            height: 30,
            width: 130,
            imagePosition: "imageleft",
            contentType: "textandimage",
            showRoundedCorner: true,
            prefixIcon: "e-icon e-handup"
        });

        $("#createFrm").validate({
            rules: {
                name: {
                    required: true
                },

                description: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: '<spring:message code="people.contactType.name.errors.required" />'
                },
                description: {
                    required: '<spring:message code="people.contactType.description.errors.required" />,

                }
            }
        });
    });
</script>