<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {

        $("#createFrm").validate({
            rules: {
                name: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: '<spring:message code="parametric.employeePosition.name.errors.required" />'
                },
                description: {
                    required: '<spring:message code="parametric.employeePosition.description.errors.required" />'
                }
            }
        });
    });
</script>