<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><spring:message code="index.title" /></title>

        <link href="assets/css/web/gradient-azure/ej.web.all.min.css" rel="stylesheet" />
        <link href="assets/css/web/default.css" rel="stylesheet" />
        
        <%@ include file="/WEB-INF/views/common/stylesCSS.jsp" %>
        <%@ include file="/WEB-INF/views/common/javaScripts.jsp" %>
        <%@ include file="/WEB-INF/views/common/tableJavaScripts.jsp" %>
    </head>
    <body>
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-2">
                <h3 class="content-header-title mb-0"><spring:message code='menu.parametric.airportrate' /></h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><spring:message code='common.index.list' /></a></li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="row match-height">
            <div class="col-md-12">
                <div class="card" style="zoom: 1;">
                    <div class="card-header">
                        <h4 class="card-title"><spring:message code="parametric.airportrate.index.title" /></h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <!--div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div-->
                        <br/>
                        <a id="createLnk" href="airportrate-create.htm" onclick="formCreateHandler(event, this);"  type="submit" class="btn btn-outline-primary">
                            <spring:message code="parametric.airportrate.create.title" />
                        </a>
                    </div>
                    <div class="card-body collapse in" aria-expanded="true" style="">
                        <div class="card-block">
                            <br/>
                            <h4 class="form-section"><i class="ft-check-circle"></i> <spring:message code='common.index.results' /></h4>
                            <div id="Grid"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <table id="indexTbl">
            <colgroup>
                <col />
                <col />
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <thead>
                <tr>
                    <th>city</th>
                    <th>airport</th>
                    <th>iataCode</th>
                    <th>year</th>
                    <th>currencyType</th>
                    <th>value</th>
                    <th>actions</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${indexList}" var="item">
                    <tr>
                        <td>${item.airport.city}</td>
                        <td>${item.airport.name}</td>
                        <td>${item.airport.iataCode}</td>
                        <td>${item.year}</td>
                        <td>${item.currency.name}</td>
                        <td>${item.value}</td>
                        <td>
                            <a id="updateLnk-${item.id}" href="airportrate-edit.htm?id=${item.id}" onclick="editHandler(event, this);"  ><spring:message code="common.form.update" /></a>
                            <a id="deleteLnk-${item.id}" name="deleteLnk--${item.id}" href="airportrate-delete.htm?id=${item.id}" onclick="deleteHandler(event, this,'airportrate-index.htm');" ><spring:message code="common.form.delete" /></a>
                        </td>                    
                    </tr>
                </c:forEach>
                
            </tbody>
        </table>
        <%@ include file="/WEB-INF/views/parametric/airportRate/indexJsFunctions.jsp" %>    
    </body>
</html>    
