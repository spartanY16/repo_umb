<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">

    jQuery(function ($) {
        
        $("#editFrm").validate({
            rules: {
                code : {
                    required: true
                },
                name : {
                    required: true
                },
                convFactorCopExt : {
                    required: true
                },
                convFactorExtCop : {    
                    required: true
                }               
            },
            messages: {
                code : {
                    required: '<spring:message code="parametric.currency.code.errors.required" />'
                },
                name : {
                    required: '<spring:message code="parametric.currency.name.errors.required" />'
                },
                convFactorCopExt : {
                    required: '<spring:message code="parametric.currency.convFactorCopExt.errors.required" />'
                }   ,
                convFactorExtCop : {
                    required: '<spring:message code="parametric.currency.convFactorExtCop.errors.required" />'
                }
            }
        });
</script>