/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Document: eventsAirport.java
 * Created on: Thu Oct 12 11:03:34 COT 2017
 * Project: sarpa
 * Objective: To manage events airport.
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
$(function () {

    var airportsList = "";
    $.ajax({
        type: "GET",
        url: "/sarpa/airport-FilterByNameOrIATACodeOrCity.htm?name=",
        dataType: "json",
        async: false,
        success: function (response) {
            airportsList = response.data;
        },
        error: function (jqXHR, status, error) {
            alert('Invocacion error consulta aeropuertos autocompletar -> Consulte con el Administrador del Sistema. ' +
                    'status: ' + status + 'error: ' + error +
                    'jqXHR' + jqXHR);
        }
    });

    $('#airportTxt').ejAutocomplete({
        dataSource: airportsList,
        fields: {key: "id", text: "name"},
        select: function (argument) {
            $("#airportHdn").val(argument.item.id);
        },
        highlightSearch: true,
        caseSensitiveSearch: false,
        filterType: 'contains',
        watermarkText: "Buscar Aeropuerto",
        emptyResultText: 'No se encuentra aeropuertos',
        width: "100%",
        showPopupButton: true,
        multiColumnSettings: {
            stringFormat: "{2},{1}({0})",
            enable: true,
            showHeader: true,
            columns: [{
                    field: "iataCode",
                    headerText: "IATA",
                },
                {
                    field: "name",
                    headerText: "Nombre"
                },
                {
                    field: "city",
                    headerText: "Ciudad"
                }
            ]}
    });

});
