    /*
     * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
     *
 * Document: eventsCustomerRequest.js
 * Created on: Thu Sep 19 15:14:39 COT 2017
 * Project: sarpa
 * Objective: To manage events airports rates.
 * @author <a href="mailto:joses@gmlsoftware.com">Jose Danilo Sanchez Torres</a>
 * Copyright 2017 GML Software, Inc. All Rights Reserved.
 */
$(function () {

    var countJourneysButtons = 0;
    var timeFlightTotal = 0.0;
    var customerRequestIdHdn = $("#idHdn").val();
    var idAirplaneType = $("#airplaneTypeId").val();
    var isValidCustomerRequestIdHdn = customerRequestIdHdn !== undefined
            && customerRequestIdHdn.length > 0;
    var isValidIdAirplaneType = idAirplaneType !== undefined
            && idAirplaneType.length > 0;

	
    $("#idOpenAirportRates").click(function () {
        //$("#dialogSearchCompany").load("../sarpa/company-create-crc.htm");
         alert("Prueba JDST Dialogo prueba");
		 //$("#Dialog3").ejDialog("open");
    });

    /**
     * Update information about customer Request with Journeys.
     * */
    if (isValidCustomerRequestIdHdn && isValidIdAirplaneType) {
        
        var listOptionsJourney = '';
        
        $.ajax({
            type: "GET",
            url: "/sarpa/journey-findByAirlineType.htm?idAirplaneType=" + idAirplaneType,
            dataType: "json",
            success: function (response) {
                var tamanio = response.data.length;
                for (var i = 0; i < tamanio; i++) {
                    var itemJourney = response.data[i].base.name + "/" + response.data[i].destination.name;
                    //timeFlightTotal += response.data[i].time;
                    listOptionsJourney += "<option value='" + response.data[i].id + "' data-id='" + response.data[i].time + "'>"
                            + itemJourney + "</option><br>";
                }
                //alert("timeFlightTotal es " + timeFlightTotal);
                $.ajax({
                    type: "GET",
                    url: "/sarpa/customerRequest-findById.htm?id=" + customerRequestIdHdn,
                    dataType: "json",
                    success: function (responseCR) {
                        var timeFlightTotal = 0;
                        timeFlightTotal += responseCR.data.time;
                        var tamanioJCR = responseCR.data.journeys.length;
                        var listJourneys = responseCR.data.journeys;
                        for (var j = 0; j < tamanioJCR; j++) {
                            countJourneysButtons++;
                            var journeyCR = listJourneys[j];
                            var listJourney = "<select id='journeyId" + countJourneysButtons + "'"
                                    + "name='journeys.id' class='form-control valid classjourneyId' aria-invalid='false'>";
                            listJourney += listOptionsJourney;
                            listJourney += "</select>";
                            var isEven = countJourneysButtons % 2 === 0;
                            var row = "";
                            if (isEven) {
                                row = createItemJourney("", listJourney);
                                row += "</div>";
                            } else {
                                row += "<div class='row'>";
                                row = createItemJourney("", listJourney);
                            }
                            var timeFlightTotalFormat = getFormatHour(timeFlightTotal);
                            $("#flighttimeTxt").val(timeFlightTotalFormat);
                            //var a = '<script type="text/javascript">function seleccionarValor() { ';
                            //a += '$("#flighttimeTxt").val(document.getElementsByClassName("form-control valid classjourneyId")[0].value + $("#flighttimeTxt").val());';
                            //a += 'alert("Valor seleccionado dinamicamente de danilo es ";}</script>';
                            //$('#journeys').append(a);
                            $('#journeys').append(row);
                            $('#journeyId' + countJourneysButtons).val(journeyCR.id).attr('selected', 'selected');
                        }
                    },
                    error: function (jqXHR, status, error) {
                        alert('Invocacion error consulta id customer request ' + customerRequestIdHdn
                                + ' autocompletar -> Consulte con el Administrador del Sistema. ' +
                                'status: ' + status + 'error: ' + error +
                                'jqXHR' + jqXHR);
                    }
                });
            },
            error: function (jqXHR, status, error) {
                alert('Invocacion error consulta Trayectos -> Consulte con el Administrador del Sistema. ' +
                        'status: ' + status + 'error: ' + error +
                        'jqXHR' + jqXHR);
            }
        });
    }
    
    $('select').find('option').click(function () {
        var timeValue = $(this).attr('data');
        if(timeValue !== undefined) {
            //alert("Valor tiempo trayecto seleccionado es " + timeValue);
            timeFlightTotal += timeValue;
        }
    });
    
    $("#idOpenCreateCompany").click(function () {
        $("#dialogSearchCompany").load("../sarpa/company-create-crc.htm");
        $("#dialogSearchCompany").show();
    });

    $("#cancelCreateCompanycrc").click(function () {
        $("#dialogSearchCompany").hide();
        $("#dialogSearchCompany").empty();
    });

    $("#createCompanycrc").click(function () {
        var dataCreate = $("#createFrmCompany").serializeArray();
        if (!validateFields()) {
            alert("Se debe diligenciar al menos dos campos para la nueva empresa ");
        } else {
            $.ajax({
                url: "/sarpa/company-save.htm",
                data: dataCreate,
                type: 'POST',
                dataType: "json",
            })
            .done(function (data, textStatus, jqXHR) {
                console.log("Se ejecuta la consulta done" + data);
                $.ajax({
                    url: "/sarpa/company-findAll.htm",
                    data: {},
                    type: 'POST',
                    dataType: "json",
                    success: function (response) {
                        $("#dialogSearchCompany").hide();
                        $("#dialogSearchCompany").empty();
                        $('#companyId').empty();
                        var tamanio = response.data.length;
                        for (var i = 0; i < tamanio; i++) {
                            console.log("item es " + response.data[i].id + " " + response.data[i].name + "\n");
                            $("#companyId").append("<option value="
                                    + response.data[i].id + ">" + response.data[i].name + "</option>");
                        }
                    },
                    error: function (jqXHR, status, error) {
                        alert('Invocacion error consulta empresas -> Consulte con el Administrador del Sistema. ' +
                                'status: ' + status + 'error: ' + error +
                                'jqXHR' + jqXHR);
                    }
                });
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert('Invocacion error guardar empresa -> Consulte con el Administrador del Sistema. ' +
                        'textStatus: ' + textStatus + 'errorThrown: ' + errorThrown +
                        'jqXHR' + jqXHR);
            });
        }
    });

    $('#idNameCompany').keypress(function () {
        //Obtenemos el value del input
        var idNameCompany = $(this).val();
        $.ajax({
            type: "GET",
            url: "/sarpa/company-findFilter.htm?name=" + idNameCompany,
            dataType: "json",
            success: function (response) {
                var listCompanies = "";
                var tamanio = response.data.length;
                for (var i = 0; i < tamanio; i++) {
                    listCompanies += "<a class = 'eventCompany' data='" + response.data[i].name + "' id=" + response.data[i].id + ">" + response.data[i].name + "</a><br>";
                }
                $('#companies').fadeIn(1000).html(listCompanies);
                $('.eventCompany').on('click', function () {
                    var id = $(this).attr('id');
                    $("#companyHdn").val(id);
                    $('#idNameCompany').val($('#' + id).attr('data'));
                    $('#companies').fadeOut(1000);
                });
            },
            error: function (jqXHR, status, error) {
                alert('Invocacion error consulta empresas autocompletar -> Consulte con el Administrador del Sistema. ' +
                        'status: ' + status + 'error: ' + error +
                        'jqXHR' + jqXHR);
            }
        });
    });

    /***
     * Write on the originText field.
     */
    $('#originTxt').keyup(function () {
        var nameAirport = $(this).val();
        $('#origins').hide();
        $.ajax({
            type: "GET",
            url: "/sarpa/airport-FilterByNameOrIATACodeOrCity.htm?name=" + nameAirport,
            dataType: "json",
            success: function (response) {
                var listAirports = "";
                var tamanio = response.data.length;
                for (var i = 0; i < tamanio; i++) {
                    var labelAirport = "" + response.data[i].city + ", "
                            + response.data[i].name + "(" + response.data[i].iataCode + ")";

                    listAirports += "<a class = 'eventOrigin' data='" + response.data[i].name + "' id=" + response.data[i].id + ">" + labelAirport + "</a><br>";
                }
                //alert("Aeropuertos " + listAirports);
                $('#origins').fadeIn(1000).html(listAirports);
                //Al hacer click en alguna de las empresas
                $('.eventOrigin').on('click', function () {
                    //Obtenemos la id unica de la sugerencia pulsada
                    var id = $(this).attr('id');
                    var value = $(this).attr('data');
                    $("#originHdn").val(id);
                    $('#originTxt').val(value);
                    $('#origins').fadeOut(1000);
                    $('#origins').empty();
                });
            },
            error: function (jqXHR, status, error) {
                alert('Invocacion error consulta aeropuertos origen autocompletar -> Consulte con el Administrador del Sistema. ' +
                        'status: ' + status + 'error: ' + error +
                        'jqXHR' + jqXHR);
            }
        });
    });

    /***
     * Write on the destinationTxt field.
     */
    $('#destinationTxt').keyup(function () {
        //Obtenemos el value del input
        var nameAirport = $(this).val();
        $.ajax({
            type: "GET",
            url: "/sarpa/airport-FilterByNameOrIATACodeOrCity.htm?name=" + nameAirport,
            dataType: "json",
            success: function (response) {
                var listAirports = "";
                var tamanio = response.data.length;
                for (var i = 0; i < tamanio; i++) {
                    var labelAirport = "" + response.data[i].city + ", "
                            + response.data[i].name + "(" + response.data[i].iataCode + ")";

                    listAirports += "<a class = 'eventDestination' data='" + response.data[i].name + "' id=" + response.data[i].id + ">" + labelAirport + "</a><br>";
                }
                $('#destinations').fadeIn(1000).html(listAirports);
                $('.eventDestination').on('click', function () {
                    //Obtenemos la id unica de la sugerencia pulsada
                    var id = $(this).attr('id');
                    var value = $(this).attr('data');
                    $("#destinationHdn").val(id);
                    $('#destinationTxt').val(value);
                    $('#destinations').fadeOut(1000);
                    $('#destinations').empty();
                });
            },
            error: function (jqXHR, status, error) {
                alert('Invocacion error consulta aeropuertos destino autocompletar -> Consulte con el Administrador del Sistema. ' +
                        'status: ' + status + 'error: ' + error +
                        'jqXHR' + jqXHR);
            }
        });
    });
    
    /**
     * 
     * Add a new Journey in page Wizard 3.
     */
    $("#addJourney").click(function () {
        var idAirplaneType = $("#airplaneTypeId").val();
        $.ajax({
            type: "GET",
            url: "/sarpa/journey-findByAirlineType.htm?idAirplaneType=" + idAirplaneType,
            dataType: "json",
            success: function (response) {
                countJourneysButtons++;
                var listJorney = "<select id='journeyId" + countJourneysButtons + "' "
                        + "name='journeys.id' class='form-control valid' aria-invalid='false'>";
                var tamanio = response.data.length;
                for (var i = 0; i < tamanio; i++) {
                    var itemJourney = response.data[i].base.name + "/" + response.data[i].destination.name;
                    timeFlightTotal += response.data[i].time;
                    listJorney += "<option value='" + response.data[i].id + "'>"
                            + itemJourney + "</option><br>";
                }
                listJorney += "</select>";
                var isEven = countJourneysButtons % 2 === 0;
                var row = "";
                if (isEven) {
                    row = createItemJourney("", listJorney);
                    row += "</div>";
                } else {
                    row += "<div class='row'>";
                    row = createItemJourney("", listJorney);
                }
                var timeFlightTotalFormat = getFormatHour(timeFlightTotal);
                $("#flighttimeTxt").val(timeFlightTotalFormat);
                //alert("Aeropuertos " + listAirports);
                $('#journeys').append(row);
            },
            error: function (jqXHR, status, error) {
                alert('Invocacion error consulta aeropuertos destino autocompletar -> Consulte con el Administrador del Sistema. ' +
                        'status: ' + status + 'error: ' + error +
                        'jqXHR' + jqXHR);
            }
        });
    });
    
    function onDialogClose(args) {
        $("#btnOpen").show();
    }
    function onOpen() {
        $("#btnOpen").hide();
        $("#dialogIcon").ejDialog("open");
    }

    /************************************ privates functions ****************************/
    
    /***
     * Validate Field the first page.
     * @returns {Boolean} Get a data type boolean.
     */
    function validateFields() {

        var isValidateFields = true;

        var name = $("#nameTxt").val();
        var phone = $("#phoneTxt").val();
        var email = $("#emailTxt").val();
        var count = 0;

        var validateFieldName = name != null && name.length > 0;
        if (validateFieldName) {
            count++;
        }
        var validateFieldPhone = phone != null && phone.length > 0;
        if (validateFieldPhone) {
            count++;
        }
        var validateFieldEmail = email != null && email.length > 0;
        if (validateFieldEmail) {
            count++;
        }
        console.log("Cantidad de campos diligenciados " + count);
        if (count < 2) {
            isValidateFields = false;
        }
        return isValidateFields;
    }

    /**
     * Create a field with information in a div element.
     * @param {type} title Set a data type String.
     * @param {type} value Set a data type String.
     * @returns {String} Return a data type String.
     */
    function createItemJourney(title, value) {
        var item = "<div class='col-md-6'><div class='form-group'><label for='idRoute'>" +
                title + "</label>";
        item += "" + value;
        item += "</div></div>";
        return item;
    }
    
    /**
     * Convert value from decimal to HH:MM:SS Format.
     * @param {type} value set value a data type double.
     * @returns {String} return a data type String
     */
    function getFormatHour(value) {
        
        value = Math.round(value);
        var hours = Math.floor(value / 60);
        var minutes = value % 60;
        var hoursFormat = (hours < 10 ? "0" + hours : "" + hours);
        var minutesFormat = (minutes < 10 ? "0" + minutes : "" + minutes);
        var secondsFormat = "00";
        var formatHour = hoursFormat + ":" + minutesFormat + ":" + secondsFormat;
        return formatHour;
    }
    /**
     * 
     * @returns {undefined}
     */
    
});


