﻿delete from people.employee;

INSERT INTO people.employee(
            id, version, 
            person_id,airplane_type_id,
            type,position, birth_day, operative_code, contract_type,
            salary,anual_bonus,admission_date,retirement_date,
            rh,med_certificates_expiration,license_number,
            passport_number,passport_expiration,
            american_visa_number, american_visa_expiration, status,
            date_created, date_last_updated, 
            user_created, user_last_updated
            )
    VALUES 
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '7545403'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'PILOTO',to_timestamp('12/24/1963', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     7537500, 376875, to_timestamp('42042', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',to_timestamp('06/20/2017', 'MM/DD/YYYY')::timestamp, 'PTL2059', 
     'AO370901',to_timestamp('02/13/2023', 'MM/DD/YYYY')::timestamp, '', to_timestamp('02/20/2023', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '80412728'), (select id from operation.airplane_type  where name = 'LJ-35'), 
     'AEREA', 'PILOTO',to_timestamp('08/11/1967', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     7537500, 376875, to_timestamp('42075', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',to_timestamp('42988', 'MM/DD/YYYY')::timestamp, 'PTL2188', 
     'AR793160',to_timestamp('45727', 'MM/DD/YYYY')::timestamp, '', to_timestamp('01/31/2023', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '16610004'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'PILOTO',to_timestamp('02/06/1958', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     7537500, 523103, to_timestamp('40613', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',to_timestamp('07/26/2017', 'MM/DD/YYYY')::timestamp, 'PTL2540', 
     'AM756912',to_timestamp('46116', 'MM/DD/YYYY')::timestamp, '', to_timestamp('10/28/2024', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),

 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '16680012'), (select id from operation.airplane_type  where name = 'E-120'), 
     'AEREA', 'PILOTO',to_timestamp('01/13/1962', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     7537500, 376875, to_timestamp('41771', 'MM/DD/YYYY')::timestamp,NULL, 
     '',to_timestamp('11/13/2017', 'MM/DD/YYYY')::timestamp, 'PTL2654', 
     'AM616530',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('12/29/2025', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '19354460'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'PILOTO',to_timestamp('04/21/1959', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     7537500, 376875, to_timestamp('02/26/2015', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',to_timestamp('12/16/2017', 'MM/DD/YYYY')::timestamp, 'PTL2799', 
     'AN426869',to_timestamp('01/13/2027', 'MM/DD/YYYY')::timestamp, '', to_timestamp('01/01/2017', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '80544344'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'PILOTO',to_timestamp('29223', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     7537500, 376875, to_timestamp('40188', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',to_timestamp('12/14/2017', 'MM/DD/YYYY')::timestamp, 'PTL3114', 
     'AR618196',to_timestamp('09/29/2025', 'MM/DD/YYYY')::timestamp, '', to_timestamp('44682', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '19380308'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'PILOTO',to_timestamp('08/22/1959', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     7537500, 523103, to_timestamp('10/15/2004', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',to_timestamp('09/22/2017', 'MM/DD/YYYY')::timestamp, 'PTL1943', 
     'PE092405',to_timestamp('01/22/2024', 'MM/DD/YYYY')::timestamp, 'No', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '14248464'), (select id from operation.airplane_type  where name = 'LJ-35'), 
     'AEREA', 'PILOTO',to_timestamp('01/03/1962', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     7537500, 1376875, to_timestamp('41950', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',to_timestamp('43081', 'MM/DD/YYYY')::timestamp, 'PTL2123', 
     'PE078836',to_timestamp('02/13/2023', 'MM/DD/YYYY')::timestamp, '', to_timestamp('45757', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '16626083'), (select id from operation.airplane_type  where name = 'E-120'), 
     'AEREA', 'PILOTO',to_timestamp('08/07/1958', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     7537500, 0, to_timestamp('10/24/2016', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',to_timestamp('42866', 'MM/DD/YYYY')::timestamp, 'PTL2227', 
     'AS837191',to_timestamp('06/30/2026', 'MM/DD/YYYY')::timestamp, '', to_timestamp('07/17/2026', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '19340841'), (select id from operation.airplane_type  where name = 'E-120'), 
     'AEREA', 'PILOTO',to_timestamp('03/19/1957', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     7537500, 523103, to_timestamp('37990', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',to_timestamp('01/01/2017', 'MM/DD/YYYY')::timestamp, 'PTL1669', 
     'AM871372',to_timestamp('07/26/2026', 'MM/DD/YYYY')::timestamp, '', to_timestamp('44417', 'MM/DD/YYYY')::timestamp,  'SUSPENDIDO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '71786428'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'PILOTO',to_timestamp('27823', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     7537500, 523103, to_timestamp('38360', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',to_timestamp('42743', 'MM/DD/YYYY')::timestamp, 'PTL2586', 
     'AP166744',to_timestamp('11/26/2023', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79436760'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'PILOTO',to_timestamp('02/13/1968', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     7537500, 376875, to_timestamp('07/15/2014', 'MM/DD/YYYY')::timestamp,NULL, 
     '',to_timestamp('09/15/2017', 'MM/DD/YYYY')::timestamp, 'PTL3474', 
     'PE086131',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('45690', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '9657236'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'PILOTO',to_timestamp('04/21/1970', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     7537500, 376875, to_timestamp('41951', 'MM/DD/YYYY')::timestamp,NULL, 
     'B-',to_timestamp('', 'MM/DD/YYYY')::timestamp, 'PTL2670', 
     'PE087928',to_timestamp('45269', 'MM/DD/YYYY')::timestamp, '', to_timestamp('09/17/2023', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '17322579'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'PILOTO',to_timestamp('05/30/1962', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3626000, 181300, to_timestamp('09/25/2016', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',to_timestamp('08/15/2017', 'MM/DD/YYYY')::timestamp, 'PTL2936', 
     'AN325181',to_timestamp('02/14/2026', 'MM/DD/YYYY')::timestamp, 'No', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1020757988'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'COPILOTO',to_timestamp('12/18/1990', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3626000, 181300, to_timestamp('09/25/2016
', 'MM/DD/YYYY')::timestamp,NULL, 
     'B+',to_timestamp('07/30/2017', 'MM/DD/YYYY')::timestamp, 'PCA10007', 
     'AO045561',to_timestamp('44630', 'MM/DD/YYYY')::timestamp, '', to_timestamp('10/29/2022', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79059319'), (select id from operation.airplane_type  where name = 'E-120'), 
     'AEREA', 'COPILOTO',to_timestamp('25823', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3626000, 181300, to_timestamp('08/14/2013', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',to_timestamp('12/19/2017', 'MM/DD/YYYY')::timestamp, 'PCA10534', 
     'AR550919',to_timestamp('09/15/2025', 'MM/DD/YYYY')::timestamp, '', to_timestamp('45728', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '80001685'), (select id from operation.airplane_type  where name = 'E-120'), 
     'AEREA', 'COPILOTO',to_timestamp('28533', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3626000, 181300, to_timestamp('10/27/2014', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',to_timestamp('', 'MM/DD/YYYY')::timestamp, 'PCA9137', 
     'AR311755',to_timestamp('07/24/2025', 'MM/DD/YYYY')::timestamp, '', to_timestamp('09/13/2025', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1037606031'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'COPILOTO',to_timestamp('07/28/1990', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3626000, 181300, to_timestamp('02/17/2015', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',to_timestamp('43171', 'MM/DD/YYYY')::timestamp, 'PCA9515', 
     'AS060646',to_timestamp('12/27/2025', 'MM/DD/YYYY')::timestamp, '', to_timestamp('11/14/2023', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1022373986'), (select id from operation.airplane_type  where name = 'LJ-35'), 
     'AEREA', 'COPILOTO',to_timestamp('33606', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3626000, 181300, to_timestamp('11/23/2016', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',to_timestamp('06/20/2018', 'MM/DD/YYYY')::timestamp, 'PCA11063', 
     'AO172857',to_timestamp('11/27/2022', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '80804049'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'COPILOTO',to_timestamp('10/26/1985', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3626000, 181300, to_timestamp('02/27/2015', 'MM/DD/YYYY')::timestamp,NULL, 
     'B+',to_timestamp('43141', 'MM/DD/YYYY')::timestamp, 'PCA9277', 
     'AQ452246',to_timestamp('1/18/2025', 'MM/DD/YYYY')::timestamp, '', to_timestamp('08/23/2022', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1037607838'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'COPILOTO',to_timestamp('10/24/1990', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3626000, 251644, to_timestamp('41282', 'MM/DD/YYYY')::timestamp,NULL, 
     'AB+',to_timestamp('43139', 'MM/DD/YYYY')::timestamp, 'PCA9589', 
     'AS257469',to_timestamp('46236', 'MM/DD/YYYY')::timestamp, '', to_timestamp('10/22/2022', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1020738380'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'COPILOTO',to_timestamp('12/19/1988', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3626000, 181300, to_timestamp('02/27/2015', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',to_timestamp('12/26/2017', 'MM/DD/YYYY')::timestamp, 'PCA9847', 
     'AP701637',to_timestamp('05/25/2027', 'MM/DD/YYYY')::timestamp, '', to_timestamp('07/22/2024', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1016054336'), (select id from operation.airplane_type  where name = 'JS-32'), 
     'AEREA', 'COPILOTO',to_timestamp('34216', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3626000, 181300, to_timestamp('10/27/2014', 'MM/DD/YYYY')::timestamp,NULL, 
     'B+',to_timestamp('10/16/2017', 'MM/DD/YYYY')::timestamp, 'PCA9961', 
     'AM841919',to_timestamp('44204', 'MM/DD/YYYY')::timestamp, '', to_timestamp('44907', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '93151406'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'INSPECTOR',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3854000, 1541600, to_timestamp('10/15/2004', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '1868', 
     'PENDIENTE',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', null,  'PENDIENTE', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '93154219'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'INSPECTOR',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3000000, 610000, to_timestamp('40067', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '4079', 
     'AT573057',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('46425', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79544166'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'INSPECTOR',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3522000, 1408800, to_timestamp('10/15/2004', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '2696', 
     'AM755337',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('45522', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '80121403'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'INSPECTOR',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3000000, 610000, to_timestamp('39823', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '3058', 
     'AO228431',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('45301', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1022979734'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'TECNICO',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1155000, 700000, to_timestamp('41860', 'MM/DD/YYYY')::timestamp,NULL, 
     'B+',null, '6925', 
     'PENDIENTE',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1044420498'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'TECNICO',to_timestamp('31391', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1100000, 700000, to_timestamp('05/22/2017', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '4149', 
     'PENDIENTE',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1037323243'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'TECNICO',to_timestamp('32793', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1600000, 600000, to_timestamp('42798', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '6329', 
     'PENDIENTE',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1018425532'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'TECNICO',to_timestamp('05/30/1989', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1800000, 0, to_timestamp('42685', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '5429', 
     'PENDIENTE',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '80009552'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'TECNICO',to_timestamp('28977', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     2600000, 400000, to_timestamp('42984', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '298', 
     'AQ512863',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('45698', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1035872466'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'TECNICO',to_timestamp('02/19/1996', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     900000, 0, to_timestamp('03/13/2017', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '7967', 
     'PENDIENTE',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79901999'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'TECNICO',to_timestamp('28585', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1100000, 700000, to_timestamp('42888', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '3066', 
     'PENDIENTE',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1013605900'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'TECNICO',to_timestamp('32516', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1600000, 400000, to_timestamp('42888', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, 'N/A', 
     'PENDIENTE',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1107053135'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'TECNICO',to_timestamp('06/15/1985', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1600000, 600000, to_timestamp('03/27/2017', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '6193', 
     'AN381406',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '19305438'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'TECNICO',to_timestamp('10/27/1951', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1684000, 600000, to_timestamp('42099', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '19364349'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'TECNICO',to_timestamp('12/20/1958', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     2500000, 500000, to_timestamp('42874', 'MM/DD/YYYY')::timestamp,NULL, 
     'AB+',null, '1442', 
     'PENDIENTE',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1022960425'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'TECNICO',to_timestamp('05/29/1990', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1600000, 400000, to_timestamp('06/26/2017', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79846949'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'PINTOR',to_timestamp('07/23/1073', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1260000, 400000, to_timestamp('41951', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1032405711'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'ALMACEN',to_timestamp('01/29/1988', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     2120000, 0, to_timestamp('42402', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '6592', 
     'PENDIENTE',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '39190067'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DESPACHADOR',to_timestamp('08/21/1979', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1260000, 190000, to_timestamp('12/15/2014', 'MM/DD/YYYY')::timestamp,NULL, 
     'B+',null, 'DPA-4430', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '14237423'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DESPACHADOR',to_timestamp('07/27/1961', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1260000, NULL, to_timestamp('01/26/2014', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, 'DPA-1230', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1014253413'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DESPACHADOR',to_timestamp('04/15/1994', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1200000, NULL, to_timestamp('06/26/2017', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1014262092'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DESPACHADOR',to_timestamp('12/29/1994', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1200000, NULL, to_timestamp('43015', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79211168'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'CONDUCTOR',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     934500, 8633, to_timestamp('41186', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79615543'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'CONDUCTOR',to_timestamp('26363', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     890000, NULL, to_timestamp('42713', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '80874953'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'CONDUCTOR',to_timestamp('08/19/1985', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     934500, NULL, to_timestamp('41740', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '21396161'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'SERVICIOS GENERALES',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     942500, 65410, to_timestamp('01/26/1997', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     'N/A',null, 'N/A', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '35514612'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'SERVICIOS GENERALES',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     836000, 49909, to_timestamp('38362', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     'N/A',null, 'N/A', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1074002271'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'SERVICIOS GENERALES',to_timestamp('04/14/1995', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     737717, 0, to_timestamp('01/14/2015', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     'N/A',null, 'N/A', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '16274800'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'SERVICIOS GENERALES',to_timestamp('07/30/1965', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     737717, 0, to_timestamp('09/18/2016', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     'N/A',null, 'N/A', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '16480360'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'SERVICIOS GENERALES',to_timestamp('07/05/1962', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     737717, 0, to_timestamp('42713', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     'N/A',null, 'N/A', null,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '39760058'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DIRECTOR COMERCIAL',to_timestamp('26188', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3000000, 0, to_timestamp('06/16/2014', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '43616295'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DIRECTOR SERVICIOS DE SALUD',to_timestamp('10/26/1977', 'MM/DD/YYYY')::timestamp, 17, 'IND', 
     4576000, 651200, to_timestamp('39094', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '35900', 
     'AR787838',to_timestamp('45727', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '43667482'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DIRECTOR ADMINISTRATIVO',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     4940000, 0, to_timestamp('39451', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '43765223'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DIRECTOR FINANCIERO',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     4951440, 444364, to_timestamp('40603', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '52325367'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DIRECTOR HSEQ',to_timestamp('06/19/1977', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3861925, 1754379, to_timestamp('41194', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '53107192'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DIRECTOR SMS',to_timestamp('06/29/1985', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     2250000, 850000, to_timestamp('42435', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79318122'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DIRECTOR CONTROL CALIDAD',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     5757150, 732140, to_timestamp('08/15/2006', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79533622'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DIRECTOR MANTENIMIENTO',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     4737988, 597240, to_timestamp('01/15/2007', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1017135067'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DIRECTOR RECURSO HUMANO',to_timestamp('31724', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     4858200, 3238200, to_timestamp('39696', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '17149518'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'DIRECTOR SERVICIOS GENERALES E INFRAESTRUCTURA',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     3149946, 2099800, to_timestamp('40918', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '71603815'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'GERENTE AMBULANCIA',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     5631920, 1982598, to_timestamp('37267', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '8716522'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'GERENTE ADMINISTRATIVO',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'SERV', 
     6000000, NULL, to_timestamp('42319', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '11386740'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'MENSAJERO',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     744610, NULL, to_timestamp('39450', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '98698960'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'MENSAJERO',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     950000, NULL, to_timestamp('09/15/2008', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '43975298'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'AUXILIAR ADMINISTRATIVO',to_timestamp('02/16/1982', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     4247000, 2830000, to_timestamp('39696', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '51618826'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'AUXILIAR ADMINISTRATIVO',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     9024281, 1425720, to_timestamp('40185', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1032441100'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'AUXILIAR ADMINISTRATIVO',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1365000, 209021, to_timestamp('40182', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1040754838'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'AUXILIAR ADMINISTRATIVO',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1600000, NULL, to_timestamp('42016', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1128456122'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'AUXILIAR ADMINISTRATIVO',to_timestamp('', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1800000, 1200000, to_timestamp('40544', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1015411800'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'AUXILIAR ADMINISTRATIVO',to_timestamp('32755', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     870000, 250000, to_timestamp('42381', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1010173469'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'PROGRAMACION MATENIMIENTO',to_timestamp('31938', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1200000, NULL, to_timestamp('42767', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1072707090'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'REGENTE FARMACIA',to_timestamp('08/29/1995', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     800000, 370000, to_timestamp('11/23/2016', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1023024507'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'RECEPCIONISTA',to_timestamp('35652', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     889454, 44472, to_timestamp('42195', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1020442913'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'RECEPCIONISTA',to_timestamp('06/21/1991', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     800000, NULL, to_timestamp('01/16/2017', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1037608513'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'AUXILIAR CONTABLE',to_timestamp('11/22/1990', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1260000, NULL, to_timestamp('42462', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '8125801'), (select id from operation.airplane_type  where name = ''), 
     'TIERRA', 'AUXILIAR CONTABLE',to_timestamp('30928', 'MM/DD/YYYY')::timestamp, NULL, 'IND', 
     1100000, NULL, to_timestamp('42857', 'MM/DD/YYYY')::timestamp,NULL, 
     '',null, '', 
     '',to_timestamp('', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '52334593'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'ENFERMERO',to_timestamp('06/17/1976', 'MM/DD/YYYY')::timestamp, 67, 'IND', 
     3055104, 174000, to_timestamp('40392', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '698', 
     'AR844984',to_timestamp('45972', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '52931230'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'ENFERMERO',to_timestamp('30233', 'MM/DD/YYYY')::timestamp, 179, 'IND', 
     2880000, NULL, to_timestamp('42889', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '23108', 
     'AR178929',to_timestamp('06/29/2025', 'MM/DD/YYYY')::timestamp, 'K4061818', to_timestamp('07/29/2025', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1128407346'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'ENFERMERO',to_timestamp('09/17/1987', 'MM/DD/YYYY')::timestamp, 163, 'IND', 
     2937600, NULL, to_timestamp('42228', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '38338', 
     'AS044452',to_timestamp('12/21/2025', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '52310028'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'ENFERMERO',to_timestamp('04/19/1976', 'MM/DD/YYYY')::timestamp, 57, 'IND', 
     2937600, NULL, to_timestamp('42491', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '5550', 
     'AQ3956791',to_timestamp('45778', 'MM/DD/YYYY')::timestamp, 'K0129308', to_timestamp('01/21/2025', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '53051308'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'ENFERMERO',to_timestamp('30987', 'MM/DD/YYYY')::timestamp, 154, 'IND', 
     3055104, NULL, to_timestamp('10/14/14', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '21145', 
     'AQ117038',to_timestamp('45332', 'MM/DD/YYYY')::timestamp, 'M3332593', to_timestamp('01/23/2027', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '43487980'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'ENFERMERO',to_timestamp('03/03/1969', 'MM/DD/YYYY')::timestamp, 22, 'IND', 
     3055104, 424000, to_timestamp('39459', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '10923', 
     'AM705981',to_timestamp('04/14/2021', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '39774631'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'ANESTESIOLOGO',to_timestamp('05/21/1967', 'MM/DD/YYYY')::timestamp, 120, 'SERV', 
     NULL,NULL , to_timestamp('05/14/2012', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '125/93', 
     'AQ127479',to_timestamp('45453', 'MM/DD/YYYY')::timestamp, 'J8062936', to_timestamp('10/15/2024', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '52961090'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'ANESTESIOLOGO',to_timestamp('08/23/1982', 'MM/DD/YYYY')::timestamp, 99, 'IND', 
     6133158, NULL, to_timestamp('40643', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '1324/2007', 
     'AN252053',to_timestamp('44295', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1053781544'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'ANESTESIOLOGO',to_timestamp('10/29/1987', 'MM/DD/YYYY')::timestamp, 177, 'SERV', 
     NULL,NULL, to_timestamp('12/20/2016', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '73 1737', 
     'AN760540',to_timestamp('05/25/2022', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '91493127'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'ANESTESIOLOGO',to_timestamp('28013', 'MM/DD/YYYY')::timestamp, 175, 'SERV', 
     NULL,NULL, to_timestamp('42381', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '1256', 
     'AR021137',to_timestamp('05/25/2025', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '52332767'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'ANESTESIOLOGO',to_timestamp('12/19/1975', 'MM/DD/YYYY')::timestamp, 153, 'SERV', 
     NULL,NULL, to_timestamp('41893', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '9532/2000', 
     'AN353486',to_timestamp('11/31/2021', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79285666'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'ANESTESIOLOGO',to_timestamp('07/16/1963', 'MM/DD/YYYY')::timestamp, 174, 'SERV', 
     NULL,NULL, to_timestamp('42712', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '6017/94', 
     'AN798194',to_timestamp('44691', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '11222843'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'EMERGENCIOLOGOS',to_timestamp('12/30/1978', 'MM/DD/YYYY')::timestamp, 176, 'SERV', 
     NULL,NULL, to_timestamp('12/20/2016', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '1122 / 2006', 
     'PE122931',to_timestamp('45909', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79593357'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'EMERGENCIOLOGOS',to_timestamp('26786', 'MM/DD/YYYY')::timestamp, 170, 'SERV', 
     NULL,NULL, to_timestamp('06/21/2016', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '131482/2001', 
     'AN468970',to_timestamp('01/15/2022', 'MM/DD/YYYY')::timestamp, 'J8026586', to_timestamp('09/16/2024', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79533312'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'EMERGENCIOLOGOS',to_timestamp('25968', 'MM/DD/YYYY')::timestamp, 150, 'SERV', 
     NULL,NULL, to_timestamp('41649', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '380/99', 
     'AO899265',to_timestamp('08/22/2023', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '19373558'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'PEDIATRA',to_timestamp('01/06/1958', 'MM/DD/YYYY')::timestamp, 5, 'SERV', 
     NULL,NULL, to_timestamp('11/20/2008', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '02297/85', 
     'AP559675',to_timestamp('45326', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79235034'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'PEDIATRA',to_timestamp('12/08/1968', 'MM/DD/YYYY')::timestamp, 42, 'SERV', 
     NULL,NULL, to_timestamp('09/15/2008', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '121/92', 
     'AQ895318',to_timestamp('04/27/2025', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79247333'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'PEDIATRA',to_timestamp('25820', 'MM/DD/YYYY')::timestamp, 1, 'SERV', 
     NULL,NULL, to_timestamp('39459', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '4934/95', 
     'AR720349',to_timestamp('10/20/2025', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '98389847'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'PEDIATRA',to_timestamp('27517', 'MM/DD/YYYY')::timestamp, 167, 'SERV', 
     NULL,NULL, to_timestamp('42371', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '33324/52', 
     'AN975212',to_timestamp('44690', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '52419110'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'PEDIATRA',to_timestamp('28464', 'MM/DD/YYYY')::timestamp, 156, 'SERV', 
     NULL,NULL, to_timestamp('02/27/2015', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '695/2002', 
     'AQ276840',to_timestamp('11/24/2024', 'MM/DD/YYYY')::timestamp, 'K0104443', to_timestamp('45839', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1037585972'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'URGENTOLOGO',to_timestamp('10/29/1987', 'MM/DD/YYYY')::timestamp, 168, 'SERV', 
     NULL,NULL, to_timestamp('02/22/2016', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '631036-12', 
     'AN704038',to_timestamp('04/27/2022', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '71782638'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'URGENTOLOGO',to_timestamp('05/22/1979', 'MM/DD/YYYY')::timestamp, 137, 'SERV', 
     NULL,NULL, to_timestamp('41281', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '531/2004', 
     'AQ956626',to_timestamp('45966', 'MM/DD/YYYY')::timestamp, 'H3759046', to_timestamp('45019', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '71261882'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'URGENTOLOGO',to_timestamp('07/31/1982', 'MM/DD/YYYY')::timestamp, 134, 'SERV', 
     NULL,NULL, to_timestamp('41647', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '2182/2007', 
     'AR062135',to_timestamp('45694', 'MM/DD/YYYY')::timestamp, 'H3701840', to_timestamp('06/23/2023', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '98644523'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'URGENTOLOGO',to_timestamp('27765', 'MM/DD/YYYY')::timestamp, 133, 'SERV', 
     NULL,NULL , to_timestamp('04/19/2013', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '432/2006', 
     'AO663554',to_timestamp('44932', 'MM/DD/YYYY')::timestamp, 'K0149638', to_timestamp('02/22/2025', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '15432010'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'URGENTOLOGO',to_timestamp('08/20/1968', 'MM/DD/YYYY')::timestamp, 141, 'SERV', 
     NULL,NULL , to_timestamp('41315', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '17-15041-95', 
     'AR081597',to_timestamp('45753', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '41759492'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'TERAPEUTA',to_timestamp('08/26/1958', 'MM/DD/YYYY')::timestamp, 151, 'SERV', 
     NULL,NULL , to_timestamp('10/15/2014', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '41759492', 
     'AR256623',to_timestamp('07/13/2025', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '79299841'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'TERAPEUTA',to_timestamp('10/15/1963', 'MM/DD/YYYY')::timestamp, 85, 'SERV', 
     NULL,NULL , to_timestamp('40184', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '19042000', 
     'AP599929',to_timestamp('04/20/2024', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '71312855'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'TERAPEUTA',to_timestamp('29435', 'MM/DD/YYYY')::timestamp, 172, 'SERV', 
     NULL,NULL , to_timestamp('06/21/2016', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '1957', 
     'AP375943',to_timestamp('01/31/2024', 'MM/DD/YYYY')::timestamp, 'F7714291', to_timestamp('44718', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '15370664'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'TERAPEUTA',to_timestamp('30963', 'MM/DD/YYYY')::timestamp, 171, 'SERV', 
     NULL,NULL , to_timestamp('06/21/2016', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '427091', 
     'AN685989',to_timestamp('04/19/2022', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '103757808'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'TERAPEUTA',to_timestamp('10/20/1986', 'MM/DD/YYYY')::timestamp, 66, 'SERV', 
     NULL,NULL , to_timestamp('40035', 'MM/DD/YYYY')::timestamp,NULL, 
     'A+',null, '5420909', 
     'AR890395',to_timestamp('11/19/2025', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '43164688'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'TERAPEUTA',to_timestamp('06/29/1980', 'MM/DD/YYYY')::timestamp, 58, 'SERV', 
     NULL,NULL , to_timestamp('12/18/2012', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '50608', 
     'AR890386',to_timestamp('11/19/2025', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '1128404383'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'TERAPEUTA',to_timestamp('31423', 'MM/DD/YYYY')::timestamp, 165, 'SERV', 
     NULL,NULL , to_timestamp('42228', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '5-5863-11', 
     'AR500400',to_timestamp('45725', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '43464287'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'TERAPEUTA',to_timestamp('31117', 'MM/DD/YYYY')::timestamp, 162, 'SERV', 
     NULL,NULL , to_timestamp('42228', 'MM/DD/YYYY')::timestamp,NULL, 
     'O+',null, '05-6423-14', 
     'AQ046629',to_timestamp('45605', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin'),
 (nextval('public.hibernate_sequence'), 0, 
    (select id from people.person where document_number = '38257310'), (select id from operation.airplane_type  where name = ''), 
     'AEROMEDICA', 'TERAPEUTA',to_timestamp('10/19/1962', 'MM/DD/YYYY')::timestamp, 34, 'SERV', 
     NULL,NULL , to_timestamp('39755', 'MM/DD/YYYY')::timestamp,NULL, 
     'O-',null, '959', 
     'PE079620',to_timestamp('44929', 'MM/DD/YYYY')::timestamp, '', to_timestamp('', 'MM/DD/YYYY')::timestamp,  'ACTIVO', 
     current_timestamp, current_timestamp,
     'admin', 'admin');


update people.employee set salary=1000000,anual_bonus = 500000;

insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '7545403'),15,'cesar.jaramillo@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80412728'),15,'wilson.gutierrez@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '16610004'),15,'fernando.merchan@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '16680012'),15,'nelson.prieto@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19354460'),15,'fernado.junca@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80544344'),15,'juan.baron@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19380308'),15,'orlando.fajardo@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '14248464'),15,'roger.arias@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '16626083'),15,'jaime.rodriguez@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19340841'),15,'ivan.jimenez@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71786428'),15,'luis.correa@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79436760'),15,'sergio.pinzon@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '9657236'),15,'arlan.barreto@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '17322579'),15,'german.galindo@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1020757988'),15,'juan.valencia@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79059319'),15,'jose.botia@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80001685'),15,'jaime.montes@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1037606031'),15,'juan.renteria@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1022373986'),15,'andres.campos@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80804049'),15,'hector.acosta@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1037607838'),15,'jhonatan.betancur@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1020738380'),15,'juan.rizo@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1016054336'),15,'nairo.carreno@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '93151406'),15,'alvaro.florez@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '93154219'),15,'carlos.hurtad@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79544166'),15,'nelson.pabon@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80121403'),15,'hewil.solano@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1022979734'),15,'bernardo.aguilera@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1044420498'),15,'jabes.castro@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1037323243'),15,'daniel.canaveral@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1018425532'),15,'oscar.novoa@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80009552'),15,'jhon.fonseca@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1035872466'),15,'jhon.rave@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79901999'),15,'cesar.ortiz@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1013605900'),15,'brayan.pulido@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1107053135'),15,'renan.urrego@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19364349'),15,'luis.santana@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1022960425'),15,'fabian.ruiz@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79846949'),15,'mauricio.legro@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1032405711'),15,'almacen.bogota@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '39190067'),15,'jefe.despacho@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '14237423'),15,'despacho@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1014253413'),15,'despacho@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1014262092'),15,'despacho@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '39760058'),15,'comercial@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43616295'),15,'adriana.escobar@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43667482'),15,'administracion@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43765223'),15,'sarpafinanciera@une.net.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52325367'),15,'marcela.ayala@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '53107192'),15,'sms@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79318122'),15,'direccion.calidad@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79533622'),15,'direccion.mantenimiento@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1017135067'),15,'karen.arbelaez@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '17149518'),15,'orlando.arbelaez@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71603815'),15,'jaime.maya@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '8716522'),15,'gerencia.administrativa@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '98698960'),15,'alejopelaez3122@gmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43975298'),15,'academico@volarcolombia.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '51618826'),15,'moonpat@une.net.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1032441100'),15,'martha.mahecha@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1040754838'),15,'santiagoal2220@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1128456122'),15,'jose_arbelaezlu@virtual.ceipa.edu.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1015411800'),15,'asistencia.ambulancia@gmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1010173469'),15,'programacion.manto@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1072707090'),15,'farmacia.ambulancia@gmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1023024507'),15,'recepcionbgta@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1020442913'),15,'sarparecep@une.net.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1037608513'),15,'contabilidad@sarpa.com.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '8125801'),15,'sarpacont@une.net.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52334593'),15,'claudia_mahechab@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52931230'),15,'helenandrea90@gmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1128407346'),15,'jhona-917@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52310028'),15,'lukaya1009@yahoo.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '53051308'),15,'martha_tc84@yahoo.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43487980'),15,'nona369@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '39774631'),15,'adriuribemejia@yahoo.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52961090'),15,'lizaalexandra@yahoo.com.br - lizaalexandra@gmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1053781544'),15,'milochamo@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '91493127'),15,'cfvillamil@gmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52332767'),15,'monsony75@gmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79285666'),15,'carcedim@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '11222843'),15,'danielwady@gmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79593357'),15,'drespicons@hotmial.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79533312'),15,'vlachogomez@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19373558'),15,'aarenasabello@gmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79235034'),15,'erneprieto@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79247333'),15,'gerardoadolphsm@yahoo.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '98389847'),15,'rmbastidas01@gmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52419110'),15,'lynnamaia@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1037585972'),15,'diegomesa41@gmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71782638'),15,'jorgegiraldo0522@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71261882'),15,'juanpablorendon@yahoo.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '98644523'),15,'laos0106@gmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '15432010'),15,'pzambrano@une.net.co' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '41759492'),15,'aliciagomez2003@yahoo.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79299841'),15,'castroangelm@yahoo.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71312855'),15,'kmiloegr26@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '15370664'),15,'dandres21@gmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '103757808'),15,'godie86@gmail,com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43164688'),15,'bibian469@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1128404383'),15,'norles-86@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43464287'),15,'paulioty@hotmail.com' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '38257310'),15,'sanesmarmo@yahoo.com' );

insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '7545403'),17,'2838275' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '16610004'),17,'3112493' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19354460'),17,'4635550' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19380308'),17,'7353552' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '14248464'),17,'7506116' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '16626083'),17,'7358002' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19340841'),17,'4282138' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71786428'),17,'5421866' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1020757988'),17,'7535172' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79059319'),17,'7584781' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80001685'),17,'4663573' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1037606031'),17,'3218693' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1022373986'),17,'4548993' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80804049'),17,'2630752' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1037607838'),17,'3015454' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1016054336'),17,'2186005' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1022960425'),17,'031768 0836' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '21396161'),17,'3613695' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43616295'),17,'17746908' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43975298'),17,'3613695' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '51618826'),17,'3613695' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1040754838'),17,'3613695' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52334593'),17,'4101383' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52931230'),17,'6146759' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1128407346'),17,'7030192' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52310028'),17,'8021333' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '53051308'),17,'7532963' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '39774631'),17,'7027995' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52961090'),17,'7342577' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1053781544'),17,'82776662' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79285666'),17,'7598893' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '11222843'),17,'7583859' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79593357'),17,'2570554' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79533312'),17,'4750558' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19373558'),17,'2717625' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79235034'),17,'6179338' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79247333'),17,'3517159' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '98389847'),17,'3862735' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52419110'),17,'4715135' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1037585972'),17,'5134916' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71782638'),17,'5809785' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71261882'),17,'4191050' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '98644523'),17,'2663143' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '15432010'),17,'5813156' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '41759492'),17,'3116603' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79299841'),17,'6417791' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71312855'),17,'5963302' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '15370664'),17,'3536494' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '103757808'),17,'2884841' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1128404383'),17,'5071508' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43464287'),17,'3435468' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '38257310'),17,'4834474' );

insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '7545403'),19,'3007148433' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '16680012'),19,'3202740380' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80544344'),19,'3123957539' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19380308'),19,'3118128381' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '14248464'),19,'3208117464' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19340841'),19,'3132629507' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71786428'),19,'3113542312' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79436760'),19,'3144424581' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '9657236'),19,'3228809736' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '17322579'),19,'3118548289' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79059319'),19,'3006007379' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80001685'),19,'3105749485' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '93154219'),19,'3173322792' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79544166'),19,'3214681829' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80009552'),19,'3214924962' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1022960425'),19,'3102663896' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '39190067'),19,'3116091274' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '14237423'),19,'3116091290' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1014253413'),19,'3134798927' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '21396161'),19,'3104341018' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '39760058'),19,'3112735028' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43616295'),19,'3122886461' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43667482'),19,'3146812841' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43765223'),19,'3207181157' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52325367'),19,'3185219142' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '53107192'),19,'3105759071' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79318122'),19,'3204990704' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79533622'),19,'3153583282' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1017135067'),19,'3116098815' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '17149518'),19,'3113099555' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71603815'),19,'3116091280' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '8716522'),19,'3128484848' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '11386740'),19,'3164811055' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '98698960'),19,'3127430872' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43975298'),19,'3148042078' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '51618826'),19,'3146266921' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1040754838'),19,'3206956834' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1015411800'),19,'3016314864' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1020442913'),19,'3116091294' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52334593'),19,'3105511155 -- 3174284340' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52931230'),19,'3124416449' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1128407346'),19,'3014915567' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52310028'),19,'3163721337' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '53051308'),19,'3208155279' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43487980'),19,'3165251893' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '39774631'),19,'3002124045' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52961090'),19,'3105551501' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1053781544'),19,'3012248716' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '91493127'),19,'3202369154-3103256393 ' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52332767'),19,'3152693540' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79285666'),19,'3124807699' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '11222843'),19,'3003832399' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79593357'),19,'3106988937' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79533312'),19,'3114443742' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19373558'),19,'3133964696' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79235034'),19,'3102711863' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79247333'),19,'3124496071' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '98389847'),19,'3016360309' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52419110'),19,'3115007416' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1037585972'),19,'3122011342' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71782638'),19,'3113104761' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71261882'),19,'3146864131' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '98644523'),19,'3162448337' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '15432010'),19,'3148300172' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '41759492'),19,'3002155397' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79299841'),19,'3102322498' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71312855'),19,'3154799595' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '15370664'),19,'3127724495' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '103757808'),19,'3146563220' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43164688'),19,'3003442499' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1128404383'),19,'3137357128' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43464287'),19,'3006758664' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '38257310'),19,'3124486686' );

insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '7545403'),19,'3007148433' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80412728'),19,'3002156739' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '16610004'),19,'3118473590' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '16680012'),19,'3002740380' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19354460'),19,'3153975984' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80544344'),19,'3123957539' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19380308'),19,'3118128381' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '14248464'),19,'3208117464' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '16626083'),19,'3133470399' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19340841'),19,'3132629507' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71786428'),19,'3206716582' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79436760'),19,'3144424581' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '9657236'),19,'3102478121' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '17322579'),19,'3118548289' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1020757988'),19,'3102626546' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79059319'),19,'3006007379' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80001685'),19,'3105749485' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1037606031'),19,'3108515199' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1022373986'),19,'3204355775' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80804049'),19,'3114519917' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1037607838'),19,'3014879490' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1020738380'),19,'3156122737' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1016054336'),19,'3178870005' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '93151406'),19,'3112736795' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '93154219'),19,'3112565360' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79544166'),19,'3214681829' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '80121403'),19,'3003141319' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1013605900'),19,'3004533010' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43616295'),19,'3006020798' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52334593'),19,'3157544710' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1128407346'),19,'4963452' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '53051308'),19,'3134536808' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '43487980'),19,'3003680901' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '39774631'),19,'2571505' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52961090'),19,'4832515' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '1053781544'),19,'3008701703' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '91493127'),19,'3113086494' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79285666'),19,'3102503112' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '11222843'),19,'3003832399' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79593357'),19,'18113564' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '19373558'),19,'3105611165' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79235034'),19,'2434351' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79247333'),19,'6485985 6485781' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '52419110'),19,'3102275908' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71261882'),19,'2505732' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '98644523'),19,'333 14 56' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '41759492'),19,'3014278673' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '79299841'),19,'3107661722' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '71312855'),19,'3004714991' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '15370664'),19,'3536494' );
insert into people.contact (id,version,person_id,type_id,value) values (nextval('public.hibernate_sequence'), 0, (select id from people.person where document_number = '38257310'),19,'3103413506' );
